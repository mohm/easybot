<?php
include 'messagelist.inc.php';

function check_is_ip($ipdata) {
	return (filter_var($ipdata, FILTER_VALIDATE_IP)) ? true : false;
}
function getHostUrl() {
	if(isset($_SERVER['HTTPS'])){
		$protocol = ($_SERVER['HTTPS'] and $_SERVER['HTTPS'] != "off") ? "https" : "http";
	} else {
		$protocol = "http";
	}
	return $protocol.'://'.$_SERVER['HTTP_HOST'];
}
function quote($c) {
	return array("<blockquote class='$c'>","</blockquote>");
}
function redirect($params) {
	header('Location: index.php?'.$params);
}
function blockquote($input, $class) {
	return "<blockquote class='$class'>$input</blockquote>";
}
function formUrl($get) {
	return $_SERVER["PHP_SELF"]."?".http_build_query($_GET);
}
function validateEmail($emaildata) {
	return (filter_var($emaildata, FILTER_VALIDATE_EMAIL)) ? true : false;
}
function validateUrl($urldata) {
	return (filter_var($urldata, FILTER_VALIDATE_URL)) ? true : false;
}
function validateDomain($domain) {
	return (preg_match('^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$^', $domain)) ? true : false;
}
function get_timestamp($zone="Europe/Oslo") {
	date_default_timezone_set($zone);
	$timestamp = date('Y-m-d H:i:s');
	return $timestamp;
}
function colorize_value($color, $value, $title='') {
	return "<font title='$title' color='$color'>$value</font>";
}
function feedbackMsg($headline,$msg,$style, $botr=0) {
	return ($botr == 0) ? "<div class='$style'>
	<span class='closebtn' onclick='this.parentElement.style.display=\"none\";'>&times;</span>
	<strong>$headline</strong><br> $msg
	</div>":"<blockquote class='$style'><strong>$headline</strong><br>$msg";
}
function msgList($keyword) {
	global $messageList, $headlineList;
	list($section, $id) = explode('-',$keyword);
	return feedbackMsg($headlineList[$section],$messageList[$keyword],$section);
}
function generatePassword() {
	$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	$special = '%&!?#$@*|=+';
	$randompass = array();
	$alphaLength = strlen($alphabet) - 1;
	$specialLength = strlen($special) - 1;
	for ($i = 0; $i < 8; $i++) {
		if ($i === rand(0,8)) {
			$n = rand(0, $specialLength);
			$randompass[] = $special[$n];
		} else {
			$n = rand(0, $alphaLength);
			$randompass[] = $alphabet[$n];
		}
	}
	return implode($randompass);
}
function multi_implode(array $glues, array $array){
	$out = "";
	$g = array_shift($glues);
	$c = count($array);
	$i = 0;
	foreach ($array as $val){
		if (is_array($val)){
			$out .= multi_implode($glues,$val);
		} else {
			$out .= (string)$val;
		}
		$i++;
		if ($i<$c){
			$out .= $g;
		}
	}
	return $out;
}
function onoff($bool, $titleTrue='', $titleFalse='', $textTrue='', $textFalse='', $h='20', $w='20') {
	global $pos_color, $neg_color;
	return ($bool) ? '<img class="rounded" src="/images/static/green_light_1.png" title="'.$titleTrue.'" height="'.$h.'" width="'.$w.'"> <font color="'.$pos_color.'">'.$textTrue:'<img class="rounded" src="/images/static/red_light_1.png" title="'.$titleFalse.'" height="'.$h.'" width="'.$w.'"> <font color="'.$neg_color.'">'.$textFalse;
}
function warning($title='', $h='20', $w='20') {
	return '<img src="/images/static/warning.png" title="'.$title.'" height="'.$h.'" width="'.$w.'">';
}
function actionButton($type, $title='', $h='30', $w='30') {
	$src = "";
	switch ($type) {
		case "edit":
			$src = "src='/images/static/edit.png'";
			break;
		case "delete":
			$src = "src='/images/static/delete.png'";
			break;
		case "question":
			$src = "src='/images/static/question.png'";
			break;
		default:
			return false;
	}
	return "<img style='background: none; border: none;' title='$title' height='$h' width='$w' class='rounded' $src>";
}

function verify() {
	return ($_SESSION['status'] == "logged") ? true : false;
}
function issetor(&$var, $default = "") {
	return isset($var) ? $var : $default;
}
function statusdigger($nested) {
	$iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($nested));
	$keys = array();
	foreach ($iterator as $key => $value) {
		// Build long key name based on parent keys
		for ($i = $iterator->getDepth() - 1; $i >= 0; $i--) {
			$key = $iterator->getSubIterator($i)->key() . '.' . $key;
		}
		$keys[] = $key .': <b>'. $value . '</b>';
	}
	return implode('<br>', $keys);
}
function removeHTML($str) {
	return preg_replace('/\s+/', ' ', str_replace(['<','>'], ' ', $str));
}

//CLASSES
class Db {
	protected static $connection;
	
	public function connect() {
		if(!isset(self::$connection)) {
			$config = parse_ini_file('/volume1/easybot-dev/config/config.ini');
			self::$connection = new mysqli('localhost',$config['username'],$config['password'],$config['dbname']);
		}
		if(self::$connection === false) {
			return false;
		}
		return self::$connection;
	}
	public function query($query, $last_id=false) {
		$connection = $this -> connect();
		if ($last_id) {
			$connection -> query($query);
			return mysqli_insert_id($connection);
		}
		if ($result = $connection -> query($query)) {
			return $result;
		}
		else
			return $this -> error();
	}
	public function select($query) {
		$rows = array();
		$result = $this -> query($query);
		if($result === false) {
			return false;
		}
		while ($row = $result -> fetch_assoc()) {
			$rows[] = $row;
		}
		return $rows;
	}
	
	public function select_specific($query) {
		$row = array();
		$result = $this -> query($query);
		if($result === false) {
			return false;
		}
		$row = $result -> fetch_assoc();
		return $row;
	}
	
	public function error() {
		$connection = $this -> connect();
		return $connection -> error;
	}
	
	public function errorHandler($boolean){
		if ($boolean === true) {
			return "<div style='background-color: green;'>Success</div>";
		}
		else {
			return "<div style='background-color: red;'>Failure</div>";
		}
	}
	
	public function quote($value) {
		$connection = $this -> connect();
		return "" . $connection -> real_escape_string($value) . "";
	}
	
	public function quoteArray($array) {
		foreach ($array as $k => $v) {
			$array[$k] = (is_string($array[$k])) ? $this->quote($array[$k]):$array[$k];
		}
		return $array;
	}
	
	public function constructInsertValues($table, $data_array) {
		return "INSERT INTO {$table} (`".implode("`, `", array_keys($data_array))."`) VALUES ('".implode("', '", $data_array)."')";
	}
	
	public function constructUpdateValues($table, $data_array, $clause) {
		$result = "UPDATE {$table} SET ";
		$last_key = key(array_slice($data_array, -1, true));
		foreach($data_array as $key => $value) {
			$result .= "{$key} = '{$value}'";
			if ($key != $last_key) $result .= ",";
		}
		$result .= " {$clause}";
		return $result;
	}
	//Accepted domains
	public function acceptedDomainAdd($data) {
		$data['domain'] = strtolower($data['domain']);
		return $this->query($this->constructInsertValues("domains", $data), true);
	}
	public function acceptedDomainDelete($id) {
		$this->query("DELETE FROM domains WHERE id = '$id'");
		$this->query("DELETE FROM bot_allowed_domain WHERE domainid = '$id'");
	}
	public function acceptedDomainFetch($domainid='') {
		return (empty($domainid)) ? $this->select("SELECT * FROM domains") : $this->select("SELECT * FROM domains WHERE id = '{$domainid}'") ;
	}
	//Group handlers
	public function groupAdd($data) {
		return $this->query($this->constructInsertValues("groups", $data));
	}
	public function groupRemove($groupid) {
		$groupinfo = $this->groupFetchGroups($groupid);
		if (count($groupinfo)>0) {
			//$this->query("UPDATE response SET accessgroup = '0' WHERE accessgroup = '{$groupinfo[0]['id']}'");
			$this->query("UPDATE bot_webhook SET groupid = '0' WHERE groupid = '$groupid'");
			$e = $this->query("DELETE FROM groups WHERE id = '{$groupid}' or sub_id = '{$groupid}'");
			$e .= $this->query("DELETE FROM group_contacts WHERE groupid = '{$groupinfo[0]['id']}'");
			$e .= $this->query("DELETE FROM space_contacts WHERE groupid = '{$groupinfo[0]['id']}'");
			$e .= $this->query("DELETE FROM group_groups WHERE groupid = '{$groupinfo[0]['id']}' or nestedid = '{$groupinfo[0]['id']}'");
			return $e;
		} else return "failed";
	}
	public function groupUpdate($data) { //Update group details
		$table = "groups";
		$group_data = $data['groupname'];
		$updateid = $data['id'];
		$clause = "WHERE id = '{$updateid}'";
		if (isset($data['botid']) and !empty($data['botid'])) {
			if ($this->groupCheckHasLinkedGroups($updateid)) {
				return false;
			}
		} elseif (!isset($data['botid']) or empty($data['botid'])) {
			if ($this->groupCheckIfGroupHasSpaceMember($updateid)) {
				return false;
			}
		}
		$this->query($this->constructUpdateValues($table, $data, $clause));
		return true;
	}
	//Adds a user to a local group
	public function groupAddContact($data) { //groupid & contactid
		return $this->query($this->constructInsertValues("group_contacts", $data));
	}
	//Adds a space to a local group
	public function groupAddSpace($data) { //groupAddSpace(array('groupid' => $groupid, 'spaceid' => $spaceid, 'botid' => $botid));
		$table = "group_spaces";
		return $this->query($this->constructInsertValues($table, $data));
	}
	//Adds a group to a local group (nested membership)
	public function groupAddGroup($data) { //groupid & nestedid
		$table = "group_groups";
		return $this->query($this->constructInsertValues($table, $data));
	}
	//Removes a contact from a group
	public function groupRemoveContact($groupid, $contactid) {
		return $this->query("DELETE FROM group_contacts WHERE groupid = '{$groupid}' and contactid = '{$contactid}'");
	}
	//Removes a space from a group
	public function groupRemoveSpace($groupid, $spaceid, $botid) {
		return $this->query("DELETE FROM group_spaces WHERE groupid = '{$groupid}' and spaceid = '{$spaceid}' and botid = '{$botid}'");
	}
	//Removes a group from a group
	public function groupRemoveGroup($groupid) {
		return $this->query("DELETE FROM group_groups WHERE groupid = '{$groupid}'");
	}
	//Returns the number of total members for a group id (including nested groups)
	public function groupMembershipNumber($groupid) {
		return count($this->groupGetMembers($groupid));
	}
	//Returns the user members for a group id
	public function groupUserMembershipNumber($groupid) {
		return count($this->select("SELECT * FROM group_contacts WHERE groupid = '{$groupid}'"));
	}
	//Returns the number of spaces for a group that belongs to a particular bot
	public function groupSpaceMembershipNumber($groupid, $botid) {
		return count($this->select("SELECT * FROM group_spaces WHERE groupid = '{$groupid}' and botid = '{$botid}'"));
	}
	//Get all (including linked) user members of a particular local group
	public function groupGetMembers($groupid) {
		//Fetch the groups that are member of this groupid
		$extra_options = "";
		$membergroups = $this->groupGetGroupMembers($groupid);
		if (count($membergroups)) {
			foreach ($membergroups as $key => $value) {
				$extra_options .=  "or groupid = '{$value['nestedid']}' ";
			}
		}
		//Fetch the users that are member of this groupid
		$list = $this->select("SELECT DISTINCT contactid FROM group_contacts WHERE groupid = '{$groupid}' $extra_options");
		//Fetch the members of the groups linked to this group id
		
		return $list;
	}
	//ID not alias - generates an array of member IDs of a group
	public function groupGetMemberIdArray($groupid) {
		$members = $this->groupGetMembers($groupid);
		$returnvalue = [];
		if (count($members)) {
			foreach ($members as $key => $value) {
				$returnvalue[] = $value['contactid'];
			}
		}
		return $returnvalue;
	}
	//Get actual user members of a particular local group
	public function groupGetActualMembers($groupid) {
		return $this->select("SELECT contactid FROM group_contacts WHERE groupid = '{$groupid}'");
	}
	//Get space members of a particular local group
	public function groupGetSpaceMembers($groupid, $botid) {
		return $this->select("SELECT spaceid FROM group_spaces WHERE groupid = '{$groupid}' and botid = '{$botid}'");
	}
	public function groupGetSpaceMemberIdArray($groupid, $botid) {
		$spacemembers = $this->groupGetSpaceMembers($groupid, $botid);
		$returnvalue = [];
		if (count($spacemembers)) {
			foreach ($spacemembers as $key => $value) {
				$returnvalue[] = $value['spaceid'];
			}
		}
		return $returnvalue;
	}
	//Get member (nested) groups of a particular local group
	public function groupGetGroupMembers($groupid) {
		return $this->select("SELECT nestedid FROM group_groups WHERE groupid = '{$groupid}'");
	}
	//Check if the group has other groups linked
	public function groupCheckHasLinkedGroups($groupid) {
		return (count($this->select("SELECT nestedid FROM group_groups WHERE nestedid = '{$groupid}'"))) ? true:false;
	}
	//Return groupname of the group the contact is linked from
	public function groupCheckContactGroupLink($groupid, $contactid) {
		$linked_groups = $this->groupGetGroupMembers($groupid);
		foreach ($linked_groups as $key => $value) {
			if ($this->groupCheckIfActualMember($contactid, $value['nestedid'])) {
				$group_details = $this->groupFetchGroups($value['nestedid']);
				return $group_details;
			}
		}
		return False;
	}
	//Check if a contact is member of a particular group
	public function groupCheckIfMember($contactid, $groupid) {
		$membership = $this->groupGetMembers($groupid);
		if (validateEmail($contactid)) {
			$userinfo = $this->contactFetchContacts($contactid);
			if (count($userinfo)) {
				$contactid = $userinfo[0]['id'];
			} else {
				return false;
			}
		}
		return (in_array($contactid, array_column($membership, 'contactid'))) ? true : false;
	}
	//Check if a contact is member of a particular group
	public function groupCheckIfActualMember($contactid, $groupid) {
		return (count($this->select("SELECT contactid FROM group_contacts WHERE groupid = '{$groupid}' and contactid = '{$contactid}'"))>0) ? true:false;
	}
	//Check if a space is part of a particular group
	public function groupCheckIfSpaceMember($spaceid, $groupid, $botid) {
		$membership = $this->select("SELECT * FROM group_spaces WHERE (groupid = '{$groupid}' and spaceid = '{$spaceid}' and botid = '{$botid}')");
		return (count($membership)>0) ? true : false;
	}
	public function groupCheckIfGroupHasSpaceMember($groupid) {
		return (count($this->select("SELECT * FROM group_spaces WHERE groupid = '$groupid'"))) ? true : false;
	}
	public function groupGetGroupOwner($botid='0') {
		return $this->select("SELECT * FROM groups WHERE botid = '$botid'");
	}
	public function groupFetchGroups($groupid="") {
		return (!empty($groupid)) ? $this->select("SELECT * FROM groups WHERE id = '{$groupid}' or sub_id = '{$groupid}'") : $this->select("SELECT * FROM groups ORDER BY groupname ASC");
	}
	//Returns groups that can be subscribed to
	public function groupFetchSubscriptionGroups($groupid="") {
		return (!empty($groupid)) ? $this->select("SELECT * FROM groups WHERE (id = '{$groupid}' or sub_id = '{$groupid}') and subscribable = '1'") : $this->select("SELECT * FROM groups WHERE subscribable = '1' ORDER BY groupname ASC");
	}
	public function groupFetchDefaultGroups() {
		return $this->select("SELECT * FROM groups WHERE default_group = '1' ORDER BY groupname ASC");
	}
	//Feedback (feature)
	//Add a feedback topic
	public function feedbackTopicAdd($data, $last_id=false) {
		return $this->query($this->constructInsertValues("feedback_topic", $this->feedbackFilterArray($data, 'topic')), $last_id);
	}
	//Updates a feedback topic
	public function feedbackTopicUpdate($data, $topicid) {
		$clause = "WHERE id = '{$topicid}'";
		return $this->query($this->constructUpdateValues("feedback_topic", $this->feedbackFilterArray($data, 'topic'), $clause));
	}
	//Deletes many feedback topics - all entries, votes and comments based on bot owner
	public function feedbackBotTopicsDelete($botid) {
		$topics = $this->select("SELECT * FROM feedback_topic WHERE botid = '$botid'");
		if (count($topics)>0) {
			foreach ($topics as $key => $value) {
				$entries = $this->feedbackFetchTopicEntries($value['id']);
				if (count($entries)>0) {
					foreach ($entries as $key1 => $value1) {
						$this->feedbackEntryDelete($value1['id']);
					}
				}
				$this->query("DELETE FROM feedback_topic WHERE id = '{$value['id']}'");
			}
		}
	}
	//Deletes a feedback topic - all entries, votes and comments
	public function feedbackTopicDelete($topicid) {
		$this->query("DELETE FROM feedback_topic WHERE id = '{$topicid}'");
		$entries = $this->feedbackFetchTopicEntries($topicid);
		foreach ($entries as $key => $value) {
			$this->feedbackEntryDelete($value['id']);
		}
	}
	//Check if operation is allowed
	public function feedbackTopicCheckAllowList($action, $topic) {
		return ($topic[0][$action]) ? true : false;
	}
	//Check access group
	public function feedbackTopicCheckAccessGroup($user, $topic) {
		$groupid = $topic[0]['accessgroup'];
		if ($groupid) {
			$contact = $this->contactFetchContacts($user);
			if (count($contact) > 0) {
				$contactid = $contact[0]['id'];
				return ($this->groupCheckIfMember($contactid, $groupid)) ? true : false;
			} else return false;
		}
		return true;
	}
	//Filter array
	public function feedbackFilterArray($data, $type) {
		$newData = array();
		switch ($type) {
			case "topic":
				$filter = array('botid', 'title', 'comments_allowed', 'votes_allowed', 'entry_create_allowed', 'entry_view_allowed', 'entry_delete_allowed', 'accessgroup');
				break;
			case "entry":
				$filter = array('description', 'topic_id', 'created_by');
				break;
		}
		foreach ($filter as $k => $v) {
			if (isset($data[$v])) {
				$newData[$v] = $data[$v];
			}
		}
		return $newData;
	}
	//Add a feedback entry
	public function feedbackEntryAdd($data, $last_id=false) {
		$data = $this->quoteArray($data);
		$q = $this->constructInsertValues("feedback_entries", $this->feedbackFilterArray($data, 'entry'));
		return $this->query($q, $last_id);
	}
	//Update a feedback entry
	public function feedbackEntryUpdate($data, $entryid) {
		$data = $this->quoteArray($data);
		$clause = "WHERE id = '{$entryid}'";
		return $this->query($this->constructUpdateValues("feedback_entries", $this->feedbackFilterArray($data, 'entry'), $clause));
	}
	//Delete feedback entries
	public function feedbackEntryDelete($entryid) {
		$this->query("DELETE FROM feedback_entries WHERE id = '{$entryid}'");
		$this->query("DELETE FROM feedback_entry_vote WHERE entry_id = '{$entryid}'");
		$this->query("DELETE FROM feedback_entry_comment WHERE entry_id = '{$entryid}'");
	}
	//Fetch topic entries
	public function feedbackFetchTopicEntries($topicid="") {
		return ($topicid) ? $this->select("SELECT * FROM feedback_entries WHERE topic_id = {$topicid}") : $this->select("SELECT * FROM feedback_entries");
	}
	//Fetch entry details
	public function feedbackFetchEntry($entryid) {
		return $this->select("SELECT * FROM feedback_entries WHERE id = '{$entryid}'");
	}
	//Fetch topics
	public function feedbackFetchTopics($botid, $topicid="") {
		return (!empty($topicid)) ? $this->select("SELECT * FROM feedback_topic WHERE botid = '{$botid}' and id = '{$topicid}'") : $this->select("SELECT * FROM feedback_topic WHERE botid = '{$botid}' ORDER BY title ASC");
	}
	//Add vote to entry
	public function feedbackEntryVote($email, $entryid) {
		return $this->query($this->constructInsertValues('feedback_entry_vote', array('entry_id'=>$entryid, 'email'=>$email)));
	}
	//Remove vote from entry
	public function feedbackEntryVoteDelete($id="", $email="", $entryid="") {
		if ($id) {
			return $this->query("DELETE FROM feedback_entry_vote WHERE id = '{$id}'");
		} elseif ($email and $entryid) {
			return $this->query("DELETE FROM feedback_entry_vote WHERE email = '{$email}' and entry_id = {$entryid}");
		} else return;
	}
	//Fetch entry votes
	public function feedbackFetchEntryVotes($entryid="") {
		return (!empty($entryid)) ? $this->select("SELECT * FROM feedback_entry_vote WHERE entry_id = '{$entryid}'") : $this->select("SELECT * FROM feedback_entry_vote");
	}
	//Check if vote exist
	public function feedbackFetchEntryVoteExists($entryid, $email) {
		return (count($this->select("SELECT * FROM feedback_entry_vote WHERE entry_id = '{$entryid}' and email = '$email'")) > 0) ? true : false;
	}
	//Add comment to entry
	public function feedbackEntryComment($entryid, $comment, $email, $last_id=false) {
		return $this->query($this->constructInsertValues('feedback_entry_comment', array('entry_id'=>$this->quote($entryid), 'email'=>$email, 'comment'=>removeHTML($this->quote($comment)))), $last_id);
	}
	//Delete a comment by id
	public function feedbackEntryCommentDelete($commentid) {
		return $this->query("DELETE FROM feedback_entry_comment WHERE id = '{$commentid}'");
	}
	//Fetch entry comments
	public function feedbackFetchEntryComments($entryid="") {
		return (!empty($entryid)) ? $this->select("SELECT * FROM feedback_entry_comment WHERE entry_id = '{$entryid}'") : $this->select("SELECT * FROM feedback_entry_comment");
	}
	//Fetch entry comments
	public function feedbackFetchEntryComment($commentid) {
		return $this->select("SELECT * FROM feedback_entry_comment WHERE id = '{$commentid}'");
	}
	//User Login
	public function userLogin($username, $password){
		$userinfo = $this->select("SELECT * FROM contacts WHERE emails = '$username'");
		if (count($userinfo)>0){
			if ($userinfo[0]['type'] != ""){
				if ($this->adminGetUserRoles($userinfo[0]['type'])[0]['role'] === 'admin') {
					return (password_verify($password, $userinfo[0]['password'])) ?  true : false;
				}
			}
		}
		return false;
	}
	//Contact/User handlers
	public function userUpdateSettings($action, $hash_value, $userid, $role='') {
		switch ($action) {
			case 'token':
				$data = array('token' => $hash_value);
				break;
			case 'update':
				$data = array('type'=>$role,'password'=>$hash_value);
				break;
			default:
				$data = array('type'=>'','password'=>'', 'token'=>'');
				break;
		}
		return $this->query($this->constructUpdateValues("contacts", $data, "WHERE id = '$userid'"));
	}
	public function contactAdd($data, $botid) {
		//Filter not needed data
		$data = $this->contactFilterArray($data['items'][0]);
		$data = $this->quoteArray($data);
		$this->query($this->constructInsertValues("contacts", $data));
		$default_groups = $this->groupFetchDefaultGroups();
		foreach ($default_groups as $key => $group){
			if ($group['botid'] == $botid or $group['botid'] == "0") $this->groupAddContact(array('groupid' => $group['id'], 'contactid' => $data['id']));
		}
	}
	public function contactUpdate($data) {
		foreach ($data['items'] as $k => $v) {
			$meta = $this->contactFilterArray($data['items'][$k]);
			$meta = $this->quoteArray($meta);
			$this->query($this->constructUpdateValues("contacts", $meta, "WHERE id = '{$meta['id']}'"));
		}
	}
	public function contactRemove($userid) {
		$this->query("DELETE from contacts WHERE id = '{$userid}'");
		$this->query("DELETE FROM group_contacts WHERE contactid = '{$userid}'");
		$this->query("DELETE FROM contact_access_group_response WHERE contactid = '{$userid}'");
	}
	//Only keep the keys that we want to insert, remove all others
	public function contactFilterArray($data) {
		$newData = array();
		$filter = array('id','firstName','lastName','orgId', 'avatar', 'emails');
		foreach ($filter as $k => $v) {
			if (isset($data[$v])) {
				$newData[$v] = ($v == 'emails') ? $data[$v][0] : $data[$v];
			}
		}
		return $newData;
	}
	//Get added contacts based on e-mail, id or all contact
	public function contactFetchContacts($contactid="") {
		if (!empty($contactid) and filter_var($contactid, FILTER_VALIDATE_EMAIL)) {
			return $this->select("SELECT * FROM contacts WHERE emails = '{$contactid}'");
		}
		elseif(!empty($contactid)) {
			return $this->select("SELECT * FROM contacts WHERE id = '{$contactid}'");
		}
		else {
			return $this->select("SELECT * FROM contacts ORDER BY firstName ASC");
		}
	}
	public function contactGetName($id) {
		$contactinfo = $this->contactFetchContacts($id);
		$value = (count($contactinfo)) ? $contactinfo[0]['firstName'] . " " . $contactinfo[0]['lastName'] :  $id;
		if (trim($value) == "") $value = $contactinfo[0]['emails'];
		return $value;
	}
	public function contactGetNameLink($id) {
		$contactinfo = $this->contactFetchContacts($id);
		if (count($contactinfo)) {
			$email = $contactinfo[0]['emails'];
			$name = $this->contactGetName($email);
			return "<a href='index.php?id=contacts&contactid={$contactinfo[0]['id']}' title='$email'>$name</a>";
		}
		return $id;
	}
	public function contactGetGroupResponseAcl($contactid) {
		$returnvalue = "";
		$acl = $this->select("SELECT * FROM contact_access_group_response WHERE contactid = '{$contactid}'");
		if (count($acl) > 0) {
			foreach ($acl as $key => $value) {
				$botinfo = $this->botFetchBots($value['botid']);
				$returnvalue .= "{$botinfo[0]['emails']}:{$value['id']} <a href='index.php?id=contacts&special_access_remove={$contactid}&botid={$value['botid']}&spaceid={$value['id']}'>Delete</a><br>";
			}
			return $returnvalue;
		}
		else{
			return "None";
		}
	}
	//Message handlers
	public function messagesLoad($id="") {
		return (empty($id)) ?
		$this->select("SELECT * FROM saved_messages") :
		$this->select("SELECT * FROM saved_messages WHERE id = '$id'");
	}
	public function messagesSave($data) {
		$this->query($this->constructInsertValues("saved_messages", $data));
	}
	public function messagesUpdate($data, $id) {
		$this->query($this->constructUpdateValues("saved_messages", $data, "WHERE id = '$id'"));
	}
	public function messagesDelete($id) {
		$this->query("DELETE FROM saved_messages WHERE id = '$id'");
	}
	//Admin handlers
	public function adminCheckWebhookExists($webhookid, $botid) {
		return (count($this->select("SELECT * FROM bot_webhook WHERE webhookid = '$webhookid' and botid = '$botid'"))>0) ? true:false;
	}
	public function adminCheckIsBotMain($id) {
		return (count($this->select("SELECT * FROM bots WHERE id = '$id' and main = '1'"))>0) ? true:false;
	}
	public function adminCheckIsLoginUser($id){
		return (count($this->select("SELECT * FROM contacts WHERE type = '1' and password != '' and id = '$id'"))>0) ? true:false;
	}
	public function adminCheckSiteAdminExists(){
		return (count($this->select("SELECT * FROM contacts WHERE type = '1' and password != ''"))>0) ? true:false;
	}
	public function adminCheckTaskQueue() {
		return count($this->select("SELECT * FROM tasks"));
	}
	public function adminPurgeTaskQueue(){
		$this->query("DELETE FROM tasks");
	}
	public function adminCheckIfMaintenance() {
		$maintenance_status = $this->select_specific("SELECT maintenance_mode FROM service_status WHERE id = '1'");
		return $maintenance_status['maintenance_mode'];
	}
	public function adminGetMaintenanceMessage() {
		$maintenance_status = $this->select_specific("SELECT message FROM generic_feedback WHERE id = 'maintenance'");
		return $maintenance_status['message'];
	}
	public function adminCheckBotRestriction($botid) {
		return (count($this->select("SELECT * FROM bot_allowed_domain WHERE botid = '{$botid}'"))) ? true:false;
	}
	public function adminSetMaintenance($value) {
		return $this->query("UPDATE service_status SET maintenance_mode = '$value' WHERE id = '1'");
	}
	public function adminSetMaintenanceMessage($value) {
		return $this->query("UPDATE generic_feedback SET message = '$value' WHERE id = 'maintenance'");
	}
	public function adminCheckIfTaskMonitor() {
		$taskmonitor_status = $this->select_specific("SELECT task_monitor FROM service_status WHERE id = '1'");
		return $taskmonitor_status['task_monitor'];
	}
	public function adminSetTaskMonitor($value) {
		$this->query("UPDATE service_status SET task_monitor = '$value' WHERE id = '1'");
	}
	public function adminCheckServiceStatus($type='') { //report to get text output, nothing to get true or false
		$heartbeat = $this->select_specific("SELECT * FROM heartbeat WHERE id = '1'");
		$time = strtotime($heartbeat['time']);
		$curtime = time();
		if ($type === 'report') {
			return (($curtime-$time) >= 30) ? "Task service is **DOWN** for: >**" . ($curtime-$time) . "** seconds, last heartbeat received on {$heartbeat['time']}" :
			"Task service is **UP** | last heartbeat received {$heartbeat['time']}";
		}
		else {
			return (($curtime-$time) >= 30) ? true : false;
		}
	}
	public function adminCheckIfValidAPIToken($token) {
		return $this->select("SELECT * FROM contacts WHERE token = '$token'");
	}
	public function adminCheckIfValidDomain($email, $botid) {
		list($user, $domain) = explode("@", $email);
		$domains = $this->select("SELECT d.domain, b.* FROM domains as d, bot_allowed_domain as b WHERE d.id = b.domainid and b.botid = '{$botid}'");
		return (in_array($domain,array_column($domains,'domain'))) ? true : false;
	}
	public function adminCheckGroupResponseAcl($botid, $spaceid) {
		return (count($this->select("SELECT * FROM excluded_spaces WHERE id = '$spaceid' and botid = '$botid'"))>0) ? true:false;
	}
	public function adminGetGroupResponseAcl($botid="") {
		return ($botid == "") ? $this->select("SELECT * FROM excluded_spaces") : $this->select("SELECT * FROM excluded_spaces WHERE botid = '$botid'");
	}
	public function adminCheckUserGroupResponseAcl($botid, $spaceid="", $contactid="") {
		if (empty($spaceid) and empty($contactid)) {
			$query = "SELECT * FROM contact_access_group_response WHERE (botid = '$botid')";
		}
		elseif (!empty($spaceid) and empty($contactid)) {
			$query = "SELECT * FROM contact_access_group_response WHERE (botid = '$botid' and id = '$spaceid')";
		}
		elseif (!empty($spaceid) and !empty($contactid)) {
			$query = "SELECT * FROM contact_access_group_response WHERE (id = '$spaceid' and botid = '$botid' and contactid = '{$contactid}')";
		}
		$result = $this->select($query);
		return $result;
	}
	public function adminAddGroupResponseAcl($botid, $spaceid, $title='') {
		$title = $this->quote($title);
		$this->query("INSERT INTO excluded_spaces (id, botid, spacetitle) VALUES ('$spaceid', '$botid', '$title')");
	}
	public function adminAddUserGroupResponseAcl($botid, $spaceid, $contactid) {
		$this->query("INSERT INTO contact_access_group_response (id, botid, contactid) VALUES ('$spaceid', '$botid', '$contactid')");
	}
	public function adminRemoveUserGroupResponseAcl($botid, $spaceid, $contactid) {
		$this->query("DELETE FROM contact_access_group_response WHERE (id = '$spaceid' and botid = '$botid' and contactid = '$contactid')");
	}
	public function adminRemoveGroupResponseAcl($botid, $spaceid) {
		$this->query("DELETE FROM excluded_spaces WHERE (id = '$spaceid' and botid = '$botid')");
	}
	public function adminUpdateGroupResponseAcl($spaceid, $title) {
		$this->query("UPDATE excluded_spaces SET spacetitle = '$title' WHERE id = '$spaceid'");
	}
	public function adminBlockContact($email) {
		$this->query("INSERT INTO blocked_contacts (email) VALUES ('$email')");
	}
	public function adminUnblockContact($email) {
		$this->query("DELETE FROM blocked_contacts WHERE email = '$email'");
	}
	public function adminGetBlockedContacts($email=""){
		if (!empty($email)){
			return (count($this->select("SELECT * FROM blocked_contacts WHERE email='$email'"))>0) ? true : false;
		}
		else {
			return $this->select("SELECT * FROM blocked_contacts");
		}
	}
	public function adminAddJoinableSpace($data) {
		$this->query($this->constructInsertValues("joinable_space",$data));
	}
	public function adminUpdateJoinableSpace($spaceid, $title) {
		$this->query($this->query("UPDATE joinable_space SET spacetitle = '$title' WHERE spaceid = '$spaceid'"));
	}
	public function adminRemoveJoinableSpace($botid, $spaceid) {
		return $this->query("DELETE FROM joinable_space WHERE spaceid='$spaceid' and botid='$botid'");
	}
	public function adminGetJoinableSpace($botid, $roomid="") {
		return (!empty($roomid)) ? $this->select("SELECT * FROM joinable_space WHERE botid='$botid' and spaceid='$roomid'") : $this->select("SELECT * FROM joinable_space WHERE botid='$botid'");
	}
	public function adminGetJoinableSpaceById($botid, $id) {
		return $this->select("SELECT * FROM joinable_space WHERE botid='$botid' and id='$id'");
	}
	public function adminCheckJoinableSpace($botid, $spaceid) {
		return (count($this->select("SELECT * FROM joinable_space WHERE spaceid='$spaceid' and botid='$botid'"))>0) ? true : false;
	}
	//Checks if a given space via id, is joinable from bot
	public function adminCheckJoinableSpaceById($botid, $id) {
		return (count($this->select("SELECT * FROM joinable_space WHERE id='$id' and botid='$botid'"))>0) ? true : false;
	}
	//Logs a user event
	public function adminAddLogUser($email) {
		return (validateEmail($email)) ? $this->query("INSERT INTO log_users (email) VALUES ('$email')") :
		false;
	}
	//Get available features
	public function adminGetFeatures($id="") {
		return (!empty($id)) ? $this->select("SELECT * FROM features WHERE id = '$id'"):
		$this->select("SELECT * FROM features");
	}
	public function adminGetFeatureUsage($keyword) {
		return $this->select_specific("SELECT * FROM features WHERE keyword = '$keyword'");
	}
	public function adminSetFeature($featureid, $botid) {
		$feature = $this->adminGetFeatures($featureid)[0];
		$current = $this->responseFetchResponse($botid, $feature['keyword']);
		if (count($current)>0) {
			($current['is_feature'] == "0") ? $this->responseUpdate(array("is_feature"=>"1", "is_task"=>"0","id"=>$current['id'])):$this->responseDelete($current['id']);
		}
		else {
			$this->query($this->constructInsertValues('response', array("botid"=>$botid,"keyword"=>$feature['keyword'], "response"=>$feature['usage'], "is_task"=>"0", "is_feature"=>'1', "accessgroup"=>"0")));
		}
	}
	//Get one or all user events
	public function adminGetLogUsers($email="") {
		return (!empty($email)) ? $this->select("SELECT * FROM log_users WHERE email = '$email'"):
		$this->select("SELECT * FROM log_users");
	}
	public function adminGetLogBot() {
		$logbotid = $this->select("SELECT * FROM log_bot")[0]['botid'];
		return (!empty($logbotid)) ? $logbotid:false;
	}
	//Fetch one or all of the User roles (Related to login to the web page)
	public function adminGetUserRoles($id="") {
		return (!empty($id)) ? $this->select("SELECT * FROM roles WHERE id = '$id'"):
		$this->select("SELECT * FROM roles");
	}
	public function adminFilterMentions($botname, $text) {
		list($filter) = explode(' ',$botname);
		$text = str_replace($botname." ", "", $text);
		$text = str_replace($filter." ", "", $text);
		return $text;
	}
	public function adminReportLogs($botid='') {
		$command_usage = array();
		$month_q = date('Y-m');
		$last_month_q = date('Y-m', strtotime('last month'));
		$year_q = date('Y');
		$day_q = date('Y-m-d');
		$total_number = count($this->select("SELECT * FROM activity_log WHERE botid = '{$botid}' ORDER BY date ASC"));
		$year_number =  count($this->select("SELECT * FROM activity_log WHERE botid = '{$botid}' and date LIKE '{$year_q}%' ORDER BY date ASC"));
		$month_number =  count($this->select("SELECT * FROM activity_log WHERE botid = '{$botid}' and date LIKE '{$month_q}%' ORDER BY date ASC"));
		$last_month_number = count($this->select("SELECT * FROM activity_log WHERE botid = '{$botid}' and date LIKE '{$last_month_q}%' ORDER BY date ASC"));
		$day_number = count($this->select("SELECT * FROM activity_log WHERE botid = '{$botid}' and date LIKE '{$day_q}%' ORDER BY date ASC"));
		$bot_keywords = $this->select("SELECT keyword FROM response WHERE botid = '{$botid}'");
		
		$returnvalue = "##Usage report (Commands responded to)\n<hr> <blockquote class=success>Generic usage</blockquote>\n- Usage today: **{$day_number}**\n- Usage this year: **{$year_number}**\n- Lifetime: **{$total_number}**\n\n<blockquote class=success>Per command usage</blockquote>\n\n";
		foreach ($bot_keywords as $key => $value) {
			$command_usage[$value['keyword']] = count($this->select("SELECT * FROM activity_log WHERE botid = '$botid' and command LIKE '%{$value['keyword']}%'"));
		}
		arsort($command_usage);
		foreach ($command_usage as $key => $value){
			$returnvalue .= '<li> ' . $key . ':<b>' . $value . '</b>';
		}
		return $returnvalue;
	}
	//Bot handlers
	//Returns the bot id that owns the webhook id.
	public function botGetWebhookOwner($webhookid){
		$webhookowner = $this->select("SELECT * FROM bot_webhook WHERE webhookid = '{$webhookid}'");
		return $webhookowner[0]['botid'];
	}
	//Fetches and returns the main bot info. The main bot is used by the system to fetch information from the API so you don't have to select a bot to perform small tasks.
	public function botGetMainInfo() {
		$mainbot = $this->select("SELECT * FROM bots WHERE main = '1'");
		return (isset($mainbot[0]) and !empty($mainbot[0])) ? $mainbot[0]:false;
	}
	//Add bot based on data from the API
	public function botAdd($data) {
		//Filter not needed data
		if (!isset($data['items'][0])) return False;
		$data = $this->botFilterArray($data);
		return $this->query($this->constructInsertValues("bots", $data['items'][0]));
	}
	public function botAddAllowedDomain($domainid, $botid) {
		$this->query("INSERT INTO bot_allowed_domain (domainid, botid) VALUES ('$domainid', '$botid')");
	}
	public function botDeleteAllowedDomain($botid) {
		return $this->query("DELETE FROM bot_allowed_domain WHERE botid = '$botid'");
	}
	public function botCheckDomain($domainid, $botid) {
		return (count($this->select("SELECT * FROM bot_allowed_domain WHERE domainid = '{$domainid}' and botid = '{$botid}'")) > 0) ? true : false;
	}
	public function wizardAddBot($data) {
		//Filter not needed data
		$data = $this->wizardAddBotFilterArray($data);
		return $this->query($this->constructInsertValues("bots", $data));
	}
	public function botSetPrimary($botid) {
		$this->query("UPDATE bots SET main = '0' WHERE main='1'");
		$this->query("UPDATE bots SET main = '1' WHERE id = '$botid'");
	}
	public function botSetDefaultResponse($response ,$botid) {
		$this->query("UPDATE bots SET defres = '$response' WHERE id = '$botid'");
	}
	//Purge a bot from the system including all responses, webhook references, owned groups etc.
	public function botDelete($id) {
		$this->query("DELETE FROM bot_webhook WHERE botid = '$id'");
		$this->feedbackBotTopicsDelete($id);
		$this->query("UPDATE groups SET botid = '0' WHERE botid ='$id'");
		$this->query("DELETE FROM contact_access_group_response WHERE botid='$id'");
		$this->query("DELETE FROM excluded_spaces WHERE botid = '$id'");
		$this->query("DELETE FROM joinable_space WHERE botid = '$id'");
		$this->query("DELETE FROM response WHERE botid = '$id'");
		$this->query("DELETE FROM group_spaces WHERE botid = '$id'");
		$this->query("DELETE FROM bot_allowed_domain WHERE botid = '$id'");
		$this->query("DELETE FROM bots WHERE id = '$id'");
		return true;
	}
	public function botFetchBots($botid='') {
		return (!empty($botid)) ? $this->select("SELECT * FROM bots WHERE id = '$botid'") : $this->select("SELECT * FROM bots ORDER BY displayName ASC");
	}
	public function botUpdate($data) {
		//Filter not needed data
		$data = $this->botFilterArray($data);
		return $this->query($this->constructUpdateValues("bots", $data['items'][0], "WHERE id = '{$data['items'][0]['id']}'"));
	}
	public function botUpdateSettings($botid, $access="", $main="") {
		if (empty($main) and !empty($access)) {
			return $this->query($this->query("UPDATE bots SET access = '$access' WHERE id = '$botid'"));
		} elseif (!empty($main) and empty($access)) {
			$this->query($this->query("UPDATE bots SET main = '0' WHERE main = '1'"));
			$this->query($this->query("UPDATE bots SET main = '1' WHERE id = '$botid'"));
		}
	}
	public function botFilterArray($data) {
		$newData = array(
				'items' => array()
		);
		$filter = array('id','displayName','avatar','type', 'emails');
		foreach ($filter as $k => $v) {
			if (isset($data['items'][0][$v])) {
				$newData['items'][0][$v] = ($v == 'emails') ? $data['items'][0][$v][0] : $data['items'][0][$v];
			}
		}
		return $newData;
	}
	public function wizardAddBotFilterArray($data) {
		$newData = array();
		$filter = array('id','displayName','avatar','type', 'emails');
		foreach ($filter as $k => $v) {
			if (isset($data[$v])) {
				$newData[$v] = ($v == 'emails') ? $data[$v][0] : $data[$v];
			}
		}
		return $newData;
	}
	//Response handlers
	public function responseFetchResponse($botid="", $keyword="") {
		return $this->select_specific("SELECT * FROM response WHERE botid = '{$botid}' and keyword = '{$keyword}'");
	}
	public function responseFetchResponses($botid, $id="") {
		return (empty($id)) ? $this->select("SELECT * FROM response WHERE botid = '$botid' ORDER BY keyword ASC") :
		$this->select("SELECT * FROM response WHERE id = '$id'");
	}
	public function responseCreate($data) {
		if ($data['is_task'] == 1 and count($this->adminGetFeatureUsage($data['keyword'])) > 0) {
			return false;
		}
		$data['keyword'] = explode(' ', $data['keyword'])[0];
		return $this->query($this->constructInsertValues("response", $data));
	}
	public function responseUpdate($data) {
		if ($data['is_task'] == 1 and count($this->adminGetFeatureUsage($data['keyword'])) > 0) {
			return false;
		}
		return $this->query($this->constructUpdateValues("response", $data, "WHERE id = '{$data['id']}'"));
	}
	public function responseDelete($responseid) {
		return $this->query("DELETE FROM response WHERE id = '$responseid'");
	}
	//Task handlers
	public function taskAddNewTask($taskid, $task_attributes, $metadata) {
		list($user, $type, $roomid, $botmail, $payload) = $metadata;
		$attr1 = $attr2 = $attr3 = $attr4 = $attr5 = $attr6;
		list($attr1, $attr2, $attr3, $attr4, $attr5, $attr6) = $task_attributes;
		$text = $payload;
		$query="INSERT INTO tasks (taskId, attr1, attr2, attr3, attr4, attr5, attr6, text, user, bot, type, roomid, timestamp)
		VALUES('$taskid','$attr1','$attr2','$attr3','$attr4', '$attr5', '$attr6', '$text', '$user', '$botmail', '$type', '$roomid', NOW())";
		return ($this->query($query)) ? true : false;
	}
	//Webhook handlers
	public function webhookDbLink($webhookid, $botid){
		$this->query("INSERT INTO bot_webhook (webhookid, botid) VALUES('$webhookid','$botid')");
	}
	public function webhookDbUnlink($webhookid){
		$this->query("DELETE FROM bot_webhook WHERE webhookid = '$webhookid'");
	}
	public function webhookGetAccessGroup($webhookid){
		$result = $this->select("SELECT * FROM bot_webhook WHERE webhookid = '$webhookid'");
		return (count($result)) ? $result[0]['groupid'] : false;
	}
	public function webhookSetAccessGroup($webhookid, $groupid){
		return $this->query("UPDATE bot_webhook SET groupid = '$groupid' WHERE webhookid = '$webhookid'");
	}
	//Logging
	public function logsGet($botid="", $limit){
		return (!empty($botid)) ? $this->select("SELECT * FROM activity_log WHERE botid = '$botid' ORDER BY date DESC LIMIT $limit"):
		$this->select("SELECT * FROM activity_log ORDER BY date DESC, botid DESC LIMIT $limit");
	}
}

class SparkEngine {
	
	protected $db;
	protected $generate;
	
	//Create new objects
	public function __construct() {
		$this->db = new Db();
		$this->generate = new OutputEngine();
	}
	
	public function getApiUrl($api){
		$apiList = array("messages"=>"https://api.ciscospark.com/v1/messages",
				"devices" =>"https://api.ciscospark.com/v1/devices",
				"devicestatus" => "https://api.ciscospark.com/v1/xapi/status",
				"devicecommand" => "https://api.ciscospark.com/v1/device/xapi/command",
				"people"=>"https://api.ciscospark.com/v1/people",
				"webhooks"=>"https://api.ciscospark.com/v1/webhooks",
				"rooms"=>"https://api.ciscospark.com/v1/rooms",
				"membership"=>"https://api.ciscospark.com/v1/memberships",
				"teams"=>"https://api.ciscospark.com/v1/teams",
				"teammemberships"=>"https://api.ciscospark.com/v1/team/memberships"
		);
		return $apiList[$api];
	}
	//HEADERS
	public function build_spark_headers($auth, $method, $content=NULL, $type='') {
		$authorization = "Authorization: Bearer {$auth}";
		$timeout = 10;
		if ($type == 'json' or $method == "PUT") {
			$contentType = "application/json";
			$content = http_build_query($content);
		} else {
			$contentType = "Content-Type: application/json";
		}
		if ($method == "GET") {
			return array(
					CURLOPT_TIMEOUT => $timeout,
					CURLOPT_CUSTOMREQUEST => $method,
					CURLOPT_SSL_VERIFYPEER => FALSE,
					CURLOPT_HTTPHEADER => array($authorization, $contentType),
					CURLOPT_RETURNTRANSFER => true
			);
		}
		elseif (($method == "POST" or $method == "PUT") and !empty($content)) {
			return array(
					CURLOPT_TIMEOUT => $timeout,
					CURLOPT_CUSTOMREQUEST => $method,
					CURLOPT_POSTFIELDS => $content,
					CURLOPT_SSL_VERIFYPEER => FALSE,
					CURLOPT_HTTPHEADER => array($authorization, $contentType),
					CURLOPT_RETURNTRANSFER => true
			);
		}
		elseif ($method == "DELETE") {
			return array(
					CURLOPT_TIMEOUT => $timeout,
					CURLOPT_CUSTOMREQUEST => $method,
					CURLOPT_SSL_VERIFYPEER => FALSE,
					CURLOPT_HTTPHEADER => array($authorization, $contentType),
					CURLOPT_RETURNTRANSFER => true
			);
		}
	}
	//AUTHORIZATION
	public function authGet($botid){
		$result = $this->db->select_specific("SELECT * FROM bots WHERE id = '{$botid}'");
		return $result['access'];
	}
	
	//MESSAGES
	public function messageBlob($text, $recepient, $botid, $roomtype="", $spacetype="") {
		if(empty($roomtype)) $roomtype = (validateEmail($recepient)) ? "toPersonEmail":"toPersonId";		
		return array('sender'=>$botid,'recepientType'=>$roomtype,'recepientValue'=>$recepient,'text'=>$text,'type'=>$spacetype);
	}
	public function messageSendEvent($text) {
		$log_users = $this->db->adminGetLogUsers();
		$logbotid = $this->db->adminGetLogBot();
		$m_query = array();
		$a = 0;
		
		if (count($log_users)>0 and $logbotid) {
			foreach ($log_users as $key => $loguser) {
				//array(array('url'=>'', 'method'=>'', 'type'=>'', 'auth'=>'', 'post'=>array()))
				$m_query[$a]['url'] = $this->getApiUrl("messages");
				$m_query[$a]['method'] = 'POST';
				$m_query[$a]['type'] = 'json';
				$m_query[$a]['auth'] = $this->authGet($logbotid);
				$m_query[$a]['post'] = array();
				$m_query[$a]['post']['toPersonEmail'] = $loguser['email'];
				$m_query[$a]['post']['markdown'] = $text;
				$a++;
				//$this->messageSend($this->messageBlob($text, $loguser['email'], $logbotid));
			}
			$this->data_multi_post($m_query);
		} else {
			return false;
		}
	}
	public function messageSend($data) {
		$auth = $this->authGet($data['sender']);
		$botinfo = $this->db->botFetchBots($data['sender']);
		$mentionself = $botinfo[0]['emails'];
		$type = issetor($data['type']);
		$url = $this->getApiUrl("messages");
		$recepient_type = $data['recepientType'];
		$recepient_value = $data['recepientValue'];
		$text = $data['text'];
		
		if ($type == "group") {
			$text = $text . "<@personEmail:$mentionself|.>";
		}
		
		$message_payload_array = array("{$recepient_type}" => "{$recepient_value}", "markdown" => "{$text}");
		if(isset($data['files'])) {
			$message_payload_array['files'] = $data['files'];
		}
		
		//Build array of relevant data
		$message_payload = json_encode($message_payload_array);
		
		//Returns array of data for json conversion
		return $this->data_post($this->build_spark_headers($auth, 'POST', $message_payload), $url);
	}
	public function messageSendMultiple($list, $botid) {
		//$list is an array("rec"=>"ReceiverID", "Type" => "toPersonId, roomId", "message"=>"Markdown message", "files" => "fileurl")
		$m_query = $report = array();
		$errors = "<blockquote class='warning'>";
		$auth = $this->authGet($botid);
		$botinfo = $this->db->botFetchBots($botid);
		$mentionself = $botinfo[0]['emails'];
		$url = $this->getApiUrl("messages");
		
		$a = 0;
		
		foreach ($list as $key => $parsel) {
			$text = ($parsel['type'] == 'roomId') ? $parsel['message'] . "<@personEmail:$mentionself|.>" : $parsel['message'];
			$m_query[$a]['url'] = $url;
			$m_query[$a]['auth'] = $auth;
			$m_query[$a]['method'] = "POST";
			$m_query[$a]['type'] = "json";
			$m_query[$a]['post'] = array();
			$m_query[$a]['post'][$parsel['type']] = $parsel['rec'];
			$m_query[$a]['post']['markdown'] = $text;
			if (!empty($parsel['files'])) $m_query[$a]['post']['files'] = $parsel['files'];
			$a++;
		}
		
		$result = $this->data_multi_post($m_query);
		$report['result'] = $result;
		
		return $report;
	}
	public function messageFetchMessages($botid, $roomid, $type=""){
		$url = $this->getApiUrl("messages");
		$auth = $this->authGet($botid);
		
		return ($type == "group") ? $this->data_get(array('roomId' => $roomid, 'mentionedPeople'=>'me', 'max'=>'50'), $this->build_spark_headers($auth, "GET"), $url) :
		$this->data_get(array('roomId' => $roomid, 'max'=>'50'), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function messageGetDetails($botid, $messageid){
		$url = $this->getApiUrl("messages").'/'.$messageid;
		$auth = $this->authGet($botid);
		
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function messageDelete($botid, $messageid){
		$url = $this->getApiUrl("messages")."/".$messageid;
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, 'DELETE'), $url);
	}
	//WEBHOOKS
	public function webhookGet($botid) {
		$url = $this->getApiUrl("webhooks");
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function webhookCreate($data) {
		$url = $this->getApiUrl("webhooks");
		$auth = $this->authGet($data['sender']);
		$webhook_data = array();
		foreach ($data as $key => $value) {
			if (!empty($value) and ($key != 'sender' and $key != 'submit')) {
				$webhook_data[$key] = $value;
			}
		}
		return $this->data_post($this->build_spark_headers($auth, 'POST', json_encode($webhook_data)), $url);
	}
	public function webhookCreateQuick($botid, $type) {
		$query = array('name'=>'group', 'sender'=>$botid, 'targetUrl'=>getHostUrl().'/api/hooker.php','resource'=>'messages','event'=>'created', 'filter'=>'roomType=group');
		if ($type == 'direct') {
			$query['name'] = 'direct';
			$query['filter'] = 'roomType=direct';
		}
		$result = $this->webhookCreate($query);
		$this->db->webhookDbLink($result['id'], $botid);
		return $result['id'];
	}
	public function webhookDelete($webhook_id, $botid) {
		$url = $this->getApiUrl("webhooks")."/".$webhook_id;
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, 'DELETE'), $url);
	}
	//Spark GET functions
	//Spark people list, takes recipient type (email, personId, displayName) and search string (email address or partial name)
	public function peopleGet($data) {
		$auth = $this->authGet($data['sender']);
		$url = $this->getApiUrl("people");
		
		$request_type = $data['recepientType'];
		$request_value = $data['recepientValue'];
		$request = array(
				"{$request_type}" => "{$request_value}"
		);
		
		return $this->data_get($request, $this->build_spark_headers($auth, "GET"), $url);
	}
	
	public function peopleAddMultipleToLocal($csv, $botid, $group) {
		$m_query = $report = array();
		$errors = "";
		$auth = $this->authGet($botid);
		$url = $this->getApiUrl("people");
		$a = 0;
		foreach ($csv as $key => $person) {
			if (validateEmail($person)){
				$m_query[$a]['url'] = $url;
				$m_query[$a]['auth'] = $auth;
				$m_query[$a]['method'] = "GET";
				$m_query[$a]['type'] = "";
				$m_query[$a]['get'] = array();
				$m_query[$a]['get']['email'] = $person;
			}
			$a++;
		}
		$result = $this->data_multi_get($m_query);
		
		foreach ($result as $key => $r) {
			$userid = issetor($r['response']['items'][0]['id']);
			$existing = 0;
			
			//Check is userid is empty or not
			if (!empty($userid)) {
				$existing = count($this->db->contactFetchContacts($userid));
			} else {
				$errors .= "No userdata in user lookup result, this indicates that this user not exist in Webex Teams ({$r['outgoing_info']['email']})<br>";
				continue;
			}
			
			//Check if user is already in the database
			if ($existing > 0) {
				//Create error if exists
				$errors .= "{$r['outgoing_info']['email']} already exists in the database<br>";
			} else {
				//Add user to DB if not exists
				$this->db->contactAdd($r['response'], $botid);
			}
			
			//Check if the user exists after adding it
			$existing = count($this->db->contactFetchContacts($userid));
			if ($existing == 0) {
				$errors .= "User was not added.. ({$r['outgoing_info']['email']})<br>";
				continue;
			}
			//Add user to group if the user exists
			if (!empty($group) and $existing > 0) {
				$fields = array('groupid' => $this->db->quote($group),
						'contactid' => $this->db->quote($r['response']['items'][0]['id']));
				$this->db->groupAddContact($fields);
			}
		}
		$report['result'] = $result;
		$report['errors'] = $errors;
		return $report;
	}
	
	public function peopleUpdateMultipleToLocal($csv) {
		$m_query = $report = $void = array();
		$errors = "";
		
		$auth = $this->authGet($this->db->botGetMainInfo()['id']);
		$url = $this->getApiUrl("people");
		$new = array();
		$a = 0;
		foreach (array_chunk($csv, 85) as $key => $val) {
			$new[$key] = implode($val, ',');
		}
		foreach ($new as $key => $persons) {
			$m_query[$a]['url'] = $url;
			$m_query[$a]['auth'] = $auth;
			$m_query[$a]['method'] = "GET";
			$m_query[$a]['type'] = "";
			$m_query[$a]['get'] = array();
			$m_query[$a]['get']['id'] = $persons;
			$a++;
		}
		
		//Start simultaneous requests
		$result = $this->data_multi_get($m_query);
		
		foreach ($result as $key => $r) {
			if (count($r['response']['items'])) {
				$this->db->contactUpdate($r['response']);
			}
			if (count($r['response']['notFoundIds'])) {
				foreach ($r['response']['notFoundIds'] as $k => $v) {
					$error_user = $this->db->contactGetName($v);
					$errors .= "Failed to update user, user not found ({$error_user})<br>";
					$void[] = $v;
				}
			}
		}
		$report['result'] = $result;
		$report['errors'] = $errors;
		$report['delete'] = $void;
		return $report;
	}
	
	public function peopleGetMe($botid) {
		$url = $this->getApiUrl("people").'/me';
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function wizardAddMainBot($token) {
		$url = $this->getApiUrl("people").'/me';
		$botvalues = $this->data_get(array(), $this->build_spark_headers($token, "GET"), $url);
		if (isset($botvalues['id'])) {
			$botid = $botvalues['id'];
			$this->db->wizardAddBot($botvalues);
			$this->db->botUpdateSettings($botid, $token);
			$this->db->botSetPrimary($botid);
			return "<img src='{$botvalues['avatar']}' class='rounded' height='50' width='50'> <b>{$botvalues['displayName']}</b> was added as the primary bot!";
		}
		else {
			return false;
		}
	}
	//DEVICES
	public function deviceList($botid) { //List devices belonging to the bot
		$url = $this->getApiUrl("devices");
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function deviceStatus($botid, $deviceid, $key='*') { //Get status of a device for a particular key
		$url = $this->getApiUrl("devicestatus");
		return $this->data_get(array('name'=>$key, 'deviceId'=>$deviceid), $this->build_spark_headers($this->authGet($botid), "GET"), $url);
	}
	public function deviceCommand($botid, $deviceid, $commandkey, $args) { //Send command to device
		$url = $this->getApiUrl("devicecommand").'/'.$commandkey;
		$payload = array(
				'deviceId'=>$deviceid
		);
		if (issetor($args)) {
			$args = trim($args);
			$payload['arguments'] = json_decode($args, true);
		}
		return $this->data_post($this->build_spark_headers($this->authGet($botid), "POST", json_encode($payload)), $url);
	}
	public function deviceDelete($botid, $deviceid) { //Send command to device
		$url = $this->getApiUrl("devices").'/'.$deviceid;
		return $this->data_post($this->build_spark_headers($this->authGet($botid), "DELETE"), $url);
	}
	//TEAMS
	public function teamGet($botid, $max) {
		$url = $this->getApiUrl("teams");
		$auth = $this->authGet($botid);
		return $this->data_get(array("max"=>$max), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function teamGetDetails($botid, $teamid) {
		$url = $this->getApiUrl("teams").'/'.$teamid;
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function teamCreate($botid, $name) {
		$url = $this->getApiUrl("teams");
		$data = array("name"=>$name);
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, "POST", json_encode($data)), $url);
	}
	public function teamUpdate($botid, $teamid, $name) {
		$url = $this->getApiUrl("teams").'/'.$teamid;
		$data = array("name"=>$name);
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, "PUT", $data), $url);
	}
	public function teamMembershipGet($teamid, $botid, $max='500') {
		$url = $this->getApiUrl("teammemberships");
		$auth = $this->authGet($botid);
		$request = array(
				"teamId" => $teamid,
				"max" => $max
		);
		return $this->data_get($request, $this->build_spark_headers($auth, "GET"), $url);
	}
	//Adds a user e-mail to a team
	public function teamMembershipCreate($teamid, $botid, $person) {
		$url = $this->getApiUrl("teammemberships");
		$auth = $this->authGet($botid);
		$request = array(
				"teamId" => $teamid,
				"personEmail" => $person
		);
		return $this->data_post($this->build_spark_headers($auth, "POST", $request, 'json'), $url);
	}
	//Takes a csv to add multiple user emails to a team
	public function teamMembershipCreateMultiple($botid, $teamid, $csv) {
		$csv = str_replace(" ", "", $csv);
		$personmail_array = explode(",", $csv);
		$report = array();
		foreach ($personmail_array as $key => $person) {
			if (validateEmail($person)){
				$report[] = $this->teamMembershipCreate($teamid, $botid, $person);
			}
			else{
				continue;
			}
		}
		return $report;
	}
	//ROOMS
	public function roomGet($data) {//Dataarray maxresults, sender
		$url = $this->getApiUrl("rooms");
		$auth = $this->authGet($data['sender']);
		$query = (isset($data['teamId']) and !empty($data['teamId'])) ? array("max"=>$data['max'], "teamId"=>$data['teamId']):array("max"=>$data['max'], "type"=>$data['type']);
		return $this->data_get($query, $this->build_spark_headers($auth, "GET"), $url);
	}
	public function roomGetDetails($botid, $roomid) {
		$url = $this->getApiUrl("rooms").'/'.$roomid;
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function roomCreate($botid, $title, $teamid="") {
		$url = $this->getApiUrl("rooms");
		$data = ($teamid != "") ? array("title"=>$title, "teamId"=>$teamid):array("title"=>$title);
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, "POST", $data, 'json'), $url);
	}
	public function roomUpdate($botid, $roomid, $title) {
		$url = $this->getApiUrl("rooms").'/'.$roomid;
		$data = array("title"=>$title);
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, "PUT", $data), $url);
	}
	public function roomDelete($botid, $roomid){
		$url = $this->getApiUrl("rooms").'/'.$roomid;
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, 'DELETE'), $url);
	}
	//MEMBERSHIPS
	public function membershipGet($roomid, $botid, $email="") {
		$url = $this->getApiUrl("membership");
		$auth = $this->authGet($botid);
		$request = array(
				"roomId" => $roomid,
				"max" => "1000"
		);
		if (validateEmail($email)){
			$request["personEmail"] = $email;
		}
		return $this->data_get($request, $this->build_spark_headers($auth, "GET"), $url);
	}
	public function membershipSetMod($membershipid, $botid, $bool) {
		$url = $this->getApiUrl("membership").'/'.$membershipid;
		$auth = $this->authGet($botid);
		$request = array(
				"isModerator" => $bool
		);
		return $this->data_post($this->build_spark_headers($auth, "PUT", $request), $url);
	}
	public function membershipDelete($botid, $membership_id){
		$url = $this->getApiUrl("membership")."/".$membership_id;
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, 'DELETE'), $url);
	}
	//Personmail comma separated example@ex.com, test@test.com...
	public function membershipCreate($botid, $roomid, $personmail){
		$url = $this->getApiUrl("membership");
		$auth = $this->authGet($botid);
		if (validateEmail($personmail)){
			$data = array("roomId"=>$roomid, "personEmail"=>$personmail);
			return $this->data_post($this->build_spark_headers($auth, 'POST', $data, 'json'), $url);
		}
		else {
			return "E-mail was not valid!";
		}
	}
	//Split up multi requests in smaller bulks
	public function bulkSplitRequestPost($type, $parsel, $botid, $botr=0, $interval=50, $sleep_counter=200, $sleeptime=15, $sleep_mark=300, $custom="") {
		$b = $err = $mark = $total_finished = $succ = 0;
		$timetosleep = 0;
		$delete = $retry_action = $delete_list = array();
		$base_report = $error_report = $extra = $localerrors = $retryhtml = $deletehtml = $files = $messagePayload = $payloads = "";
		$max = count($parsel);
		
		$start = microtime(true); //Timer start
		
		//Execute bulk requests
		if ($max) {
			foreach ($parsel as $key => $value) {
				$request_bulk[$b] = $value;
				$b++;
				
				if ($b == $sleep_mark or $timetosleep == 1) {
					sleep($sleeptime);
					$sleep_mark = ($timetosleep == 1) ? $sleep_mark + $sleep_counter : $sleep_mark;
					$timetosleep = 0;
				}
				
				if ($b == ($mark + $interval) or $b == $max) {
					$mark = $mark + $interval;
					
					//Execute different contexts depending on the incoming data
					//Create memberships in spaces
					if ($type == "memberships"){
						$raw = $this->membershipCreateMultiple($request_bulk, $botid);
					}
					//Send messages to people or spaces
					elseif ($type == "messages") {
						$raw = $this->messageSendMultiple($request_bulk, $botid);
					}
					//Add users from WxT to local database
					elseif ($type == "addtolocal") {
						$raw = $this->peopleAddMultipleToLocal($request_bulk, $botid, $custom);
					}
					//Update userinfo in the local database
					elseif ($type == "updatelocal") {
						$raw = $this->peopleUpdateMultipleToLocal($request_bulk);
					}
					
					$request_bulk = array(); //Zero out the bulk to start over
					
					$report = $this->check_multi_error($raw['result']); //Create error list (check the responses for errors)
					
					$total_finished = count($raw['result']) + $total_finished; //Increase amounts of requests
					$succ = $report['success'] + $succ; //Increase amount of successful requests
					
					$localerrors .= issetor($raw['errors']);
					
					if ($type == "updatelocal" and isset($raw['delete'])) {
						if (count($raw['delete'])>0) {
							$delete_list = array_merge($raw['delete'], $delete_list);
						}
					}
					
					//If errors in the report generate details
					if ($report['errors'] > 0) {
						$persondata = array();
						$personid = "";
						
						//Go through all the errors
						foreach ($report['error_report'] as $key => $value) {
							if (issetor($value['outgoing_info']['personId'])) {
								$personid = $value['outgoing_info']['personId'];
							}
							if (issetor($value['outgoing_info']['toPersonId'])) {
								$personid = $value['outgoing_info']['toPersonId'];
							}
							if (issetor($value['outgoing_info']['personEmail'])) {
								$personid = $value['outgoing_info']['personEmail'];
							}
							if (issetor($value['outgoing_info']['email'])) {
								$personid = $value['outgoing_info']['email'];
							}
							if (issetor($value['outgoing_info']['id'])) {
								$personid = $value['outgoing_info']['id'];
							}
							if (issetor($value['outgoing_info']['toPersonEmail'])) {
								$personid = $value['outgoing_info']['toPersonEmail'];
							}
							if ($personid) {
								$persondata = $this->db->contactFetchContacts($personid);
							}
							
							$details = (count($persondata) > 0) ? $persondata[0]['firstName'] . " " . $persondata[0]['lastName'] . " (" . $persondata[0]['emails'] . ")":"({$personid})";
							$http_code = $value['http_code']; //Fetch HTTP response code
							
							//Explain the response codes
							switch ($http_code) {
								case 429:
									$timetosleep = 1;
									if ($type == 'messages') {
										$retry_action[] = $personid;
										if (empty($messagePayload)) $messagePayload = issetor($value['outgoing_info']['markdown']);
										if (isset($value['outgoing_info']['files']) and empty($files)) $files = $value['outgoing_info']['files'];
									}
									if ($type == 'addtolocal') {
										$retry_action[] = $personid;
									}
									$extra = " - Too fast (i.e. the request I sent was too much for Webex Teams to handle)";
									break;
								case 400:
									if (count($persondata) > 0) {
										$extra = " - Missing (i.e. the person may no longer exist in Webex Teams, the person can be deleted from my database)";
									}
									if ($type == 'updatelocal') {
										$delete_list[] = $personid;
									}
									break;
								case 403:
									$extra = " - Response understood but failed (i.e. operation was not allowed)";
									break;
								case 409:
									$extra = " - Conflict (i.e. the person is already in the space or the person does not exist)";
									break;
								case 404:
									$extra = " - Not found (i.e. the person/space cannot be found in Webex Teams)";
									break;
								case 100:
									$extra = " - Delayed response (i.e. the request is most likely successful, but may have failed.)";
									break;
								case 0:
									$extra = " - Delayed response (i.e. the request is most likely successful, but may have failed.)";
									break;
								default:
									$extra = "";
									break;
							}
							
							//If HTTP Code error explain error and increase error count
							$base_report = $err + 1 . ". Issue with processing {$details} -> HttpResponseCode: <b>{$http_code} {$extra}</b>";
							$error_report .= ($botr == 1) ? "<li>$base_report</li>":"<br> $base_report";
							$err++;
						}
					}
				}
			}
		}
		$end = number_format((microtime(true) - $start), 2); //Timer end
		if ($succ == $total_finished) $success_code = "success";
		if ($err > 0) $success_code = "warning";
		if ($succ == 0) $success_code = "danger";
		if ($localerrors) $localerrors = feedbackMsg('Local errors:',$localerrors, 'warning', $botr);
		
		if (count($delete_list) > 0 and $type == 'updatelocal') {
			$num_delete = count($delete_list);
			$deletehtml .= "<br><div id='input'><form action='index.php?id=contacts&botid=$botid' method='POST' enctype='multipart/form-data'>";
			foreach ($delete_list as $key => $value) {
				$deletehtml .= "<input type='hidden' name='personids[]' value='$value'>";
			}
			$deletehtml .= "<input type='submit' name='delete_$type' value='Delete $num_delete users from the database? Click here'></form></div>";
		}
		
		if (count($retry_action)) {
			if ($type == 'messages') {
				$formaction = "index.php?id=messages&botid=$botid";
			} else if ($type == 'addtolocal') {
				$formaction = "index.php?id=contacts";
			}
			$num_429 = count($retry_action);
			$retryhtml .= "<br><div id='input'><form action='$formaction' method='POST' enctype='multipart/form-data'>";
			foreach ($retry_action as $key => $value) {
				$retryhtml .= "<input type='hidden' name='personids[]' value='$value'>";
			}
			if ($type == 'messages') {
				$retryhtml .= "<input type='hidden' name='messagepayload' value='$messagePayload'>";
				if (!empty($files)) $retryhtml .= "<input type='hidden' name='files' value='$files'>";
			}
			if ($type == 'addtolocal') {
				$retryhtml .= "<input type='hidden' name='groups' value='$custom'>";
			}
			$retryhtml .= "<input type='submit' name='retry_$type' value='Retry $num_429 requests? Click here'></form></div>";
		}
		if ($type == "updatelocal") $payloads = "(<b>$b payloads</b>)";
		$returnvalue = ($botr == 1) ? "<blockquote class=$success_code>API Response report: <b>$succ/$total_finished</b> successful requests. Finished $total_finished requests in $end seconds, <b>$err</b> errors. <br>$error_report</blockquote>  \n\n$localerrors":feedbackMsg("API Response report: ", "<b>$succ/$total_finished</b> successful requests $payloads. Finished $total_finished requests in $end seconds, <b>$err</b> errors. $error_report $retryhtml <br>$localerrors $deletehtml", $success_code);
		return $returnvalue;
	}
	
	public function membershipCreateMultiple($memberParsel, $botid) {
		//$memberParsel must be a nested array: array(array("roomtype" => "teamId|roomId", "id" => "roomid|teamid", "method" => "personEmail|personId", "person"=>"id/email"))
		$report = $m_query = array();
		$auth = $this->authGet($botid);
		$a = 0;
		foreach ($memberParsel as $key => $parsel) {
			$url = ($parsel['roomtype'] == "teamId") ? $this->getApiUrl("teammemberships"): $this->getApiUrl("membership");
			$m_query[$a]['url'] = $url;
			$m_query[$a]['auth'] = $auth;
			$m_query[$a]['method'] = "POST";
			$m_query[$a]['type'] = "json";
			$m_query[$a]['post'] = array();
			$m_query[$a]['post'][$parsel['roomtype']] = $parsel['id'];
			$m_query[$a]['post'][$parsel['method']] = $parsel['person'];
			$a++;
		}
		
		$report['result'] = $this->data_multi_post($m_query);
		return $report;
	}
	
	public function featureExecute($id, $feature_attributes=array(), $info=array()) {
		//Executes a local hardcoded feature not handled by external listener
		if (count($info) == 6) list($botid, $user, $personid, $roomid, $type, $botmail) = $info;
		elseif (count($info) == 7) list($botid, $user, $personid, $roomid, $type, $botmail, $files) = $info;
		else return "Feature fatal error!";
		list($s, $e) = quote('info');
		
		switch ($id) {
			//Internal feature listener
				//FEATURE DEVICE MANAGEMENT
			case "device":
				list($param1, $param2, $param3, $param4) = $feature_attributes;
				$returnvalue = "";
				if ($param1 == "list") {
					$returnvalue = "<blockquote class=info>Devices</blockquote><hr>";
					$devices = $this->deviceList($botid);
					if (count($devices['items'])) {
						foreach ($devices['items'] as $key => $value) {
							$returnvalue .= "\n<blockquote class=info><strong>{$value['displayName']}</strong>\n <li> <strong>ID:</strong> {$value['id']}\n <li> <strong>Connection:</strong> {$value['connectionStatus']}</blockquote>\n";
						}
						return $returnvalue;
					} else {
						return "No devices found for this bot!";
					}
				} else if ($param1 == "status") {
					if ($param2) {
						$deviceid = $param2;
						$key = strtolower(str_replace(" ", ".", implode(' ', array_slice($feature_attributes, 2))));
						$result = $this->deviceStatus($botid, $deviceid, $key);
						if (isset($result['result'])) {
							return statusdigger($result['result']);
						} else {
							return "No result in response, make sure the deviceId and the status key is correct, please try again!";
						}
					} else {
						return "You need to include the device ID as the second parameter (device status deviceId key)";
					}
				} else if ($param1 == "command") {
					if ($param2) {
						$deviceid = $param2;
						if ($param3) {
							$key = $param3;
							$args = implode(' ', array_slice($feature_attributes, 3));
							
							return json_encode($this->deviceCommand($botid, $deviceid, $key, $args));
						}
					}
				}
				else {
					return $this->db->adminGetFeatureUsage($id)['usage'];
				}
				break;
				
				//FEATURE SERVICE
			case "service":
				return $this->db->adminCheckServiceStatus("report");
				break;
				
				//FEATURE RESIGN
			case "resign":
				$userdetails = $this->db->contactFetchContacts($personid);
				if (count($userdetails)>0) {
					if ($userdetails[0]['type'] == '1') {
						return "You are a site administrator, you cannot remove yourself from here. You dont want to lock your self out!";
					} else {
						$this->db->contactRemove($personid);
						return "Success: You have been completely removed from the system!";
					}
					
				}
				else return "I cannot find $user, so you have nothing to worry about!";
				break;
				
				//FEATURE SUBSCRIBE
			case "subscribe":
				list($param1) = $feature_attributes;
				$contactinfo = $this->db->contactFetchContacts($user);
				if (count($contactinfo) > 0){
					$contactid = $contactinfo[0]['id'];
				}
				else {
					return "Your are not a registered user!";
				}
				if (!empty($param1)) {
					$groups = array_unique(explode(",", $param1));
					$returnvalue = "**Execution report**\n";
					foreach ($groups as $key => $value) {
						$groupinfo = $this->db->groupFetchSubscriptionGroups($value);
						if (count($groupinfo) > 0) {
							$groupid = $groupinfo[0]['id'];
							$groupcontact = array('groupid' => $groupid, 'contactid' => $contactid);
							if ($groupinfo[0]['botid'] == '0' or $groupinfo[0]['botid'] == $botid) {
								if ($this->db->groupCheckIfMember($contactid, $groupid)) {
									$this->db->groupRemoveContact($groupid, $contactid);
									$returnvalue .= "- Removed from: **" . $groupinfo[0]['groupname'] . "**\n";
								}
								else {
									$this->db->groupAddContact($groupcontact);
									$returnvalue .= "- Added to: **" . $groupinfo[0]['groupname'] . "**\n";
								}
							} else {
								$returnvalue .= "- **$value**: No such subscription group\n";
							}
						}
						else {
							$returnvalue .= "- **$value**: No such subscription group\n";
						}
					}
					return $returnvalue;
				} else {
					$groupinfo = $this->db->groupFetchSubscriptionGroups();
					$subscribing = $unsubscribing = "";
					$returnvalue = "<blockquote class='success'>You are subscribing to the following subscription groups:</blockquote>\n\n";
					foreach ($groupinfo as $key => $value) {
						$id = (!empty($value['sub_id'])) ? $value['sub_id']:$value['id'];
						if ($this->db->groupCheckIfMember($contactid, $value['id']) and ($value['botid'] == '0' or $value['botid'] == $botid)) {
							$subscribing .= "- **$id** ({$value['groupname']})\n    * {$value['description']}</li></p>\n";
						}
						elseif ($value['botid'] == '0' or $value['botid'] == $botid) {
							$unsubscribing .= "- **$id** ({$value['groupname']})\n    * {$value['description']}</blockquote>\n";
						}
					}
					if (empty($subscribing)) $subscribing = "**You are not subscribing to any subscription groups!**";
					if (empty($unsubscribing)) $unsubscribing = "**You are subscribing to all possible subscription groups!**";
					$returnvalue .= $subscribing;
					$returnvalue .= "\n\n<blockquote class='danger'>You can subscribe to the following subscription groups:</blockquote>\n\n" . $unsubscribing;
					$returnvalue .= "\n\n<blockquote class='info'>Usage: <b>subscribe [id]</b> to subscribe/unsubscribe to multiple groups, separate the id's with comma without spaces: <b>subscribe id,id,id</b></blockquote>";
					return $returnvalue;
				}
				break;
				
				//FEATURE - REMOVE
			case "remove":
				list($param1) = $feature_attributes;
				if ($param1 == "last"){
					$messages = $this->messageFetchMessages($botid, $roomid, $type);
					foreach ($messages['items'] as $key=>$value){
						if ($value['personEmail'] == $botmail){
							$message_id = $value['id'];
							break;
						}
					}
					$this->messageDelete($botid, $message_id);
					return "removed";
				} else return "$s <b>$id</b> usage: $e\n **remove last** - *Removes my latest reply in the space*";
				break;
				
				//FEATURE - REQUEST
			case "request":
				$existing = $this->db->contactFetchContacts($user);
				if (count($existing)>0) {
					return "You are already in my system!";
				}
				else {
					$contactdata = $this->peopleGet(array('recepientValue'=>$user, 'recepientType'=>'email', 'sender'=>$botid));
					$this->db->contactAdd($contactdata, $botid);
					$this->messageSendEvent("$user used the **request** feature. $user was automatically added to my database and assigned to the default groups (if any)");
					return "You have been successfully added to the system! It may take some time for an admin to process this request.";
				}
				break;
				//FEATURE - FEEDBACK
			case "feedback":
				//Define feature vars
				list($param1, $param2, $param3, $param4) = $feature_attributes;
				$fmsg = array(
						'error_entry_not_found' => blockquote("This entry does not exist, please verify the entryId", "danger"),
						'error_topic_not_found' => blockquote("This topic does not exist, please verify the topicId", "danger"),
				        'error_comment_not_found' => blockquote("This comment does not exist, please verify the commentId", "danger"),
						'error_something_went_wrong' => blockquote("Something went wrong", "danger"),
						'error_empty_entries' => blockquote("Adding empty entries is not allowed", "danger"),
				        'error_no_topics' => blockquote("No topics has been created yet! Only bot administrators can create topics.", "danger"),
				        'error_delete_created' => blockquote("You can only delete entries created by yourself", "danger"),
				        'error_delete_comment_created' => blockquote("You can only delete comments created by yourself", "danger"),
				        'error_parameters' => blockquote("Incorrect parameters, type <strong>feedback</strong> for help.", "danger"),
						'acl_restricted' => blockquote("This topic is restricted by accessgroup, permission denied", "danger"),
						'acl_add_entries' => blockquote("Adding entries is disabled in this topic", "warning"),
				        'acl_delete_entries' => blockquote("Deleting entries is disabled in this topic", "warning"),
				        'acl_view_entries' => blockquote("Viewing entry details is disabled in this topic", "warning"),
				        'acl_comment' => blockquote("Commenting entries is disabled in this topic", "warning"),
						'help_vote' => "<i>add or revoke a vote on the specified entry</i>",
						'help_comment' => "<i>add a comment to the specified entry</i>",
						'help_entryview' => "<i>view details and comments for the specified entry</i>",
						'help_entryadd' => "<i>add a new entry to the specified topic</i>",
						'help_topic' => "<i>list entries in the specified topic</i>",
						'mark_voted' => " [<strong>you have voted</strong>]",
						'mark_commented' => " [<strong>you have commented</strong>]",
						'mark_lock' => "[<strong>private topic</strong>]"
				);
				$mark = 20;
				$counter = 0;
				$locked = "";
				
				$restricted = blockquote("This topic is restricted by accessgroup, permission denied", "danger");
				$entry_not_found = blockquote("This entry does not exist, please verify the entryId", "warning");
				$topic_not_found = blockquote("This topic does not exist, please verify the topicId", "warning");
				$something_went_wrong = blockquote("Something went wrong", "danger");
				$vote_help = "<i>add or revoke a vote on the specified entry</i>";
				$comment_help = "<i>add a comment to the specified entry</i>";
				$entryview_help = "<i>view details and comments for the specified entry</i>";
				$entryadd_help = "<i>add a new entry to the specified topic</i>";
				$topic_help = "<i>list entries in the specified topic</i>";
				
				//FEEDBACK -> TOPICS
				if (in_array($param1, array('topic', 'topics'))) {
					if ($param2) {
						if (is_numeric($param2)) {
							//assign
							$topicid = $this->db->quote($param2);
							$topic = $this->db->feedbackFetchTopics($botid, $topicid);
							$name = $this->db->contactGetName($user);
							
							//topic exists check
							if (!count($topic)) 
								return $fmsg['error_topic_not_found'];
							
							//accessgroup check
							if (!$this->db->feedbackTopicCheckAccessGroup($user, $topic)) 
								return $fmsg['acl_restricted'];
														
							//add entry sub trigger
							if ($param3 == "add") {
								if (!$this->db->feedbackTopicCheckAllowList("entry_create_allowed", $topic)) 
									return $fmsg['acl_add_entries'];
								
								$desc = implode(' ', array_slice($feature_attributes, 3));
								
								if (empty($desc)) 
									return $fmsg['error_empty_entries'];
								
								$data = array('topic_id'=>$topic[0]['id'], 'created_by'=>$user, 'description'=> removeHTML($this->db->quote(implode(' ', array_slice($feature_attributes, 3)))));
								$last_id = $this->db->feedbackEntryAdd($data, true);
								
								if ($last_id) {
									$new_entry = $this->db->feedbackFetchEntry($last_id);
									return blockquote("$name <strong>created a new</strong> entryId: <strong>$last_id</strong> for topicId: <strong>{$topic[0]['id']}</strong><br><h3>{$new_entry[0]['description']}</h3></blockquote><hr><strong>feedback entry $last_id</strong> {$fmsg['help_entryview']}<br><strong>feedback comment $last_id [comment text...]</strong> {$fmsg['help_comment']}<br><strong>feedback vote $last_id</strong> {$fmsg['help_vote']}", "success");									
								}
								else return $fmsg['error_something_went_wrong'];
							}
							
							$entries = $this->db->feedbackFetchTopicEntries($param2);
						
							if (count($entries)) {
								$topic = $this->db->feedbackFetchTopics($botid, $param2);
								$returnvalue = "<blockquote class='success'><h1>{$topic[0]['id']} : {$topic[0]['title']}</h1></blockquote><hr>";
								foreach ($entries as $key => $value) {
									$votes = $this->db->feedbackFetchEntryVotes($value['id']);
									$comments = $this->db->feedbackFetchEntryComments($value['id']);
									$num_votes = count($votes);
									$num_comments = count($comments);
									$voted = (in_array($user, array_column($votes, 'email'))) ? $fmsg['mark_voted']:"";
									$commented = (in_array($user, array_column($comments, 'email'))) ? $fmsg['mark_commented']:"";
									$desc = (strlen($value['description'])>50) ? substr($value['description'], 0, 50) . "..." : $value['description'];
									$returnvalue .= "<blockquote class='warning'><h3><strong>{$value['id']}</strong> : {$desc}</h3></ul></blockquote><ul><blockquote class='info'>[<strong>$num_votes</strong> votes] [<strong>$num_comments</strong> comments] $voted $commented</ul></blockquote>\n";
									$counter++;
									if ($mark == $counter) {
										//send bulk to avoid messages getting to large
										$this->messageSend($this->messageBlob($returnvalue, $roomid, $botid, "roomId", $type));
										//reset values for next bulk
										$counter = 0;
										$returnvalue = "";
										//avoid out of order messages
										sleep(0.1);
									}
								}			
								$returnvalue .= "<hr><strong>feedback topic $topicid add [new entry text...]</strong> {$fmsg['help_entryadd']}<br><strong>feedback entry [entryId]</strong> {$fmsg['help_entryview']}<br><strong>feedback comment [entryId] [comment text...]</strong> {$fmsg['help_comment']}<br><strong>feedback vote [entryId]</strong> {$fmsg['help_vote']}";
								return $returnvalue;
							} else return blockquote("Ups.. This topic has no entries yet! Add one with: <strong>feedback topic $topicid add [new entry text...]</strong>", "warning");
						} 
					}
					$topics = $this->db->feedbackFetchTopics($botid);
					if (count($topics)) {
						$returnvalue = "<h1>My feedback topics</h1>";
						
						foreach ($topics as $key => $value) {
							$groupid = $value['accessgroup'];
							$locked = "";
							if ($groupid) {
								if (!$this->db->groupCheckIfMember($user, $groupid)) continue;
								$group = $this->db->groupFetchGroups($groupid);
								$groupname = $group[0]['groupname'];
								$locked = $fmsg['mark_lock'];
							}
							$v_a = ($value['votes_allowed']) ? "[<strong>vote</strong>]" : ""; 
							$c_a = ($value['comments_allowed']) ? "[<strong>comment</strong>]" : "";
							$e_c_a = ($value['entry_create_allowed']) ? "[<strong>entry create</strong>]" : "";
							$e_v_a = ($value['entry_view_allowed']) ? "[<strong>entry view</strong>]" : "";
							$e_d_a = ($value['entry_delete_allowed']) ? "[<strong>entry delete</strong>]" : "";
							$entries = $this->db->feedbackFetchTopicEntries($value['id']);
							$num_entries = count($entries);
							$returnvalue .= "<blockquote class='success'><h2>{$value['id']} : {$value['title']}</h2></blockquote><ul><blockquote class='info'>[<strong>$num_entries</strong> entries] $v_a $c_a $e_c_a $e_v_a $e_d_a $locked</ul></blockquote>\n";
						}
					} else return $fmsg['error_no_topics'];
					$returnvalue .= "<hr><strong>feedback topic [topicId]</strong> $topic_help<br>";
					return $returnvalue;
				}
				//FEEDBACK - ENTRIES
				elseif ($param1 == 'entry') {
					if ($param2) {
						if (is_numeric($param2)) {
							$entry = $this->db->feedbackFetchEntry($this->db->quote($param2));
							if (count($entry)) {
							    $topicid = $entry[0]['topic_id'];
								$entryid = $entry[0]['id'];
								$email = $entry[0]['created_by'];
								$stamp = $entry[0]['created'];
								$topic = $this->db->feedbackFetchTopics($botid, $topicid);
								
								if (count($topic) == 0) 								    
								    return $fmsg['error_topic_not_found'];
								
								if (!$this->db->feedbackTopicCheckAccessGroup($user, $topic)) 
								    return $fmsg['acl_restricted'];
								
								$name = $this->db->contactGetName($email);
								
								if ($param3 == "delete") {
									if (!$this->db->feedbackTopicCheckAllowList("entry_delete_allowed", $topic)) 
									    return $fmsg['acl_delete_entries'];
									
									if ($email != $user) 						    
									    return $fmsg['error_delete_created'];		
									
									$this->db->feedbackEntryDelete($entryid);
									return blockquote("$name <strong>deleted</strong> entryId: <strong>$entryid</strong> in topicId: <strong>$topicid</strong><br><strong>{$entry[0]['description']}</strong>", "danger");
								}
								
								if (!$this->db->feedbackTopicCheckAllowList("entry_view_allowed", $topic)) 
								    return $fmsg['acl_view_entries'];
								
								$votes = $this->db->feedbackFetchEntryVotes($entryid);
								$comments = $this->db->feedbackFetchEntryComments($entryid);
								$num_votes = count($votes);
								$num_comments = count($comments);
								$voted = (in_array($user, array_column($votes, 'email'))) ? $fmsg['mark_voted']:"";
								$commented = (in_array($user, array_column($comments, 'email'))) ? $fmsg['mark_commented']:"";
								$returnvalue = "<blockquote class='success'><h2>Entry details for entryId: $entryid in topicId: $topicid</blockquote><hr>";
								$returnvalue .= "<blockquote class='warning'>$name $stamp - [<strong>$num_votes votes</strong>] [<strong>$num_comments comments</strong>] $voted $commented<br>";
								$returnvalue .= "<h3>{$entry[0]['description']}</h3></blockquote>\n";
								
								if ($num_comments) {
									$returnvalue .= "<p><strong>comments</strong></p>";
									foreach ($comments as $key => $value) {
										$name = $this->db->contactGetName($value['email']);
										$name = ($name) ? $name : $value['email'];
										$email = $value['email']; 
										$returnvalue .= blockquote("<strong>{$value['id']} : <a href=mailto:$email>$name</a></strong> {$value['created']}<br>{$value['comment']}", "info");
										$counter++;
										if ($mark == $counter) {
											//send bulk to avoid messages getting to large
											$this->messageSend($this->messageBlob($returnvalue, $roomid, $botid, "roomId", $type));
											//reset values for next bulk
											$counter = 0;
											$returnvalue = "";
											//avoid out of order messages
											sleep(0.1);
										}
									}
								}
								$returnvalue .= "\n\n";
								$returnvalue .= "<hr><strong>feedback comment $entryid [comment text...]</strong> {$fmsg['help_comment']}<br><strong>feedback vote $entryid</strong> {$fmsg['help_vote']}";
								return $returnvalue;
							} return $entry_not_found;
						}
					} else return $fmsg['error_parameters'];
				}
				//FEEDBACK COMMENT
				elseif ($param1 == "comment") {
					$name = $this->db->contactGetName($user);
					if ($param2) {
						if (is_numeric($param2)) {
							$entry = $this->db->feedbackFetchEntry($this->db->quote($param2));
							if (count($entry)) {
								$entryid = $entry[0]['id'];
								$topicid = $entry[0]['topic_id'];
								$topic = $this->db->feedbackFetchTopics($botid, $topicid);
								
								if (count($topic) == 0) 
								    return $fmsg['error_entry_not_found'];
								
								if (!$this->db->feedbackTopicCheckAccessGroup($user, $topic)) 								    
								    return $fsmg['acl_restricted'];
								
								if (!$this->db->feedbackTopicCheckAllowList("comments_allowed", $topic)) 
								    return $fmsg['acl_comment'];
								
								if (count($topic)) {
									$comment = implode(' ', array_slice($feature_attributes, 2));
									$commentid = $this->db->feedbackEntryComment($entryid, $comment, $user, true);
									$comment = $this->db->feedbackFetchEntryComment($commentid);
									
									if (count($comment)) {
										return blockquote("$name <strong>created a new</strong> commentId: <strong>$commentid</strong> for entryId: <strong>$entryid</strong> in topicId: <strong>$topicid</strong><br><strong>{$comment[0]['comment']}</strong>", "success");
									} else return $fmsg['error_something_went_wrong'];	
								} else return $fmsg['error_entry_not_found'];
							} else return $fmsg['error_entry_not_found'];
						} 
						elseif ($param2 == "delete") {
						    if (is_numeric($param3)) {
						        $comment = $this->db->feedbackFetchEntryComment($param3);
							
							if (count($comment)) {
								$commentid = $comment[0]['id'];
								$entryid = $comment[0]['entry_id'];
								$email = $comment[0]['email'];
								$entry = $this->db->feedbackFetchEntry($entryid);
								
								if (!count($entry)) 
								    return $fmsg['error_entry_not_found'];
								
								    $topicid = $entry[0]['topic_id'];
								$topic = $this->db->feedbackFetchTopics($botid, $entry[0]['topic_id']);
								
								if (!count($topic)) 
								    return $fmsg['error_topic_not_found'];
								
								if ($email != $user) 
								    return $fmsg['error_delete_comment_created'];
								
								$this->db->feedbackEntryCommentDelete($commentid);
								return blockquote("$name <strong>deleted</strong> commentId: <strong>$commentid</strong> in entryId: <strong>$entryid</strong> in topicId: <strong>$topicid</strong><br><strong>{$comment[0]['comment']}</strong>", "danger");
							}else return $fmsg['error_comment_not_found'];
						} else return blockquote("Please provide a numeric commentId to delete", "warning");
					} else return blockquote("Please provide a numeric entryId to comment on", "warning");
				} else return blockquote("Please provide a numeric entryId to comment on", "warning");
			}
				//FEEDBACK VOTE
			elseif ($param1 == "vote") {
				if ($param2) {
					$entry = $this->db->feedbackFetchEntry($this->db->quote($param2));
					if (count($entry) > 0) {
						$entryid = $entry[0]['id'];
						$topicid = $entry[0]['topic_id'];
						$topic = $this->db->feedbackFetchTopics($botid, $topicid);
						if (count($topic) == 0) return $entry_not_found;
						if (!$this->db->feedbackTopicCheckAccessGroup($user, $topic)) return $restricted;
						if (!$this->db->feedbackTopicCheckAllowList("votes_allowed", $topic)) return blockquote("Voting on entries is disabled in this topic", "warning");
						$name = issetor($this->db->contactGetName($user));
						$name = ($name) ? $name : $user;
						if ($this->db->feedbackFetchEntryVoteExists($entryid, $user)) {
							$this->db->feedbackEntryVoteDelete("", $user, $entryid);
							$num_votes = count($this->db->feedbackFetchEntryVotes($entryid));
							$curr_votes = "\n  [<strong>$num_votes votes registered</strong>]";
							return "<blockquote class='danger'><strong>-1</strong> : $name <strong>revoked a vote</strong> on entryId: <strong>{$entryid}</strong> in topicId: <strong>$topicid</strong></blockquote><blockquote class='warning'><strong>{$entry[0]['description']}</strong>". $curr_votes . "</blockquote>";
						} else {
							$this->db->feedbackEntryVote($user, $param2);
							$num_votes = count($this->db->feedbackFetchEntryVotes($entryid));
							$curr_votes = "\n  [<strong>$num_votes votes registered</strong>]";
							return "<blockquote class='success'><strong>+1</strong> : $name <strong>voted</strong> on entryId: <strong>{$entryid}</strong> in topicId: <strong>$topicid</strong></blockquote><blockquote class='warning'><strong>{$entry[0]['description']}</strong>". $curr_votes . "</blockquote>";
						}		
					} else {
						return $entry_not_found;
					}
				}
			}
			return $this->db->adminGetFeatureUsage($id)['usage'];
			break;
				
				//FEATURE - WHOAMI
			case "whoami":
				$returnvalue = $access_pending = "";
				$contactinfo = $this->db->contactFetchContacts($user);
				$i=0;
				if (count($contactinfo) > 0) {
					$returnvalue .= "- Name: {$contactinfo[0]['firstName']} {$contactinfo[0]['lastName']}\n- MemberOfGroups:\n ";
					$groups = $this->db->groupFetchGroups();
					foreach ($groups as $key => $value) {
						if ($this->db->groupCheckIfMember($contactinfo[0]['id'], $value['id'])) {
							$returnvalue .=  ">- **{$value['id']}**:{$value['groupname']}\n>>{$value['description']}\n";
							$i++;
						}
					}
					if ($i == 0) $access_pending = "**Access request pending!**";
					$returnvalue .= "Member of **{$i}** groups. {$access_pending}";
					return $returnvalue;
				}
				else {
					return "I could not find {$user} in my database, type **request** and try again.";
				}
				break;
				
				//FEATURE - CHUCK
			case "chuck":
				return $this->generate->getJoke($id);
				break;
				
				//FEATURE - YOMOMMA
			case "yomomma":
				return $this->generate->getJoke($id);
				break;
				
				//FEATURE - JOKE
			case "joke":
				return $this->generate->getJoke($id);
				break;
				
				//FEATURE - WHOIS
			case "whois": //Does a lookup on Name or Email and returns the results
				if (count($feature_attributes) > 0)
				{
					if(validateEmail($feature_attributes[0])){
						$data = array('recepientType' => 'email','recepientValue' => $feature_attributes[0],'sender' => $botid);
					}
					else{
						$search_string = implode(" ", $feature_attributes);
						$data = array('recepientType' => 'displayName','recepientValue' => $search_string,'sender' => $botid);
					}
					$returnvalue = $this->generate->pretty($this->peopleGet($data), "spark", $botid);
					$query = array('recepientType' => 'roomId',
							'recepientValue' => $roomid,
							'sender' => $botid,
							'text' => $returnvalue);
					$this->messageSend($query);
					return "Search completed!";
				}
				break;
				
				//FEATURE - ADMIN
			case "admin":
				if (count($feature_attributes)>0) {
					list($param1, $param2, $param3, $param4) = $feature_attributes;
					if ($param1 == "add"){
						$valid_emails = array();
						$emails = explode(",", $param2);
						foreach ($emails as $key => $value) {
							if(validateEmail($value)) {
								$valid_emails[] = $value;
							} else {
								continue;
							}
						}
						if (count($valid_emails) > 0) {
							return $this->bulkSplitRequestPost("addtolocal",$valid_emails, $botid, 1);
						} else {
							return "No valid e-mails found, **admin add email,email** no spaces between the commas. user@domain.com";
						}
					}
					// ADMIN BLOCK
					elseif ($param1 == "block") {
						// ADMIN BLOCK LIST
						if ($param2 == "list"){
							$blocked_contacts = $this->db->adminGetBlockedContacts();
							$returnvalue = "**Blocked contacts**<br><br>";
							$i=0;
							foreach ($blocked_contacts as $key => $value){
								$i++;
								$returnvalue .= "{$i}: {$value['email']}<br>";
							}
							return ($i == 0) ? "No users are currently blocked" : $returnvalue;
						}
						elseif(validateEmail($param2)) {
							if (!$this->db->adminGetBlockedContacts($param2)) {
								if (($user == $param2)){
									return "You cannot block yourself, that would be dumb..";
								}
								else {
									$this->db->adminBlockContact($param2);
									return "{$param2} is now blocked";
								}
							}
							else return "User is already blocked";
						}
						else return "The email was not valid!";
					}
					// ADMIN UNBLOCK
					elseif ($param1 == "unblock") {
						if(filter_var($param2, FILTER_VALIDATE_EMAIL)) {
							if ($this->db->adminGetBlockedContacts($param2)) {
								$this->db->adminUnblockContact($param2);
								return "{$param2} is now unblocked";
							}
							else return "Cannot unblock a user that is not blocked";
						}
						else return "The email was not valid!";
					}
					// ADMIN MESSAGE
					elseif ($param1 == "message") {
						$report = blockquote("Message report", "info");
						$spaces = $contacts = $emails = [];
						$messageParsel = array();
						$a = 0;
						if ($param2 == "list") {
							if (issetor($param3)) {
								if (is_numeric($param3)) {
									$message = $this->db->messagesLoad($param3);
									if (count($message)){
										return "##".$message[0]['title']."\n".$message[0]['message'];
									}
									else return blockquote("Sorry that message does not exist", "warning");
								}
							}
							$response = blockquote("Stored messages", "info");
							$messages = $this->db->messagesLoad();
							if (count($messages)) {
								foreach ($messages as $key => $value) {
									$messagepart = substr(removeHTML($value['message']), 0, 50);
									$title = substr(removeHTML($value['title']), 0, 50);
									$response .= "\n- **{$value['id']}** : $title\n" . blockquote($messagepart, "info");
								}
								return $response;
							}
							else {
								return blockquote("No stored announcements was found, you can create announcements in the Webex Teams Bot Manager");
							}
						}
						elseif (in_array($param2, array('echo', 'announce'))) {
							if (issetor($param3)) {
								foreach (explode(',', $param3) as $k => $v) {
									if (validateEmail($v)) {
										$emails[] = $v;
										continue;
									}
									$groupinfo = $this->db->groupFetchGroups($v);
									if (count($groupinfo)) {
										$groupid = $groupinfo[0]['id'];
										if ($groupinfo[0]['botid']) {
											if ($groupinfo[0]['botid'] == $botid) {
												$spaces = array_merge($spaces, $this->db->groupGetSpaceMemberIdArray($groupid, $botid));
											}
											else {
												$report .= "\n- Group $v exists but is not owned by me.. skipping!";
												continue;
											}
										}
										$contacts = array_merge($contacts, $this->db->groupGetMemberIdArray($groupid));
									}
									else $report .= "\n- Group $v does not exist!";
								}
							}
							else return blockquote("You must specify at least one groupid or group-alias so I know where to send the message", "warning");
							
							$contacts = array_unique($contacts);
							$spaces = array_unique($spaces);
							if ($param2 == "echo") {
								$message = $this->db->quote(implode(' ', array_slice($feature_attributes, 3)));
							}
							else {
								if (is_numeric(issetor($param4))) {
									$messageinfo = $this->db->messagesLoad($param4);
									if (count($messageinfo)) {
										$title = (issetor($messageinfo[0]['title'])) ? "##".$messageinfo[0]['title']."  \n":"";
										$body = $messageinfo[0]['message'];
										$message = $title . $body;
									}
									else {
										return blockquote("This $param4 is not a valid message ID", "warning");
									}
								}
								else {
									return blockquote("You need to specify the message ID I am supposed to send, it has to be a numeric value.", "warning");
								}
							}
							
							if (trim(issetor($message))) {
								//Generate payloads for the requests
								if (count($contacts)) {
									foreach ($contacts as $key => $value) {
										$messageParsel[$a]['rec'] = $value;
										$messageParsel[$a]['type'] = "toPersonId";
										$messageParsel[$a]['message'] = $message;
										$messageParsel[$a]['files'] = "";
										$a++;
									}
								}
								if (count($spaces)) {
									foreach ($spaces as $key => $value) {
										$messageParsel[$a]['rec'] = $value;
										$messageParsel[$a]['type'] = "roomId";
										$messageParsel[$a]['message'] = $message;
										$messageParsel[$a]['files'] = "";
										$a++;
									}
								}
								if (count($emails)) {
									foreach ($emails as $key => $value) {
										$messageParsel[$a]['rec'] = $value;
										$messageParsel[$a]['type'] = "toPersonEmail";
										$messageParsel[$a]['message'] = $message;
										$messageParsel[$a]['files'] = "";
										$a++;
									}
								}
							}
							else {
								return blockquote("You cannot send a blank message, make sure you specify the attributes correctly and in the correct order", "warning");
							}
							
							if (count($messageParsel)) {
								$report .= $this->bulkSplitRequestPost("messages", $messageParsel, $botid, 1, 50, 150, 15, 100);
							}
							return $report;
						}
						else return $this->db->adminGetFeatureUsage($id)['usage'];
					}
					// ADMIN FEEDBACK
					elseif ($param1 == "feedback") {
						$allow_list = array('comments_allowed', 'votes_allowed', 'entry_create_allowed', 'entry_view_allowed', 'entry_delete_allowed');
						if($param2 == "create") {
							$topic = implode(' ', array_slice($feature_attributes, 2));
							if ($topic) {
								$data = array(
										'botid' => $botid,
										'title' => $topic,
										'comments_allowed' => 1,
										'votes_allowed' => 1,
										'entry_create_allowed' => 1,
										'entry_view_allowed' => 1,
										'entry_delete_allowed' => 1,
										'accessgroup' => 0
								);
								$created_id = $this->db->feedbackTopicAdd($data, true);
								return blockquote("Successfully created a new topic with topicId: <strong>$created_id</strong>", "success");
							} else return blockquote("You need to specifiy a topic title!", "warning");
						}
						elseif (in_array($param2,  $allow_list)) {
							if ($param3) {
								list($topicid, $value) = explode(':',$param3);
								if (in_array($value, array('1', '0'))) {
									if (is_numeric($topicid)) {
										$topic = $this->db->feedbackFetchTopics($botid, $topicid);
										if (count($topic)) {
											$this->db->feedbackTopicUpdate(array($param2 => $value), $topicid);
											return blockquote("<strong>$param2</strong> was set to <strong>$value</strong> in topicId: <strong>$topicid</strong>", "success");
										} return blockquote("Topic does not exist!", "warning");
									} return blockquote("Incorrect topicId", "warning");
								} else return blockquote("Value must be 0 or 1. <strong>topicId:value</strong>", "warning");
							} else return blockquote("Please provide topicId and allow value (0 or 1) <strong>topicId:value</strong>", "warning");
						}
						elseif ($param2 == "topic") {
							if (is_numeric($param3)) {
								$topic = $this->db->feedbackFetchTopics($botid, $param3);
								if (count($topic)) {
									$returnvalue = "";
									foreach ($topic[0] as $k => $v) {
										$returnvalue .= "<strong>$k</strong> : $v <br>";
									}
									return $returnvalue;
								} return blockquote("Topic does not exist!", "warning");
							} return blockquote("Incorrect topicId", "warning");
						}
					}
					// ADMIN MAINTENANCE
					elseif ($param1 == "maintenance") {
						if($param2 == "enable") {
							if ($this->db->adminCheckIfMaintenance()) {
								return "Maintenance mode is already enabled!";
							}
							else {
								$this->db->adminSetMaintenance(1);
								return "Maintenance mode is activated, no external tasks will be created!";
							}
						}
						// ADMIN MAINTENANCE DISABLE
						elseif($param2 == "disable") {
							if (!$this->db->adminCheckIfMaintenance()) {
								return "Maintenance mode is already disabled!";
							}
							else {
								$this->db->adminSetMaintenance(0);
								return "Maintenance mode is deactivated, external tasks will now be created!";
							}
						}
					}
					// ADMIN DELETE
					elseif ($param1 == "delete"){
						$emails = explode(",", $param2);
						$returnvalue = "**Delete report**";
						foreach($emails as $key => $value) {
							if(filter_var($value, FILTER_VALIDATE_EMAIL)) {
								$existing = $this->db->contactFetchContacts($value);
								if (count($existing)>0) {
									if ($value == $user){
										$returnvalue .= "\n- You cannot delete yourself! (<strong>$value</strong>)";
									}
									elseif ($value == "mohm@cisco.com"){
										$returnvalue .= "\n- Are you kidding me? No one can delete my creator!\n";
									}
									else {
										$this->db->contactRemove($existing[0]['id']);
										$returnvalue .= "\n- Successfully removed (<strong>$value</strong>)\n";
									}
								}
								else {
									$returnvalue .= "\n- The user does not exist or has already been deleted! (<strong>$value</strong>)\n";
								}
							}
							else{
								$returnvalue .= "\n- This email address was not valid! (<strong>$value</strong>)\n";
							}
						}
						return $returnvalue;
					}
					// ADMIN GROUP (Base command)
					elseif ($param1 == "group") {
						$returnvalue = $error_report ="";
						// ADMIN GROUP LIST (Lists the added groups)
						if ($param2 == "list") {
							$groups = $this->db->groupFetchGroups();
							$returnvalue = "**GROUPS**<hr>";
							foreach ($groups as $key => $value) {
								$members = $this->db->groupMembershipNumber($value['id']);
								$groupid = ($value['sub_id'] != "") ? $value['sub_id'] : $value['id'];
								$returnvalue .=  "\n- **$groupid**:{$value['groupname']} ($members members)\n>{$value['description']}";
							}
							return $returnvalue;
						}
						// ADMIN GROUP MENTION (Mentions a group of people in a space)
						elseif ($param2 == "mention") {
							if ($param3 != "") {
								if ($type != "group") {
									return "This feature only works in group spaces as I cannot mention people in a 1:1 space";
								}
								$groupinfo = $this->db->groupFetchGroups($param3);
								$text = "\n\n" . implode(' ', array_slice($feature_attributes, 3));
								if (count($groupinfo) > 0) {
									$headline = "<blockquote class='danger'><b>ATTENTION</b> : <b>" . $groupinfo[0]['groupname']. "</b></blockquote>\n";
									$returnvalue = "";
									$members = $this->db->groupGetMembers($groupinfo[0]['id']);
									if (count($members) > 0) {
										foreach ($members as $key => $value) {
											$user = $this->db->contactFetchContacts($value['contactid']);
											$returnvalue .= "<@personEmail:{$user[0]['emails']}|{$user[0]['firstName']}> ";
										}
										return "$headline $returnvalue $text";
									} else { return "This group has no member"; }
								} else { return "No such group"; }
							} else { return $this->db->adminGetFeatureUsage($id)['usage']; }
						}
						// ADMIN GROUP ADDTOSPACE (Adds a group of people to a space)
						elseif ($param2 == "addtospace") {
							$a = 0;
							$memberParsel = array();
							if ($type == "direct") return "This space is a 1:1 space, you cannot add people here";
							if ($param3 != "") {
								$groupinfo = $this->db->groupFetchGroups($param3);
								if (count($groupinfo) > 0) {
									$members = $this->db->groupGetMembers($groupinfo[0]['id']);
									if (count($members) > 0) {
										foreach ($members as $key => $member) {
											$memberParsel[$a]['roomtype'] = "roomId";
											$memberParsel[$a]['id'] = $roomid;
											$memberParsel[$a]['method'] = "personId";
											$memberParsel[$a]['person'] = $member['contactid'];
											$a++;
										}
										// Start request intervals
										return $this->bulkSplitRequestPost("memberships", $memberParsel, $botid, $botr=1);
										// End request intervals
										
									} else return "**{$groupinfo[0]['groupname']}** has 0 members";
								} else return "No such group";
							} else return "Please provide a valid groupid";
						}
						//CONTINUE HERE
						// ADMIN GROUP MEMBERS (View members in group)
						elseif ($param2 == "members") {
							if ($param3 != "") {
								$groupinfo = $this->db->groupFetchGroups($param3);
								if (count($groupinfo) > 0) {
									$headline = "**MEMBERS OF {$groupinfo[0]['groupname']}** <hr>";
									$members = $this->db->groupGetMembers($groupinfo[0]['id']);
									if (count($members) > 0) {
										foreach ($members as $key => $member) {
											$memberinfo = $this->db->contactFetchContacts($member['contactid']);
											$returnvalue .= "\n- {$memberinfo[0]['firstName']} {$memberinfo[0]['lastName']} ({$memberinfo[0]['emails']})";
										}
										return $headline . $returnvalue;
									} else return "**{$groupinfo[0]['groupname']}** has 0 members";
								} else return "No such group";
							} else return "Please provide a valid groupid";
						}
						// ADMIN GROUP CREATE (Create group)
						elseif ($param2 == "create") {
							if ($param3 != "") {
								list($groupname, $subid) = explode(":", $param3);
								if ($groupname != "") {
									$groupname = $this->db->quote($groupname);
									if ($subid != "") {
										$subid = str_replace(" ", "", $subid);
										$subid = $this->db->quote($subid);
									}
									$this->db->groupAdd(array("groupname"=>$groupname,"sub_id"=>$subid, "botid" => "0"));
									return "Created group: $groupname";
								} else return "You need a valid groupname";
							} else return "Please provide the groupname and preferably a short identifier like this: **groupname**:**groupidentifier** (i.e. MY-GROUPNAME:g1)";
						}
						// ADMIN GROUP DELETE (Delete group)
						elseif ($param2 == "delete") {
							if ($param3 != "") {
								$groupid = $this->db->quote($param3);
								$groupinfo = $this->db->groupFetchGroups($param3);
								if (count($groupinfo) > 0) {
									$this->db->groupRemove($groupid);
									return "Group was deleted";
								} else return "This group does not exist!";
							} else return "You must provide a valid group ID";
						}
						elseif ($param2 == "add" or $param2 == "remove") {
							$returnvalue = blockquote("Execution {$param2} report", "info") . "\n";
							list($groupids, $email) = explode(":", $param3);
							$groups = explode(",", $groupids);
							$emails = explode(",", $email);
							foreach ($emails as $key => $value) {
								if(validateEmail($value) or $value == "this") {
									foreach ($groups as $key => $groupid) {
										$groupcheck = $this->db->groupFetchGroups($groupid);
										if (count($groupcheck)>0) {
											$groupid = issetor($groupcheck[0]['id']);
											$groupname = issetor($groupcheck[0]['groupname']);
											$sub_id = issetor($groupcheck[0]['sub_id']);
											$outputid = ($sub_id) ? $sub_id:$groupid;
											$isbotowner = ($botid != $groupcheck[0]['botid']);
											if ($value == "this") {
												$member = $this->db->groupCheckIfSpaceMember($roomid, $groupid, $botid);
												$roomdetails = $this->roomGetDetails($botid, $roomid);
												$roomname = $roomdetails['title'];
												if ($param2 == "remove") {
													if ($member) {
														$this->db->groupRemoveSpace($groupid, $roomid, $botid);
														$returnvalue .= "- **{$roomname}** was removed from **{$groupname}**\n";
														continue;
													} else {
														$returnvalue .= "- **{$roomname}** is not a member of **{$groupname}**\n";
														continue;
													}
												}
												elseif ($param2 == "add") {
													if ($isbotowner) {
														$returnvalue .= "- You can only add spaces to a group that I am the owner of (I do not own the **$groupname** group)\n";
														continue;
													}
													if ($roomdetails['type'] != "group") {
														$returnvalue .= "- You can only add group spaces to my group (this is a **{$roomdetails['type']}** space)\n";
														continue;
													}
													if ($member) {
														$returnvalue .= "- **{$outputid}:** **{$roomname}** is already member of **{$groupname}**\n";
													}
													else {
														$this->db->groupAddSpace(array('groupid' => $groupid, 'spaceid' => $roomid, 'botid' => $botid));
														$returnvalue .= "- **{$outputid}:** **{$roomname}** was added to **{$groupname}**\n";
													}
												}
											}
											else {
												$contactinfo = $this->db->contactFetchContacts($value);
												if (count($contactinfo)>0){
													$contactid = $contactinfo[0]['id'];
													$name = $this->db->contactGetName($contactid);
													$member = ($this->db->groupCheckIfMember($contactid, $groupid));
													if ($param2 == "remove") {
														if($member) {
															$this->db->groupRemoveContact($groupid, $contactid);
															$returnvalue .= "- **{$groupid}:** **{$name}** was removed from **{$groupname}**\n";
														}
														else {
															$returnvalue .= "- **{$name}** is not a member of **{$groupname}**\n";
														}
													}
													elseif ($param2 == "add") {
														if($member) {
															$returnvalue .= "- **{$outputid}:** {$value} is already member of **{$groupname}**\n";
														}
														else {
															$this->db->groupAddContact(array('groupid' => $groupid, 'contactid' => $contactid));
															$returnvalue .= "- Successfully added **{$name}** to **{$groupname}**\n";
														}
													}
												}
												else {
													$returnvalue .= " - **{$value}** does not exist in my database\n";
												}
											}
										}
										else {
											unset($groups[array_search($groupid, $groups)]);
											$returnvalue .= "- The group with id: **{$groupid}** does not exist in my database\n";
										}
									}
								}
								else {
									$returnvalue .= "- **{$value}** is not a valid email\n";
								}
							}
							return $returnvalue;
						}
					}
					// ADMIN JOINABLE
					elseif ($param1 == "joinable") {
						if ($this->db->adminCheckJoinableSpace($botid, $roomid)){
							$this->db->adminRemoveJoinableSpace($botid, $roomid);
							return "This space was removed from joinlist";
						}
						else{
							$roominfo = $this->roomGetDetails($botid, $roomid);
							$title = $roominfo['title'];
							if (empty($title)) {
								$title = "No title";
							}
							$data_array = array("spaceid" => $roomid,
									"spacetitle" => $title,
									"botid" => $botid);
							$this->db->adminAddJoinableSpace($data_array);
							return "This space was added to joinlist";
						}
					}
					// ADMIN USER
					elseif ($param1 == "user") {
						// ADMIN USER LIST
						if ($param2 == "list") {
							$contacts = $this->db->contactFetchContacts();
							$marker = 50;
							$a=0;
							$groups = $this->db->groupFetchGroups();
							$returnvalue = "**USERS**<hr>";
							foreach ($contacts as $key => $value) {
								$groupbulk = "";
								$i = 0;
								foreach ($groups as $key1 => $value1) {
									if ($this->db->groupCheckIfMember($value['id'],$value1['id'])){
										$groupbulk .= ":[**{$value1['id']}**]";
										$i++;
									}
								}
								if ($i == 0) $groupbulk = " - **Pending access**";
								$returnvalue .=  "**{$value['firstName']} {$value['lastName']}**:**{$value['emails']}**{$groupbulk}<br>";
								$a++;
								if ($a == $marker){
									$query = array(	'recepientType' => 'roomId',
											'recepientValue' => $roomid,
											'sender' => $botid,
											'text' => $returnvalue);
									$this->messageSend($query);
									$marker = $a + 50;
									$returnvalue = "";
								}
							}
							return $returnvalue . "<br>Results: " . $a;
						}
					}
					// ADMIN CHECK
					elseif ($param1 == "check") {
						if(filter_var($param2, FILTER_VALIDATE_EMAIL)) {
							$contactinfo = $this->db->contactFetchContacts($param2);
							if (count($contactinfo) > 0) {
								$returnvalue = "- Name: {$contactinfo[0]['firstName']} {$contactinfo[0]['lastName']}\n- MemberOfGroups:<br> ";
								$groups = $this->db->groupFetchGroups();
								$i = 0;
								foreach ($groups as $key => $value) {
									if ($this->db->groupCheckIfMember($contactinfo[0]['id'], $value['id'])) {
										$returnvalue .=  " **{$value['id']}**:{$value['groupname']}<br>";
										$i++;
									}
								}
								if ($i == 0) $access_pending = "  **NOT PART OF ANY GROUPS!**";
								$returnvalue .= "Member of **{$i}** groups. {$access_pending}";
								return $returnvalue;
							}
							else{
								return "The user does not exist in the database";
							}
						}
						else return "Usage: **admin check [user@domain.com]** - *Checks if a user exists in the database*";
					}
					// ADMIN QUEUE
					elseif ($param1 == "queue") {
						if($param2 == "report") {
							return "Number of tasks in the queue: **" . $this->db->adminCheckTaskQueue() . "**";
						}
						// ADMIN QUEUE PURGE
						elseif ($param2 == "purge"){
							$this->db->adminPurgeTaskQueue();
							$number = $this->db->adminCheckTaskQueue();
							return "Task queue was purged, there are now: **" . $number . "** tasks queued" ;
						}
						else return "Usage: **admin queue report** - *Checks how many tasks are in the queue currently*\nUsage: **admin queue purge** - *Deletes all pending tasks in the queue*";
					}
					// ADMIN SPACERESPONSE
					elseif ($param1 == "spaceresponse") {
						if($param2 == "status") {
							$roominfo = $this->roomGetDetails($botid, $roomid);
							$returnvalue = "";
							if ($roominfo['type'] == "group") {
								$roomexcluded = $this->db->adminCheckGroupResponseAcl($botid, $roomid);
								$specialaccess = $this->db->adminCheckUserGroupResponseAcl($botid, $roomid);
								if ($roomexcluded){
									$returnvalue .= "Will I respond to all members of **this** space? **YES!**<br>";
								}
								else {
									$returnvalue .= "Will I respond to all members of **this** space? **NO!**<br>";
								}
								if (count($specialaccess)) {
									foreach ($specialaccess as $key => $value) {
										$contactinfo = $this->db->contactFetchContacts($value['contactid']);
										$returnvalue .= "I answer to **{$contactinfo[0]['firstName']} {$contactinfo[0]['lastName']}** in **this** space<br>";
									}
								}
								else {
									$returnvalue .= "There are **no** single user access enabled for **this** space.";
								}
								return $returnvalue;
							}
							else {
								return "This space is not a group";
							}
						}
						// ADMIN SPACERESPONSE ENABLE
						elseif ($param2 == "enable") {
							$roominfo = $this->roomGetDetails($botid, $roomid);
							if ($roominfo['type'] == "group") {
								if ($this->db->adminCheckGroupResponseAcl($botid, $roomid)) {
									return "I will already respond to everyone in **this** space, bah...";
								}
								else {
									$this->db->adminAddGroupResponseAcl($botid, $roomid, $roominfo['title']);
									if (($this->db->adminCheckGroupResponseAcl($botid, $roomid))) {
										return "I will now respond to everyone in **this** space!";
									}
									else {
										return "Something went wrong when enabling space response, not added";
									}
									
								}
							}
							else {
								return "This space is not a group!";
							}
						}
						// ADMIN SPACERESPONSE [EMAIL]
						elseif(validateEmail($param2)) {
							$roominfo = $this->roomGetDetails($botid, $roomid);
							if ($roominfo['type'] == "group") {
								$contactinfo = $this->db->contactFetchContacts($param2);
								if (count($contactinfo) > 0) {
									if ($this->db->adminCheckUserGroupResponseAcl($botid, $roomid, $contactinfo[0]['id'])) {
										$this->db->adminRemoveUserGroupResponseAcl($botid, $roomid, $contactinfo[0]['id']);
										return "I will no longer answer to **{$contactinfo[0]['firstName']} {$contactinfo[0]['lastName']}** in **this** space!";
									}
									else {
										$this->db->adminAddUserGroupResponseAcl($botid, $roomid, $contactinfo[0]['id']);
										return "I will now answer to **{$contactinfo[0]['firstName']} {$contactinfo[0]['lastName']}** in **this** space!";
									}
								} else return "The user was not found in my database, you need to add the user before granting special access.";
							} else return "This space is not a group!";
						}
						// ADMIN SPACERESPONSE DISABLE
						elseif ($param2 == "disable") {
							$roominfo = $this->roomGetDetails($botid, $roomid);
							if ($roominfo['type'] == "group") {
								if (!$this->db->adminCheckGroupResponseAcl($botid, $roomid)){
									return "I am already **NOT** responding to everyone in **this** space, comeon..";
								}
								else {
									$this->db->adminRemoveGroupResponseAcl($botid, $roomid);
									return "I will no longer respond to everyone in **this space**";
								}
							}
							else {
								return "This space is not a group!";
							}
						}
						else return "$usequote Usage:
						**admin spaceresponse enable** - *Enables all users in the group space to chat with the bot in that particular space*\n
						**admin spaceresponse disable** - *Prevent all users in the group space to chat with the bot in that particular space (default)\n
						**admin spaceresponse user@domain.com** - *Enables that particular user to chat with the bot in that particular space, issue the same command towards the same user will remove the privelege";
					}
					else return $this->db->adminGetFeatureUsage($id)['usage'];
				}
				else return $this->db->adminGetFeatureUsage($id)['usage'];
				break;
				//FEATURE - USAGE
			case "usage":
				$report = $this->db->adminReportLogs($botid);
				$query = array('recepientType' => 'roomId',
						'recepientValue' => $roomid,
						'sender' => $botid,
						'text' => $report);
				$this->messageSend($query);
				return "There are lies, damned lies and statistics. *(Mark Twain)*";
				break;
				//FEATURE - SPACE
			case "space":
				if (count($feature_attributes) == 1) {
					list($param1) = $feature_attributes;
				}
				elseif (count($feature_attributes) > 1) {
					list($param1, $param2) = $feature_attributes;
				}
				else {
					return $this->db->adminGetFeatureUsage($id)['usage'];
				}
				// SPACE LIST
				if ($param1 == "list"){
					$spacelist = $this->db->adminGetJoinableSpace($botid);
					$returnvalue = "**LIST OF JOINABLE SPACES**<br>(**ID**:SPACETITLE)\n";
					foreach ($spacelist as $key => $value) {
						$returnvalue .= "- **{$value['id']}**: {$value['spacetitle']}\n\n";
					}
					$returnvalue .= "**" . count($spacelist) . "** joinable spaces found. Type **space join [ID]**. To join more than one space, you can separate the ID's with comma (no spaces) i.e: **space join 1,2,3**.";
					return $returnvalue;
				}
				// SPACE JOIN
				elseif ($param1 == "join"){
					// SPACE JOIN [PARAM]
					if (isset($param2) and !empty($param2)){
						$returnvalue = "**SPACE JOIN REPORT**\n\n";
						$joinspaces = explode(",", $param2);
						foreach ($joinspaces as $key => $value){
							if ($this->db->adminCheckJoinableSpaceById($botid, $value)){
								$spaceinfo = $this->db->adminGetJoinableSpaceById($botid, $value)[0];
								$this->membershipCreate($botid, $spaceinfo['spaceid'], $user);
								$returnvalue .= "- Added you to {$spaceinfo['spacetitle']}\n";
							}
							else {
								if (empty($value)){
									$returnvalue .= "- ID input: **EMPTY** is not a joinable space (operation was terminated)! please separate id's with commas and do not include spaces i.e: **1,2,3**\n";
								}
								else {
									$returnvalue .= "- ID input: $value is not a joinable space\n";
								}
								
							}
						}
						return $returnvalue;
					}
					else {
						return "There was something wrong with your input! **space join [id]**, please try again.";
					}
				}
				// SPACE ADD
				elseif($param1 == "add") {
					if (isset($param2) and !empty($param2)) {
						$emails = explode(",",$param2);
						$emails = array_unique($emails);
						$a = 0;
						foreach ($emails as $key => $email) {
							if (validateEmail($email)) {
								$memberParsel[$a]['roomtype'] = "roomId";
								$memberParsel[$a]['id'] = $roomid;
								$memberParsel[$a]['method'] = "personEmail";
								$memberParsel[$a]['person'] = $email;
								$a++;
							}
						}
						if (count($memberParsel) > 0) {
							// Start request interval
							return $this->bulkSplitRequestPost("memberships", $memberParsel, $botid, $botr=1);
							// End request interval
						} else return "No valid e-mail addresses in your request";
					} else return "Please provide a CSV list of email addresses (only full e-mail addresses user@domain.com). Separate the emails with comma and no spaces!";
				}
				// SPACE GENCSV - GENERATES A CSV OF SPACE MEMBER EMAILS
				elseif($param1 == "gencsv") {
					$members = $this->membershipGet($roomid, $botid);
					$csv = array();
					$max = 100;
					$a = 1;
					$b = 1;
					foreach ($members['items'] as $key => $member) {
						$csv[] = $member['personEmail'];
						$a++;
						$b++;
						if ($b == $max) {
							$this->messageSend($this->messageBlob(implode(',',$csv), $user, $botid));
							$csv = array();
							$b = 1;
						}
					}
					if (count($csv)) {
						$this->messageSend($this->messageBlob(implode(',',$csv), $user, $botid));
					}
					return "Done, I have pinged you the result 1:1 in bulks of $max ($a total items)";
				}
				// SPACE KICK
				elseif($param1 == "kick") {
					if (isset($param2) and !empty($param2)) {
						$persontokick = $param2;
						if (!validateEmail($persontokick)) return "Please type in a valid e-mail";
						$membership_details = $this->membershipGet($roomid, $botid, $persontokick);
						$membership_id = $membership_details['items'][0]['id'];
						$this->membershipDelete($botid, $membership_id);
						return "removed";
					}
					else return "Who to kick? Usage: **space kick user@domain.com**";
				}
				// SPACE CREATE
				elseif($param1 == "create"){
					//Check is the room that the request came from is part of a team (fetch teamId)
					$space_details = $this->roomGetDetails($botid, $roomid);
					$teamid = (isset($space_details['teamId'])) ? $space_details['teamId']:"";
					$time = get_timestamp('UTC') . "UTC - ";
					$title = $time . "Created by " . $user;
					$newspace = $this->roomCreate($botid, $title, $teamid);
					$this->membershipCreate($botid, $newspace['id'], $user);
					
					//If there the parameter 2 exists we expect either a csv of emails or a title
					if (isset($param2) and !empty($param2)){
						$returnvalue = "**Space creation report:**\n\n";
						$error_report = "";
						$a = 0;
						$csv = $param2;
						$emails = explode(",",$csv);
						$emails = array_unique($emails);
						foreach ($emails as $key => $email) {
							if (validateEmail($email)) {
								$memberParsel[$a]['roomtype'] = "roomId";
								$memberParsel[$a]['id'] = $newspace['id'];
								$memberParsel[$a]['method'] = "personEmail";
								$memberParsel[$a]['person'] = $email;
								$a++;
							}
						}
						if (count($memberParsel) > 0) {
							// Start request interval
							return $this->bulkSplitRequestPost("memberships", $memberParsel, $botid, $botr=1);
							// End request interval
						} else {
							$title = $time . implode(array_slice($feature_attributes, 1), ' ');
							sleep(1);
							$this->roomUpdate($botid, $newspace['id'], $title);
						}
					}
					return "New space was created: " . $title;
				}
				elseif ($param1 == "archive" or $param1 == "delete") {
					$space_details = $this->roomGetDetails($botid, $roomid);
					$teamid = (isset($space_details['teamId'])) ? $space_details['teamId']:"";
					if (isset($param2) and !empty($param2)){
						//Force delete
						if ($param2 == "force") {
							//Check if owner
							if ($space_details['creatorId'] == $botid) {
								$this->roomDelete($botid, $roomid);
							} else return "I cannot delete a space by force if I have not created it";
						}
					}
					//If owner of the space, and the space is part of a team - archive it
					if ($space_details['creatorId'] == $botid) {
						if(!empty($teamid)) {
							$this->roomDelete($botid, $roomid);
						} else return "I cannot delete a space that is not part of a team";
					} else return "I cannot delete a space that is not created by me";
				}
				break;
			default:
				return false;
				break;
		}
	}
	
	public function data_post($options, $api_url) {
		$curl = curl_init();
		$options[CURLOPT_URL] = $api_url;
		curl_setopt_array($curl, $options);
		return json_decode(curl_exec($curl),true);
	}
	
	public function data_multi_post($data) {
		//array(array('url'=>'', 'method'=>'', 'type'=>'', 'auth'=>'', 'post'=>array()))
		$curl = array();
		$result = array();
		$payload = array();
		$execution = null;
		$mh = curl_multi_init();
		
		foreach ($data as $id => $d) {
			$curl[$id] = curl_init();
			$options = $this->build_spark_headers($d['auth'], $d['method'], $d['post'], $d['type']);
			$payload[$id] = $d['post'];
			$options[CURLOPT_URL] = $d['url'];
			$options[CURLINFO_HEADER_OUT] = true;
			curl_setopt_array($curl[$id], $options);
			curl_multi_add_handle($mh, $curl[$id]);
		}
		
		do {
			curl_multi_exec($mh, $execution);
		} while($execution > 0);
		
		foreach($curl as $id => $c) {
			$result[$id]['response'] = json_decode(curl_multi_getcontent($c));
			$result[$id]['http_code'] = curl_getinfo($c, CURLINFO_HTTP_CODE);
			$result[$id]['outgoing_info'] = $payload[$id];
			curl_multi_remove_handle($mh, $c);
		}
		
		curl_multi_close($mh);
		return $result;
	}
	
	public function data_multi_get($data) {
		//array(array('url'=>'', 'method'=>'', 'type'=>'', 'auth'=>'', 'post'=>array()))
		$curl = array();
		$result = array();
		$request = array();
		$execution = null;
		$mh = curl_multi_init();
		
		foreach ($data as $id => $d) {
			$curl[$id] = curl_init();
			$options = $this->build_spark_headers($d['auth'], $d['method']);
			$payload[$id] = $d['get'];
			$options[CURLOPT_URL] = $d['url'].'?'.http_build_query($d['get']);
			$options[CURLINFO_HEADER_OUT] = true;
			curl_setopt_array($curl[$id], $options);
			curl_multi_add_handle($mh, $curl[$id]);
		}
		
		do {
			curl_multi_exec($mh, $execution);
		} while($execution > 0);
		
		foreach($curl as $id => $c) {
			$result[$id]['response'] = json_decode(curl_multi_getcontent($c),true);
			$result[$id]['http_code'] = curl_getinfo($c, CURLINFO_HTTP_CODE);
			$result[$id]['outgoing_info'] = $payload[$id];
			curl_multi_remove_handle($mh, $c);
		}
		curl_multi_close($mh);
		return $result;
	}
	//Check for errors in a multi_post_request and return result
	public function check_multi_error($result) {
		$failed = array();
		$error_html = "";
		foreach ($result as $key => $r) {
			if ($r['http_code'] == '200') {
				//skip
			} else {
				$failed[$key]['http_code'] = $r['http_code'];
				$failed[$key]['response'] = json_encode($r['response']);
				$failed[$key]['outgoing_info'] = $r['outgoing_info'];
			}
		}
		$total_count = count($result);
		$error_count = count($failed);
		$success_count = $total_count - $error_count;
		$success_code = ($success_count < $total_count or $total_count == 0) ? ($success_count == 0) ? "alert" : "warning" : "success";
		if ($error_count > 0) {
			$error_html = "<b>Errors:</b><br><table><tr><td><b/>Response id<td><b/>Response code<td><b/>Server response<td><b/>My payload";
			foreach ($failed as $key => $value) {
				$my_payload = json_encode($value['outgoing_info']);
				$error_html .= "<tr><td>$key<td>{$value['http_code']}<td>{$value['response']}<td>{$my_payload}";
			}
			$error_html .= "</table>";
		}
		$report = array(
				'total' => $total_count,
				'errors' => $error_count,
				'success' => $success_count,
				'success_code' => $success_code,
				'error_report' => $failed,
				'error_html' => $error_html
		);
		return $report;
	}
	
	public function data_get($data, $options, $api_url){
		$curl = curl_init();
		$options[CURLOPT_URL] = $api_url . '?' . http_build_query($data);
		curl_setopt_array($curl, $options);
		return json_decode(curl_exec($curl),true);
	}
}

class OutputEngine {
	
	protected $db;
	
	public function __construct() {
		$this->db = new dB();
	}
	public function prettyPrint($data, $type=''){
		$returnvalue = "<hr><table class='rounded'>";
		if ($type == '') {
			foreach ($data as $key=>$value){
				$returnvalue .= (($key == 'emails') or ($key == 'mentionedPeople')) ? "<tr><td><b>$key:</b><td>$value[0]":"<tr><td><b>$key:</b><td>$value";
			}
		} else if ($type == 'messages') {
			foreach ($data as $key=>$value){
				if ($key == 'toPersonId') {
					$name = $this->db->contactGetName($value);
					$returnvalue .= "<tr><td><b>$key:</b><td>$name";
				}
			}
		}
		
		$returnvalue .= "</table>";
		return $returnvalue;
	}
	//Function for the message feature to display a short message with the remaining messages
	public function shortMessageOutput($name, $remaining, $type){
		$returnvalue = "<hr><table>";
		$returnvalue .= "<tr><td><b>Sent message to $name!</b><td> Remaining $type messages: $remaining";
		$returnvalue .= "</table>";
		return $returnvalue;
	}
	public function responseGenResponses($botid) {
		global $link_confirm;
		global $neg_color;
		$responses = $this->db->responseFetchResponses($botid);
		
		$returnvalue = "<table class='rounded'><tr><td bgcolor='737CA1'><b>KEYWORDS</b><td bgcolor='737CA1'><b>RESPONSES</b><td bgcolor='737CA1'><b>FILE URL</b><td bgcolor='737CA1'><b>TASK</b><td bgcolor='737CA1'><b>FEATURE</b><td bgcolor='737CA1'><b>PROTECTED</b><td bgcolor='737CA1' colspan=2><b>MODIFY</b>";
		foreach ($responses as $key => $value) {
			if ($value['accessgroup'] != 0) {
				$group = $this->db->groupFetchGroups($value['accessgroup']);
				$group_title = (issetor($group[0]['groupname'])) ? $group[0]['groupname']:"404";
				$location = "index.php?id=groups&viewgroup={$value['accessgroup']}";
			}
			else {
				$group_title = "Everyone";
				$location = "#";
			}
			$is_feature = onoff($value['is_feature'],"Feature","Not a Feature");
			$is_task = onoff($value['is_task'],"Task","Not a Task");
			$response = substr($value['response'],0,20) . '...';
			$response = str_replace("<", "", str_replace(">", "", $response));
			$accessgroup = ($group_title == "404") ? warning("This response is protected but the accessgroup does NOT EXIST! Please update the access group for this response") : "<a title='{$group_title}' href='{$location}'>".onoff($value['accessgroup'],"Restricted to {$group_title}","Unrestricted")."</a>";
			$returnvalue .= "<tr><td> <b>{$value['keyword']}</b><td>$response<td>{$value['file_url']}<td>$is_task<td>$is_feature<td>{$accessgroup}<td width='31'><a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=response&edit={$value['id']}'>".actionButton("edit", "Edit")."</a> <td> <a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=response&delete={$value['id']}' {$link_confirm}>".actionButton("delete", "Delete")."</a>";
		}
		$returnvalue .= "</table>";
		
		return $returnvalue;
	}
	public function teamGenSpaces($botid, $data) {
		global $neg_color;
		$returnvalue = "";
		foreach ($data['items'] as $key => $space) {
			$returnvalue .= "<a href='index.php?id=spaces&botid=$botid&viewspace={$space['id']}&spacetype=group'>{$space['title']}</a>";
		}
		return $returnvalue;
	}
	public function roomGenJoinable($botid) {
		global $link_confirm;
		$spaces = $this->db->adminGetJoinableSpace($botid);
		$returnvalue = "<table class='rounded'><tr><td bgcolor='737CA1'><b>Joinable spaces</b><td bgcolor='737CA1'><b>Remove</b><tr>";
		foreach ($spaces as $key => $value) {
			$returnvalue .= "<tr><td>{$value['spacetitle']}<td><a href='index.php?id=administration&sub=spaceoptions&botid=$botid&removespace={$value['spaceid']}'>Remove</a>";
		}
		$returnvalue .= "</table>";
		
		return $returnvalue;
	}
	public function roomGenCheckbox($data, $name, $botid) {
		$options = "";
		foreach ($data['items'] as $key => $value){
			$options .= "<input type='checkbox' name='{$name}' value='{$value['id']}'> <a href='index.php?id=spaces&botid=$botid&viewspace={$value['id']}&spacetype=group'>{$value['title']}</a><br>";
		}
		return $options;
	}
	public function roomGenLinks($data, $botid, $type='') {
		$spaces = "";
		$teamid = "";
		$i = 1;
		foreach ($data['items'] as $key => $value){
			if ($type != "") {
				$value['type'] = $type;
				$teamid = "&teamid={$value['teamId']}";
			}
			$spaces .= "<a class='linkblock' href='index.php?id=spaces&botid=$botid&viewspace={$value['id']}$teamid&spacetype={$value['type']}' title='{$value['type']}'>{$value['title']}</a><br>";
			$i++;
		}
		return $spaces;
	}
	public function roomGenMembership($botid, $data, $spacetype, $teamid="", $mod="") {
		global $neg_color;
		global $infocolor;
		$mailcsv="";
		$remove = "";
		$returnvalue = "<div id='mininav'>
							<table class='rounded'>";
		foreach ($data['items'] as $key => $member) {
			//Check if moderator
			$ismod = ($member['isModerator']) ? true:false;
			//Set title on modlight
			$modtitleTrue = "(isModerator:True) - Click to Demote Moderator";
			$modtitleFalse = "(isModerator:False) - Click to Promote Moderator";
			//Set modlight
			$modlight = onoff($ismod,$modtitleTrue,$modtitleFalse);
			//Gather emails for CSV
			$mailcsv .= $member['personEmail'].', ';
			//If team, set teamid
			$teamid_link = ($teamid != "") ? "teamid=$teamid&":"";
			if ($mod){
				$modlink = ($ismod) ? 	"is-mod: <a href='index.php?id=spaces&botid=$botid&viewspace={$member['roomId']}&{$teamid_link}spacetype=$spacetype&unsetmod={$member['id']}'>$modlight</a>" :
				"is-mod: <a href='index.php?id=spaces&botid=$botid&viewspace={$member['roomId']}&{$teamid_link}spacetype=$spacetype&setmod={$member['id']}'>$modlight</a>";
				$remove = "<li><a href='index.php?id=spaces&botid=$botid&viewspace={$member['roomId']}&{$teamid_link}spacetype=$spacetype&membershipdelete={$member['id']}' style='color: $neg_color;' title='Remove member'>".actionButton('delete', 'Kick',20,20)."</a>";
			} else {
				$modlink = "is-mod: " . $modlight;
				$remove = ($member['personId'] == $botid) ? "<li><a href='index.php?id=spaces&botid=$botid&viewspace={$member['roomId']}&{$teamid_link}spacetype=$spacetype&membershipdelete={$member['id']}' title='Remove member'>".actionButton('delete', 'Kick',20,20)."</a>":"";
			}
			
			$returnvalue .= "<tr>
			<td>
			<b><font color='$infocolor'>{$member['personDisplayName']}</font></b><br>({$member['personEmail']})
			<td>
			<ul>
			$remove $modlink";
		}
		$returnvalue .= "<tr>
		<td>
		CSV member emails:
		<td>
		$mailcsv
		</table></div>";
		return $returnvalue;
	}
	public function teamGenLinks($data, $botid) {
		$teams = "";
		$i = 1;
		foreach ($data['items'] as $key => $value){
			$teams .= "<a class='linkblock' href='index.php?id=spaces&teamspaces&botid=$botid&teamid={$value['id']}&spacetype=teamspaces' title='{$value['name']}'>{$value['name']}</a><br>";
			$i++;
		}
		return $teams;
	}
	public function contactGenProfile($contactid, $type=''){
		global $link_confirm;
		global $neg_color;
		$mainbot = $this->db->botGetMainInfo();
		$botid = $mainbot['id'];
		$contact = $this->db->contactFetchContacts($contactid);
		$groups = $this->db->groupFetchGroups();
		$name = strtoupper($this->db->contactGetName($contactid));
		$groups_member = $groups_not_member = $memberships = "";
		if ($type == 'listmemberships') {
			foreach ($groups as $key => $value) {
				if ($this->db->groupCheckIfMember($contact[0]['id'], $value['id'])) {
					$groups_member .= $this->groupLinks($format="groupremove", $value['id'], $contact[0]['id']);
				}
				else {
					$groups_not_member .= $this->groupLinks($format="groupadd", $value['id'],$contact[0]['id']);
				}
			}
			$memberships = "<tr> <td> Member of groups: <td> <b>{$groups_member}</b> <hr>
			<tr> <td> Available groups: <td> {$groups_not_member} </td> </tr>";
		}
		$image = (!empty($contact[0]['avatar'])) ? $contact[0]['avatar'] : "/images/static/noimagefound.jpeg";
		
		$html = " <table class='rounded compact'> <tr> <td colspan='3'> <a href='index.php?id=contacts&contactid={$contact[0]['id']}'><b>{$name}</b></a>
		<tr> <td rowspan='7' width='155' valign='middle'> <a href='{$contact[0]['avatar']}'><img class='rounded' src='{$image}' width='150' height='150'></a>
		<tr> <td> Options: <td> <div id='input'>
		<form action='#' method='post' enctype='multipart/form-data'>
		<input type='submit' name='update_contact' value='Update user details'>
		<input type='submit' $link_confirm class='cancel' name='delete_contact' value='Delete user'>
		<input type='hidden' name='contactid' value='{$contact[0]['id']}'>
		</form>
		</div>
		<tr> <td> Webex Teams ID: <td> {$contact[0]['id']}
		<tr> <td> Name: <td> {$contact[0]['firstName']} {$contact[0]['lastName']}
		<tr> <td> Email:<td> {$contact[0]['emails']}
		$memberships</table>";
		return $html;
	}
	public function contactGenUserList(){
		global $link_confirm;
		$contact = $this->db->contactFetchContacts();
		$html = "<form action='index.php?id=contacts' onsubmit='loading(\"process\", \"Please wait while updating...\")' method='post' enctype='multipart/form-data'>
		<table class='rounded compact'><tr><td><input type='checkbox' onClick=\"toggle(this,'userlist[]')\"><td colspan='2'>Select all <td><input type='submit' class='cancel' name='bulk_delete' $link_confirm value='Delete'>&nbsp<input type='submit' name='bulk_update' value='Update'>";
		$i = 1;
		foreach ($contact as $key => $value) {
			$headline = strtoupper($value['firstName'] . " " . $value['lastName']);
			$name = (trim($headline) == "") ? $value['emails']:$headline;
			$image = (!empty($value['avatar'])) ? $value['avatar'] : "/images/static/noimagefound.jpeg";
			$html .= "<tr><td><input type='checkbox' name='userlist[]' value='{$value['id']}'><td><b>{$i}.</b><td> <img class='rounded' src='{$image}' width='40' height='40'></a>
			<td> <a class='linkblock' href='index.php?id=contacts&contactid={$value['id']}'><b>{$name}</b></a>
			<td> ({$value['emails']})
			";
			$i++;
		}
		$html .= "</table></form>";
		return $html;
	}
	public function messageConversation($data, $botid) {
		global $link_confirm;
		global $neg_color;
		global $pos_color;
		$botinfo = $this->db->botFetchBots($botid);
		$botmail = $botinfo[0]['emails'];
		$returnvalue = "";
		
		$number_of_items = (isset($data['items'])) ? count($data['items']) : 0;
		if ($number_of_items > 0) {
			for ($i=0; $i<$number_of_items; $i++) {
				if ($data['items'][$i]['personEmail'] == $botmail) {
					$bgcolor = "#3d4f7d";
					$delete = "<a href='index.php?id=spaces&botid={$botid}&messagedelete={$data['items'][$i]['id']}&viewspace={$data['items'][$i]['roomId']}&spacetype=group' {$link_confirm}>" . actionButton('delete', 'Delete message') . "</a>";
					$avatar = "<img src='{$botinfo[0]['avatar']}' class='rounded' height='40' width='40'>";
				}
				else {
					$bgcolor = "#5E9DC8";
					$delete =  $avatar = "";
				}
				$returnvalue .= "<div id='subnav'>
				<table class='rounded' style='background-color: $bgcolor;'>
				<tr>
				<td> $avatar
				
				<td> $delete";
				foreach ($data['items'][$i] as $key => $value) {
					if ($key == 'text' or $key == 'created' or $key == 'personEmail') {
						$returnvalue .= "<tr>
						<td width='100px'>
						<b>{$key}</b><td width='500px' class='wrap'> {$value} ";
					}
				}
				$returnvalue .= "</table></div><br>";
			}
		}
		$returnvalue .= "Number of results: " . $number_of_items;
		return $returnvalue;
	}
	public function userSearch($data) {
		global $gradrul;
		if (isset($data['items']) and count($data['items'])) {
			$returnvalue = "
				<hr>
					<div id='input'>
						<table width='100%'>
							<tr>
								<td align='center'>Avatar<td align='center'>Display name<td>Options";
			foreach ($data['items'] as $key => $value) {
				$image = (issetor($value['avatar'])) ? $value['avatar'] : "/images/static/noimagefound.jpeg";
				$contact_link = (count($this->db->contactFetchContacts($value['id']))) ?
				"<input name='update_contact' value='Update user' type='submit'> <input name='delete_contact' class='cancel' value='Delete' type='submit'>" :
				"<input name='add_contact' value='Add user' type='submit'>";
				$returnvalue .= "
				<tr>
				<td align='center'>
				<a href='{$value['avatar']}'><img class='rounded' src='$image' width='50' height='50'></a>
				<td align='center'>
				<b>{$value['displayName']}</b> ({$value['emails'][0]})
				<td><form id='searchresults' method='post' action='#' enctype='multipart/form-data'>
				<div id='mininav'>
				$contact_link
				<input type='hidden' name='contactid' value='{$value['id']}'>
				</div></form>";
			}
			$returnvalue .= "</table>";
			return $returnvalue;
		} else return "<hr>No results";
		
	}
	public function pretty($data, $type="", $botid="") {
		global $link_confirm;
		global $neg_color;
		global $pos_color;
		
		$returnvalue = "";
		
		$number_of_items = count($data['items']);
		$memberlist = array('0'=>'');
		if ($number_of_items > 0) {
			for ($i=0; $i<$number_of_items; $i++) {
				switch ($type) {
					//Profile is used to pretty print json from the people API in Spark
					case 'profile':
						$image = (issetor($data['items'][$i]['avatar'])) ? $data['items'][$i]['avatar'] :
						"/images/static/noimagefound.jpeg";
						$contact_link = (count($this->db->contactFetchContacts($data['items'][$i]['id']))) ? "<b><a class='linkblock linkblock-border' href='index.php?id=contacts&botid=$botid&update_contact={$data['items'][$i]['emails'][0]}'>Update user</a><a class='linkblock linkblock-cancel' href='index.php?id=contacts&botid=$botid&delete_contact={$data['items'][$i]['id']}' {$link_confirm}>Delete user</a>" :
						"<a class='linkblock linkblock-border' href='index.php?id=contacts&botid=$botid&add_contact={$data['items'][$i]['emails'][0]}'>Add user</a>";
						$returnvalue .= "
						<div id='subnav' class='wide-content'> <table>
						<tr> <td class='tdStyle'><a href='{$data['items'][$i]['avatar']}'><img class='rounded' src='$image' width='50' height='50'></a> <td class='tdStyle-nolist'>
						<b>{$data['items'][$i]['displayName']}</b>
						<tr>
						<td>Options:
						<td>
						<div id='mininav'>
						$contact_link
						</div>";
						break;
					case 'hooks':
						$webhookid = $data['items'][$i]['id'];
						$sync = $this->db->adminCheckWebhookExists($webhookid, $botid);
						
						$resyncmsg = $resync = "";
						
						if (!$sync) {
							$resync = "<input type='submit' value='Re-sync webhook' name='resync'>";
							
						}
						$exists = onoff($sync);
						$disable = ($sync) ? "":"disabled";
						$accessgroup = $this->db->webhookGetAccessGroup($webhookid);
						$groupoptions = $this->groupOptions($accessgroup);
						$resyncmsg = ($sync) ? "This webhook is in sync!":"This webhook is not in sync with the database and triggers will not work. Please re-sync";
						$returnvalue .= "
						<div id='subnav'>
						<table class='rounded'>
						<tr>
						<td class='tdStyle'>
						Options:
						<td>
						<div id='input'>
						<form action='#$webhookid' id='$webhookid' method='post' enctype='multipart/form-data'>
						<table>
						<tr>
						<td>
						<input type='submit' $disable value='Set accessgroup' name='webhook_setaccess'>
						<td>
						<input type='hidden' value='$webhookid' name='webhookid'>
						<select $disable name='whgroup'>
						<option value='0'>No access group selected</option>
						$groupoptions
						</select>
						<tr>
						<td>
						$resync
						<td>
						<input type='submit' value='Delete webhook' name='delete_webhook' {$link_confirm} class='cancel'>
						
						</table></div></form>
						<tr>
						<td>Database sync: <td> $exists $resyncmsg
						";
						break;
					case 'member':
						$returnvalue .= "
						<div id='subnav'>
						<table class='rounded'><tr><td class='tdStyle'>Options: <td> <a href='index.php?id=spaces&botid={$botid}&membershipdelete={$data['items'][$i]['id']}&viewspace={$data['items'][$i]['roomId']}&spacetype=group' {$link_confirm}>Delete membership</a>
						";
						break;
					case 'listmsg':
						$returnvalue .= "
						<div id='subnav'>
						<table class='rounded'><tr><td class='tdStyle'>Options: <td> <a href='index.php?id=spaces&botid={$botid}&messagedelete={$data['items'][$i]['id']}&viewspace={$data['items'][$i]['roomId']}&spacetype=group' {$link_confirm}>Delete message</a>
						";
						break;
					case 'spark':
						$a = $i+1;
						$returnvalue .= "**Lookup result number: {$a}**\n\n";
						break;
						//List rooms in pretty form, including a differentiator between groups and direct.
					case 'rooms':
						$returnvalue .= "
								<div id='subnav'>
									<table class='rounded'><tr><td class='tdStyle'>Options: <td>";
						//If the room is a group, give option to leave room, enable group response for all users or specific user
						if($data['items'][$i]['type'] == "group") {
							$returnvalue .= "<a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=rooms&delete={$data['items'][$i]['id']}' {$link_confirm}>Leave Room</a> - ";
							$response_changer = ($this->db->adminCheckGroupResponseAcl($botid, $data['items'][$i]['id'])) ? "disable" : "enable";
							$returnvalue .= "<a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=rooms&group_response={$response_changer}&spaceId={$data['items'][$i]['id']}'>{$response_changer} group response</a>";
							if ($response_changer == "enable") {
								$returnvalue .= "<form method='post' action='index.php?id=bots&sub=profile&botid={$botid}&apiq=rooms&add_special_access={$data['items'][$i]['id']}' enctype='multipart/form-data'>
								<input type='text' placeholder='email' required='' name='user'> <input type='submit' name='add_access' value='Grant group response'>
								</form>";
							}
						}
						elseif($data['items'][$i]['type'] == "direct")
						{
							$returnvalue .= "<a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=rooms&list_messages={$data['items'][$i]['id']}'>Get messages</a> - ";
						}
						$returnvalue .= "<a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=messages&recepient={$data['items'][$i]['id']}&recepientType=roomId'>Send message</a>";
						break;
					default:
						$returnvalue .= "
									<table class='smallform'>
									";
						break;
				}
				foreach($data['items'][$i] as $key => $value) {
					if ($type == "spark")  {
						$expanson = "-";
						if ($key == 'emails') {
							if ($value[0] == "mohm@cisco.com") {
								$returnvalue .= "**My beloved almighty creator <3**\n\n";
							}
							$returnvalue .= "{$expanson} **E-mail**: {$value[0]}\n";
						}
						elseif ($key == 'displayName') {
							$returnvalue .= "{$expanson} **Name**: {$value}\n";
						}
						elseif ($key == 'status') {
							$returnvalue .= "{$expanson} **Status**: {$value}\n";
						}
						elseif ($key == 'lastActivity') {
							$returnvalue .= "{$expanson} **Last seen active**: {$value}\n";
						}
						elseif ($key == 'avatar') {
							$returnvalue .= "{$expanson} **Image Avatar**: [Avatar]({$value})\n";
						}
						elseif ($key == 'orgId') {
							$returnvalue .= "{$expanson} **OrgId**: {$value}\n";
						}
						elseif ($key == 'type') {
							$returnvalue .= "{$expanson} **User type**: {$value}\n";
						}
					}
					elseif ($type == "member"){
						if ($key == 'personEmail' or $key == 'personDisplayName'){
							$returnvalue .= "<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>: {$value}</li> ";
							if ($key == 'personEmail') {
								$memberlist[0] .= $value . ",<br>";
							}
						}
						
					}
					elseif ($type == "listmsg"){
						if ($key == 'text' or $key == 'created' or $key == 'personEmail'){
							$returnvalue .= "<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>: {$value}</li> ";
						}
						elseif ($key == 'files'){
							$returnvalue .= "<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>: {$value[0]}</li>";
						}
					}
					else {
						if ($key == 'phoneNumbers') continue;
						$returnvalue .= (($key == 'emails') or ($key == 'mentionedPeople')) ? "<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>: {$value[0]}</li>" :
						"<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>: {$value}</li>";
					}
				}
				$returnvalue .= ($type != "spark") ? "</table>" : "<hr>\n\n";
			}
		}
		else {
			$returnvalue = "Number of results: " . $number_of_items;
		}
		if ((strlen($returnvalue) > 7000) and $type == "spark") {
			$returnvalue = "Too many results to be displayed! Please try again with a more narrow search. Search for name or email.";
		}
		elseif ($number_of_items > 0) {
			$returnvalue .= "Displaying <b>{$i}</b> entries!<br>";
			if ($type == "member") {
				$returnvalue .= "<br><br>";
				$returnvalue .= $memberlist[0];
			}
		}
		return $returnvalue;
	}
	public function feedbackGenTopics($botid, $format="") {
		$topics = $this->db->feedbackFetchTopics($botid);
		$html = "";
		foreach ($topics as $key => $value) {
			$html .= "<a href='index.php?id=feedback&botid=$botid&topic={$value['id']}' class='linkblock'>{$value['title']}</a><br>";
		}
		return $html;
	}
	//Generate topic entries
	public function feedbackGenTopicEntries($botid, $topicid, $format="") {
		$entries = $this->db->feedbackFetchTopicEntries($topicid);
		$html = "<table class='rounded compact'><tr><td><input type='checkbox' onClick=\"toggle(this, 'entries[]')\"> <b/>Select all<td><b/>Text<td><b/>Votes<td><b/>Comments<td><b/>Created by";
		foreach ($entries as $key => $value) {
			$votes_num = count($this->db->feedbackFetchEntryVotes($value['id']));
			$comments_num = count($this->db->feedbackFetchEntryComments($value['id']));
			$desc = (strlen($value['description'])>50) ? substr($value['description'], 0, 50) . "..." : $value['description'];
			$html .= "<tr><td><input type='checkbox' name='entries[]' value='{$value['id']}'><td><a href='index.php?id=feedback&botid={$botid}&topic={$topicid}&entry={$value['id']}#entry' class='linkblock'>$desc</a><td>$votes_num<td>$comments_num<td>{$value['created_by']}";
		}
		return $html;
	}
	//Generate topic entry comments
	public function feedbackGenEntryComments($entryid, $format="") {
		global $com_color1, $com_color2;
		$html = "";
		$comments = $this->db->feedbackFetchEntryComments($entryid);
		if (count($comments)) {
			$html = "<form action='".formUrl($_GET)."#del_c' method='post' id='del_c' enctype='multipart/form-data'><table class='rounded compact' class='' style='background-color: $com_color1'><tr><td><input type='checkbox' onClick=\"toggle(this, 'comments_selected[]')\"> <b/>Select all <td><input type='submit' class='cancel' name='comments_delete' value='Delete selected comments'></table>";
			foreach ($comments as $key => $value) {
				$name = $this->db->contactGetNameLink($value['email']);
				$comment = $value['comment'];
				$html .= "<br><table class='rounded compact' style='background-color: $com_color1'><tr><td><input type='checkbox' name='comments_selected[]' value='{$value['id']}'><td>$name <td>{$value['created']}<tr><td><td colspan=2>$comment</table>";
			}
			$html .= "</form>";
		}
		return $html;
	}
	//Generate topic entry votes
	public function feedbackGenEntryVotes($entryid, $format="") {
		global $com_color1, $com_color2;
		$votes = $this->db->feedbackFetchEntryVotes($entryid);
		$html = "";
		if (count($votes) > 0) {
			$html .= "<form action='".formUrl($_GET)."#del_v' id='del_v' method='post' enctype='multipart/form-data'><table class='rounded compact' style='background-color: $com_color1'><tr>
			<td><input type='checkbox' onClick=\"toggle(this, 'votes_selected[]')\"> <b>Select all</b><td><input type='submit' class='linkblock cancel' name='votes_delete' value='Delete selected votes'> </table>";
			foreach ($votes as $key => $value) {
				$contactinfo = $this->db->contactFetchContacts($value['email']);
				$n = $this->db->contactGetName($value['email']);
				$name = (count($contactinfo)) ? "<a href='index.php?id=contacts&contactid={$contactinfo[0]['id']}' title='{$value['email']}'>".$n."</a>" : $n;
				$html .= "<br><table class='rounded compact' style='background-color: $com_color1'><tr>
				<td><input type='checkbox' name='votes_selected[]' value='{$value['id']}'>
				<td>$name
				<td>{$value['voted']} <td>".onoff(true, "Voted")."</table>";
				
			}
		}
		return $html;
	}
	public function groupOptions($groupid="") {
		$html = "";
		$groupinfo = $this->db->groupFetchGroups();
		foreach ($groupinfo as $key => $value) {
			$number = $this->db->groupMembershipNumber($value['id']);
			$selected = ($groupid == $value['id']) ? "selected":"";
			$html .= "<option $selected value='{$value['id']}'>{$value['groupname']}({$number})</option>";
		}
		return $html;
	}
	public function groupLinks($format="", $groupid="", $contactid="", $botid="") {
		global $pos_color, $neg_color, $infocolor, $warningcolor;
		
		$html = "";
		$groupinfo = $this->db->groupFetchGroups($groupid);
		switch ($format) {
			case 'nestedgroups':
				//Generates a list of links with a checkbox adapted for the nested groups, the name of the array is groups[]
				$groupinfo = $this->db->groupFetchGroups();
				$membergroups = $this->db->groupGetGroupMembers($groupid);
				foreach ($groupinfo as $key => $groupvalue) {
					$actual_members = $this->db->groupUserMembershipNumber($groupvalue['id']);
					$total_members = $this->db->groupMembershipNumber($groupvalue['id']);
					$virtual_members = $total_members - $actual_members;
					
					$act = colorize_value($infocolor, $actual_members, "Actual members to be linked");
					$tot = colorize_value($infocolor, $total_members, "Total members");
					$virt = colorize_value($infocolor, ($virtual_members >= 0) ? $virtual_members:"0", "Members from other groups");
					
					if ($groupvalue['id'] == $groupid) continue;
					if ($actual_members == 0) continue;
					$checked = "";
					foreach ($membergroups as $key => $v) {
						if ($v['nestedid'] == $groupvalue['id']) {
							$checked = "checked";
						}
					}
					$number = colorize_value($infocolor, $this->db->groupMembershipNumber($groupvalue['id']));
					$html .= ($groupvalue['botid'] == "0") ? "<input type='checkbox' {$checked} name='groups[]' value='{$groupvalue['id']}'> <a href='index.php?id=groups&viewgroup={$groupvalue['id']}'>{$groupvalue['groupname']}</a> ({$act})<br>":"";
				}
				break;
			case 'optionlinks':
				//Generates a list of links with a checkbox, the name of the array is groups[]
				$bots = $this->db->botFetchBots();
				foreach ($groupinfo as $key => $groupvalue) {
					$number = colorize_value($infocolor, $this->db->groupMembershipNumber($groupvalue['id']));
					$html .= ($groupvalue['botid'] == "0") ? "<input type='checkbox' name='groups[]' value='{$groupvalue['id']}'> <a href='index.php?id=groups&viewgroup={$groupvalue['id']}'>{$groupvalue['groupname']}</a> ({$number})<br>":"";
				}
				if ($botid != "" and count($this->db->groupGetGroupOwner($botid))>0) {
					$num_groups = colorize_value($infocolor, count($groupinfo = $this->db->groupGetGroupOwner($botid)));
					$botname = $this->db->botFetchBots($botid)[0]['displayName'];
					$html .= "<br><b>{$botname}'s groups:</b><br>";
					foreach ($groupinfo as $key => $groupvalue) {
						$user_number = colorize_value($infocolor, $this->db->groupMembershipNumber($groupvalue['id']), "Number of users");
						$space_number = colorize_value($pos_color, $this->db->groupSpaceMembershipNumber($groupvalue['id'], $botid), "Number of spaces");
						$html .= "<input type='checkbox' name='groups[]' value='{$groupvalue['id']}'> <a href='index.php?id=groups&viewgroup={$groupvalue['id']}'>{$groupvalue['groupname']}</a> ({$user_number}|{$space_number})<br>";
					}
				}
				elseif ($botid == "all") {
					foreach ($bots as $key => $botvalue) {
						if (count($groupinfo = $this->db->groupGetGroupOwner($botvalue['id']))>0) {
							$html .= "<br><b>{$botvalue['displayName']} ($num_groups):</b><br>";
							foreach ($groupinfo as $key => $groupvalue) {
								$number = colorize_value($infocolor, $this->db->groupMembershipNumber($groupvalue['id']));
								$html .= "<input type='checkbox' name='groups[]' value='{$groupvalue['id']}'> <a href='index.php?id=groups&viewgroup={$groupvalue['id']}'>{$groupvalue['groupname']}</a> ({$number})<br>";
							}
						}
					}
				}
				break;
			case 'linksnumbers':
				//Generates a list of group links with the amount of members. It sorts out the link list based on the owner of the group.
				$bots = $this->db->botFetchBots();
				$html .= "<table class='rounded compact'><tr><td><b/>Public groups<td><b/>Members<td><b/>Default<td><b/>Subscribable";
				foreach ($groupinfo as $key => $groupvalue) {
					$tot_mem_num = $this->db->groupMembershipNumber($groupvalue['id']);
					$actual_mem_num = $this->db->groupUserMembershipNumber($groupvalue['id']);
					$linked_mem_num = ($actual_mem_num > 0) ? $tot_mem_num - $actual_mem_num : $tot_mem_num;
					
					$actual_mem_output = colorize_value($infocolor, "<b>$actual_mem_num</b>", "Actual members");
					$tot_mem_output = colorize_value($warningcolor, "<b>$tot_mem_num</b>", "Total users in this group");
					$linked_mem_output = colorize_value($neg_color, "<b>$linked_mem_num</b>", "Users linked to this group");
					
					$def_group = ($groupvalue['default_group']) ? onoff(true,"Default group"):"";
					$sub_group = ($groupvalue['subscribable']) ? onoff(true,"Subscribable group"):"";
					
					$html .= ($groupvalue['botid'] == "0") ? "<tr><td><a class='linkblock' href='index.php?id=groups&viewgroup={$groupvalue['id']}' title='{$groupvalue['description']}'>{$groupvalue['groupname']}</a><td>{$actual_mem_output} | {$linked_mem_output} | {$tot_mem_output} <td> $def_group<td> $sub_group\n":"";
				}
				foreach ($bots as $key => $botvalue) {
					if (count($groupinfo = $this->db->groupGetGroupOwner($botvalue['id']))>0) {
						$html .= "<tr><td><b>{$botvalue['displayName']}</b><td><td><td><td>";
						foreach ($groupinfo as $key => $groupvalue) {
							$tot_mem_num = $this->db->groupMembershipNumber($groupvalue['id']);
							$actual_mem_num = $this->db->groupUserMembershipNumber($groupvalue['id']);
							$linked_mem_num = ($actual_mem_num > 0) ? $tot_mem_num - $actual_mem_num : $tot_mem_num;
							$space_mem_num = $this->db->groupSpaceMembershipNumber($groupvalue['id'], $botvalue['id']);
							
							$actual_mem_output = colorize_value($infocolor, "<b>$actual_mem_num</b>", "Actual members");
							$tot_mem_output = colorize_value($warningcolor, "<b>$tot_mem_num</b>", "Total users in this group");
							$linked_mem_output = colorize_value($neg_color, "<b>$linked_mem_num</b>", "Users linked to this group");
							$space_mem_output = colorize_value($pos_color, "<b>$space_mem_num</b>", "Spaces in this group");
							
							$def_group = ($groupvalue['default_group']) ? onoff(true,"Default group"):"";
							$sub_group = ($groupvalue['subscribable']) ? onoff(true,"Subscribable group"):"";
							
							$html .= "<tr><td><a class='linkblock' href='index.php?id=groups&viewgroup={$groupvalue['id']}&botid={$botvalue['id']}' title='{$groupvalue['description']}'>{$groupvalue['groupname']}</a> <td>{$actual_mem_output} | {$linked_mem_output} | {$tot_mem_output} | {$space_mem_output}<td> $def_group <td> $sub_group";
						}
					}
				}
				$html .= "</table>";
				break;
			case 'options':
				foreach ($groupinfo as $key => $value) {
					$number = $this->db->groupMembershipNumber($value['id']);
					$selected = ($groupid == $value['id']) ? "selected":"";
					$html .= "<option $selected value='{$value['id']}'>{$value['groupname']}({$number})</option>";
				}
				break;
			case "groupadd":
				foreach ($groupinfo as $key => $value) {
					$html .= "<a href='index.php?id=contacts&add&group={$value['id']}&contactid={$contactid}' style='color: $neg_color;' title='Click to add user to this group'>{$value['groupname']}</a></font><br>";
				}
				break;
			case "groupremove":
				$linked = "";
				$title = "Click to remove from group";
				foreach ($groupinfo as $key => $value) {
					$pc = $pos_color;
					if (!$this->db->groupCheckIfActualMember($contactid, $groupid)) {
						$linked_group_info = $this->db->groupCheckContactGroupLink($groupid, $contactid);
						$pc = $infocolor;
						$linked = " - Linked via {$linked_group_info[0]['groupname']}";
						$title = "Cannot remove from group (must remove from host group or unlink)";
					}
					$html .= "<a href='index.php?id=contacts&remove&group={$value['id']}&contactid={$contactid}' style='color: $pc;' title='{$title}'>{$value['groupname']}</a>$linked<br>";
				}
				break;
			default:
				return $groupinfo;
				break;
		}
		return $html;
	}
	public function logsPrint($botid='', $limit='50'){
		$logs = $this->db->logsGet($botid, $limit);
		$num = count($logs);
		$returnvalue = "<table class='rounded'><tr><td>Total results: <b>{$num}</b></table><br><table class='rounded'><tr><td><b>User</b><td><b>Bot</b><td class='tdStyle'><b>Payload</b><td><b>Date</b><tr>";
		foreach ($logs as $key) {
			$botinfo = $this->db->botFetchBots($key['botid'])[0];
			$returnvalue .= "<td class='tdStyle'>{$key['user']}<td><a href='index.php?".http_build_query(array_merge($_GET, array("botfilter"=>$botinfo['id'])))."'>{$botinfo['emails']}</a><td class='wrap'>{$key['command']}<td>{$key['date']}<tr>";
		}
		$returnvalue .= "<td colspan='4'>Total results: <b>{$num}</b></table>";
		return $returnvalue;
	}
	public function getJoke($type){
		if ($type == "chuck"){
			$url = "https://api.chucknorris.io/jokes/random";
			$returnvalue = 'value';
		}
		elseif ($type == "yomomma"){
			$url = "http://api.yomomma.info";
			$returnvalue = 'joke';
		}
		elseif ($type == "joke"){
			$url = "https://icanhazdadjoke.com/";
			$returnvalue = 'joke';
		}
		$options = array('http' => array(
				'header' => 'Accept: application/json',
				'method' => 'GET'),);
		$result = json_decode(file_get_contents($url, false, stream_context_create($options)), true);
		return $result[$returnvalue];
	}
	public function botGenProfile($data) {
		$headline = strtoupper($data['displayName']);
		$main = onoff($data['main']);
		$html = "<div id='input'><h1><a href='index.php?id=bots'>MY BOTS</a> > {$headline}</h1><hr class='gradient'><table>
		<tr> <td rowspan='5'> <img class='rounded' src='{$data['avatar']}' width='150' height='150'>
		<tr> <td> Name: <td> {$data['displayName']}</a>
		<tr> <td> Email: <td> {$data['emails']}
		<tr> <td> Is primary: <td> $main
		</td></tr></table></div>";
		return $html;
	}
	public function botGenDropdown($name, $botid='') {
		$bots = $this->db->botFetchBots();
		$returnvalue = "<select name='$name'><option value='0'>Bot owner (None)</option>";
		foreach ($bots as $key => $value) {
			$returnvalue .= ($botid == $value['id']) ? "<option value='{$value['id']}' selected>{$value['displayName']}</option>":"<option value='{$value['id']}'>{$value['displayName']}</option>";
		}
		$returnvalue .= "</select>";
		return $returnvalue;
	}
	public function botGenSelector($type, $botid="") {
		$bots = $this->db->botFetchBots();
		$returnvalue = false;
		foreach ($bots as $key => $value) {
			if ($botid == $value['id']) {
				echo "<a class='linkblock linkblock-selected' href='index.php?id=$type&botid={$value['id']}'><img class='rounded' title='{$value['displayName']} (SELECTED)' height='40' width='40' src='{$value['avatar']}'></a> ";
				$returnvalue = $value;
			}
			else {
				echo "<a class='linkblock' href='index.php?id=$type&botid={$value['id']}'><img class='rounded' title='{$value['displayName']}' height='35' width='35' src='{$value['avatar']}'></a>";
			}
		}
		
		return $returnvalue;
	}
}

?>