<?php 
$headlineList = array (
					'alert' 	=> 'Alert!',
					'warning' 	=> 'Warning!',
					'info' 		=> 'Info!',
					'success' 	=> 'Success!',
					'help' 		=> 'Query successful'
		);
$messageList = array(
					'alert-Generic' 	=> 'The request failed!',
					'alert-Delete' 		=> 'Failed to delete content!',
					'alert-Query' 		=> 'Query failed!',
					'alert-BotAddAccess'=> 'Failed to add bot, please check that the accesstoken is valid!',
					'alert-Login'		=> 'Login failed! Please try again. If you have forgot your password type your e-mail address in the email field and press <b>Forgot Password</b>',
					'warning-Email'		=> 'You need to type in a valid e-mail address in the e-mail field',
					'warning-botEmail'	=> 'You need to type in a valid bot e-mail address in the e-mail field (bot@sparkbot.io)',
					'warning-AddFailed'	=> 'Data was not added to database, please try again!',
					'warning-PwMismatch'	=> 'The passwords did not match!',
					'warning-InvalidDomain' => 'Not a valid domain, please type in a valid domain!',
					'warning-UserNotFound'=> 'Could not find any user based on your input data, verify and try again!',
					'warning-UserNotFound1'=> 'User was not found, maybe the user do not exist in Webex Teams anymore?',
		            'warning-NoReceivers'=> 'No recepients was selected, aborting!',
					'success-PwReset'	=> 'If this e-mail was valid a new password should now be sent to your Spark user from the Primary bot of this system. Make sure you change the password back once you have logged in again',
					'success-Generic' 	=> 'The request succeeded!',
					'success-Query' 	=> 'Query successful',
					'success-Wizard' 	=> 'Wizard completed, a password should have been sent to the added Spark account! Click the "Login" button to procceed.',
					'success-BotAdd'	=> 'Bot added successfully!',
                    'success-DeleteGroup' => 'Group deleted successfully',
					'success-UserUpdate'	=> 'User updated successfully!',
					'success-UserAdd'	=> 'User added successfully!',
				    'warning-groupNestedBot' => 'Another group is getting members from this group, groups with a bot owner cannot have member links'		
		  );
?>