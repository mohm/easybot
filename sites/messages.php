<?php 
if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 
$botid = $executor = $disable = $editchecked = "";
$rooms_selected = $contacts_selected = $rooms_raw = $contacts_raw = array();
$savemsg = "Save";
$savevalue = "Save";

if (empty($_GET['botid'])) {
	$disable = "disabled";
}
else {
	$botid = $db_local->quote($_GET['botid']);
	$botid_link = "&botid=" . $botid;	
	if (isset($_GET['delete_saved'])) {
		$db_local->messagesDelete($db_local->quote($_GET['delete_saved']));
	}
	if (isset($_POST['Save']) or isset($_POST['Edit'])){
		$save_data=array('message' => $db_local->quote($_POST['message']), 'files' => $_POST['files'], 'title' => $db_local->quote($_POST['title']));
		(isset($_POST['Save'])) ? $db_local->messagesSave($save_data) : $db_local->messagesUpdate($save_data, $_POST['update']);
		$die = 1;
	}
	if (isset($_GET['edit_saved'])) {
		$msgedit = $db_local->messagesLoad($db_local->quote($_GET['edit_saved']));
		$savemsg = "Edit";
		$savevalue = "Save changes";
		$editchecked = (issetor($_GET['c']) == '1') ? "checked":"";
	}
	
	$direct_rooms = $spark->roomGet(array("sender"=>$botid,"max"=>"5000","type"=>"direct"));
	
}
?>
<?php
if (isset($_POST['retry_messages'])) {
	$a = 0;
	foreach ($_POST['personids'] as $key => $value) {
		$messageParsel[$a]['rec'] = $value;
		$messageParsel[$a]['type'] = "toPersonId";
		$messageParsel[$a]['message'] = $_POST['messagepayload'];
		if (!empty($_POST['files'])) $messageParsel[$a]['files'] = $_POST['files'];
		$a++;
	}
	//Start request intervals
	echo $spark->bulkSplitRequestPost("messages", $messageParsel, $botid, 0, 50, 150, 15, 100);
	//End request intervals
}
if (isset($_POST['submit'])) {
	if (isset($die)) die();
	$a = 0;
	
	if (isset($_POST['contacts']) and (count($_POST['contacts']))) {
		foreach ($_POST['contacts'] as $key => $value) {
			$contacts_raw[] = $value;
		}
	}			
	if (isset($_POST['groups']) and (count($_POST['groups']))) {
		foreach ($_POST['groups'] as $key => $value) {
			$group_details = $db_local->groupFetchGroups($value);
			if ($group_details[0]['botid']) {
				$space_members = $db_local->groupGetSpaceMembers($value, $group_details[0]['botid']);
				foreach ($space_members as $spacekey => $spacevalue) {
					$rooms_raw[] = $spacevalue['spaceid'];
				}
			}
			$members = $db_local->groupGetMembers($value);
			foreach ($members as $key => $value) {
				$contacts_raw[] = $value['contactid'];
			}
		}
	}
	if (isset($_POST['rooms']) and (count($_POST['rooms']))) {
		foreach ($_POST['rooms'] as $key => $value) {
			$rooms_raw[] = $value;
		}
	}	
	if(isset($_GET['botid'])) {
		$direct_rooms = $spark->roomGet(array("sender"=>$botid,"max"=>"5000","type"=>"direct"));
	}
	//Clean duplicate entries if any
	$contacts_selected = (count($contacts_raw) > 0) ? array_unique($contacts_raw) : array();
	$rooms_selected = (count($rooms_raw) > 0) ? array_unique($rooms_raw) : array();
	$messageParsel = array();
	
	if (isset($_POST['alldirect']) and $_POST['alldirect'] == 1){
		foreach ($direct_rooms['items'] as $key => $value){
			$messageParsel[$a]['rec'] = $value;
			$messageParsel[$a]['type'] = "roomId";
			$messageParsel[$a]['message'] = "##" . $_POST['title'] . "##\n" . $_POST['message'];
			if (!empty($_POST['files'])) $messageParsel[$a]['files'] = $_POST['files'];
			$a++;
		}
	}
	else {	
		if (count($contacts_selected)) {
			foreach ($contacts_selected as $key => $value) {
				$messageParsel[$a]['rec'] = $value;
				$messageParsel[$a]['type'] = "toPersonId";
				$messageParsel[$a]['message'] = "##" . $_POST['title'] . "##\n" . $_POST['message'];
				if (!empty($_POST['files'])) $messageParsel[$a]['files'] = $_POST['files'];
				$a++;
			}
		}
	}
	if (count($rooms_selected) > 0) {
		foreach ($rooms_selected as $key => $value) {
			$messageParsel[$a]['rec'] = $value;
			$messageParsel[$a]['type'] = "roomId";
			$messageParsel[$a]['message'] = "##" . $_POST['title'] . "##\n" . $_POST['message'];
			if (!empty($_POST['files'])) $messageParsel[$a]['files'] = $_POST['files'];
			$a++;
		}
	}
	if (count($messageParsel)) {
	//----------------------- Message request execute -----------
	
	//Start request intervals
	echo $spark->bulkSplitRequestPost("messages", $messageParsel, $botid, 0, 50, 150, 15, 100);
	//End request intervals
	} else $feedback = msgList("warning-NoReceivers");
}
?>
		<?php echo issetor($feedback); ?><h1>MESSAGES <?php echo (issetor($disable)) ? ": Select a bot to send messages":"";?></h1><?php echo $gradrul; ?>
		<table> 
			<tr> 
			<td colspan=2>
		<form id='message' method='post' onsubmit='loading("process", "Please wait...")' action='#' enctype='multipart/form-data'>	
		<table class='rounded compact'>
			<tr>
				<td>
					
						<?php 
						$selected = $generate->botGenSelector('messages', issetor($botid));
						
						if ($selected) {
							$executor = "<b>{$selected['displayName']}</b>";
							$executor_avatar = "<img class='rounded' title='{$selected['displayName']} (SELECTED)' height='35' width='35' src='{$selected['avatar']}'>";
						}
						
						$disable = (empty($executor)) ? "disabled" : "";
						
						?>
				</td></tr></table>
			<td>
			
			<tr> 
			<td valign='top' width='70%'> 
			<div id='input'>
			<table class='rounded compact'>
				<tr>
					<td>
						Headline:
							<td>
								<input type='text' <?php echo $disable; ?> placeholder='Headline' name='title' value="<?php echo issetor($msgedit[0]['title'])?>">
				<tr> 
					<td> 
						Message:
							<td> 
								<textarea required='' placeholder='Text to send' name='message' <?php echo $disable; ?> rows='20' cols='60'><?php echo issetor($msgedit[0]['message']); ?></textarea>
				<tr> 
					<td> 
						File URL:
							<td> 
								<input type='text' <?php echo $disable; ?> placeholder='File URL' name='files' value='<?php echo issetor($msgedit[0]['files']);?>'>
								<input type='hidden' value='<?php echo issetor($_GET['edit_saved']); ?>' name='update'>
				<tr> 
					<td>
						Send:
							<td> 
								<table>
									<tr>
										<td>
											<input type='submit' value='Send message' name='submit' id='messagesend' <?php echo $disable; ?> <?php echo $link_confirm; ?>>
										<td>
											<input <?php echo $disable;  ?> type='submit' class='spacing-left generic' value='<?php echo $savevalue; ?>' name='<?php echo $savemsg; ?>'>
										<td>
											<?php echo (issetor($_GET['edit_saved'])) ? "<input type='submit' class='spacing-left generic' value='Save as new' name='Save'><td><a href='index.php?id=messages$botid_link' class='linkblock-cancel linkblock linkblock-border'>Cancel</a>":"";?>
									</td></tr></table>
					<tr> 
					<td><td><button type='button' width='100%' id='process' class='processhide'></button> 
				</td></tr></table></div>
			<td valign='top'>
			<?php 
			//Print out all stored messages so they can be re-populated in the text fields
			if(isset($_GET['botid'])){
				$stored_messages = $db_local->messagesLoad();
				$nummsg = count($stored_messages);
				echo "<table class='rounded compact'><tr><td colspan='2'><b><font color='$infocolor'>$nummsg</font> STORED MESSAGES</b><td>";
				foreach ($stored_messages as $key => $messagedetails) {
					echo "<tr>
							<td><a href='index.php?id=messages$botid_link&edit_saved={$messagedetails['id']}' title='Click to load message'>{$messagedetails['title']}</a> 
							<td>
								<a href='index.php?id=messages$botid_link&edit_saved={$messagedetails['id']}' title='Click to edit message' style='color: $pos_color;'>".actionButton('edit')."</a></b><br></li>
								<td><a href='index.php?id=messages$botid_link&delete_saved={$messagedetails['id']}' $link_confirm title='Click to delete message' style='color: $neg_color;'>".actionButton('delete')."</a></li>";
				}
				echo "</table>";
			}
			?>
						
		</table>
	<h3>Recipients <?php echo (issetor($disable)) ? ": Select a bot to send messages":"";?></h3><?php echo $gradrul; ?>
    <table class='rounded compact'>
		<tr><td class='tdTop'>
	<?php  
	$groups = $db_local->groupFetchGroups();
	$number_of_groups = count($groups); 
	
	?>
	<b>Public groups:</b><br>
	<?php 
	echo $generate->groupLinks('optionlinks',"","", $botid);
	?>
	<td class='tdTop'><b>Spaces:</b><br>
	<?php 
	
	if(isset($_GET['botid'])) {
		$query = array("sender"=>$botid,"max"=>"200","type"=>"group");
		echo $generate->roomGenCheckbox($spark->roomGet($query), "rooms[]", $botid);
	}
	else {
		echo "Select a bot to view spaces!<br>";
	}
	$contacts = $db_local->contactFetchContacts();
	$number_of_contacts = count($contacts);
	?>		
	<td class='tdTop'><b>USERS (<?php echo $number_of_contacts;?>):</b><br>
	<?php 

	foreach ($contacts as $key => $value) {
		$name = "{$value['firstName']} {$value['lastName']}";
		if (trim($name) == "") {
			$name = $value['emails'];
		}
		echo "<input type='checkbox' {$disable} name='contacts[]' value='{$value['id']}'> <a href='index.php?id=contacts&contactid={$value['id']}'>$name</a><br>";
	}

	?>
	
	
	<td class='tdTop'><b>All 1:1: (<?php echo (isset($direct_rooms['items'])) ? colorize_value($infocolor, count($direct_rooms['items'])) : colorize_value($infocolor, 0);?>)</b><br>
	<?php 
	echo "<input type='checkbox' {$disable} name='alldirect' value='1'> <a href='index.php?id=spaces&botid=$botid&spacetype=direct'>All 1:1 conversations</a> <br><br>"; ?>		
</td></tr></table></form>

</div>



