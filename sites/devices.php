<?php
$botid = issetor($_GET['botid']);
$deviceid = issetor($_GET['deviceid']);

$result = $commandkey = "";

if (empty($botid)) {
	$disable = "disabled";
}

$filter = (isset($_POST['status'])) ? strtolower(str_replace(" ", ".", issetor($_POST['filter']))):'';

if (isset($_POST['command'])) {
	$commandkey = strtolower(str_replace(" ", ".", issetor($_POST['commandkey'])));
	$args = issetor($_POST['args']);
} else {
	
}
if (isset($_POST['device_delete']) and $botid and issetor($_POST['deviceid'])) {
	print_r($_POST);
	echo $botid;
	print_r($spark->deviceDelete($botid, $_POST['deviceid']));
}
?>

<h1>DEVICES <?php echo (issetor($disable)) ? ": Select a bot to see controlled devices":"";?></h1><?php echo $gradrul; ?>

<?php  

$generate->botGenSelector('devices', issetor($botid));
					
$disable = (empty($executor)) ? "disabled" : "";

if ($botid) {
	?>
	<table><tr><td><strong>Devices</strong><td><strong>Connection status</strong></td><tr><td>
	<?php
	$devices = $spark->deviceList(issetor($botid));
	if (count($devices['items']) > 0) {
		foreach ($devices['items'] as $key => $value) {
			$selected = (issetor($_GET['deviceid']) == $value['id']) ? "linkblock-selected":"";
			$connectionstatus = $value['connectionStatus'];
			echo "<tr>
					<td>
						<a class='linkblock linkblock-border $selected' href='index.php?id=devices&botid=$botid&deviceid={$value['id']}'>{$value['displayName']}
					<td align='center'>
						{$connectionstatus}
					<td>
						<div id='input'>
							<form action='#' id='deletedevice' method='post' enctype='multipart/form-data'>
								<input type='hidden' value='{$value['id']}' name='deviceid'>
							</form>
						</div>";
		}
	}
	else {
		echo "<tr><td>No devices controlled by this bot, please give access to a bot via Control Hub<td>N/A";
	}
	echo "</table>";
}

if ($deviceid and $botid) {
	if ($filter) {
		$status = $spark->deviceStatus($botid, $deviceid, $filter);
	}
	if ($commandkey) {
		$result = $spark->deviceCommand($botid, $deviceid, $commandkey, $args);
	}
	$statusline = "";
	echo "<table width='100%'><tr>
					<td>
				<table class='rounded compact' width='100%'><tr>
				<td width='50%'>
					<h3>Device commands</h3>$gradrul
				<td width='500'>
					<h3>Device status</h3>$gradrul
				<tr>
					<td width='50%' valign='top'>
						<div id='input'> 
							<form id='command' method='post' action='#command' enctype='multipart/form-data'>
							<table>
								<tr>
									<td>
										<input type='text' name='commandkey' required value='".issetor($commandkey)."' placeholder='presentation.start'>
								<tr>
									<td> 
										<textarea cols='40' rows='4' name='args' placeholder='{\n\"ConnectorId\":[1,2]\n}'>".issetor($args)."</textarea>
								<tr>
									<td> 
										<input type='submit' name='command' value='Send command'></form>
						</table>
			
					<td width='50%' valign='top'>
						<div id='input'>
							<form id='status' method='post' action='#status' enctype='multipart/form-data'>
							<table>
								<tr>
									<td>
										<input type='text' name='filter' placeholder='systemunit.*' value='". issetor($filter) . "'>
								<tr>
									<td> 
										<input type='submit' name='status' value='Get status'> </form>
						</table>
			<tr>
				<td>
	";
	
	if (isset($result)) {
		print_r($result);
	}
	
	echo	"<td>
						<table><tr><td class='wrap' valign='top'>";
	if (isset($status['result'])) {
		echo statusdigger($status['result']);
	} else {
		echo "No results found on status filter, please check that the filter is correct. Wildcards are allowed, for example: Audio Input *, audio.input.*, audio.*.connectors.*. If the command is incomplete you must always end the filter with a .*";
	}
	echo "</table></table>";
}
						
?>