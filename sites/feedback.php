<script language="JavaScript">
function toggleMember(source, name) {
	  checkboxes = document.getElementsByName(name);
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}
function toggleNonMember(source, name) {
	checkboxes = document.getElementsByName(name);
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	} 
</script>

<?php 
if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 
$v_y = $c_y = $ec_y = $ed_y = $ev_y = "checked";
$v_n = $c_n = $ec_n = $ed_n = $ev_n = "";
$topic = issetor($_GET['topic']);
$botid = issetor($_GET['botid']);
$entryid = issetor($_GET['entry']);

$note = (!$botid) ? " - Select a bot":"";

//PAGE OPERATIONS
if (issetor($_POST['topic_add'])) {
    $db_local->feedbackTopicAdd($_POST);
}
if (issetor($_POST['topic_update'])) {
	$db_local->feedbackTopicUpdate($_POST, $_POST['topicid']);
}
if (issetor($_POST['topic_delete'])) {
	$db_local->feedbackTopicDelete($_POST['topicid']);
	header("Location: index.php?id=feedback&botid={$botid}");
}
if (issetor($_POST['entry_add'])) {
	$db_local->feedbackEntryAdd($_POST);
}
if (issetor($_POST['entry_update'])) {
	$entryid = issetor($_POST['entryid']);
	$db_local->feedbackEntryUpdate($_POST, $entryid);
}
if (issetor($_POST['entry_delete'])) {
	$db_local->feedbackEntryDelete($_POST['entryid']);
	if (issetor($_GET['entry'])) unset($_GET['entry']);
	header("Location: ".formUrl($_GET)."#entry");
}
if (issetor($_POST['entries_delete'])) {
	$entries_d = issetor($_POST['entries']);
	foreach ($entries_d as $key => $value) {
		$db_local->feedbackEntryDelete($value);
	}
	header("Location: index.php?id=feedback&botid={$botid}&topic=$topic#entry");
}
if (issetor($_POST['comment_add'])) {
	$db_local->feedbackEntryComment(issetor($_POST['entryid']), issetor($_POST['comment']), $_SESSION['email']);
}
if (issetor($_POST['votes_delete'])) {
	$entries_d = issetor($_POST['votes_selected']);
	foreach ($entries_d as $key => $value) {
		$db_local->feedbackEntryVoteDelete($value);
	}
	header("Location: ".formUrl($_GET)."");
}
if (issetor($_POST['comments_delete'])) {
	$entries_d = issetor($_POST['comments_selected']);
	foreach ($entries_d as $key => $value) {
		$db_local->feedbackEntryCommentDelete($value);
	}
	header("Location: ".formUrl($_GET)."");
}

if (issetor($_POST['cancel_topic'])) {
	header("Location: index.php?id=feedback&botid={$botid}");
}
if (issetor($_POST['cancel_entry'])) {
	header("Location: index.php?id=feedback&botid={$botid}&topic=$topic");
}

if ($topic) {
		$actionname = "topic_update"; 
		$submitname = "Update topic";
		$edit = "Update topic";
		$topicinfo = $db_local->feedbackFetchTopics($botid, $db_local->quote($topic));
		$form = (issetor($_GET['topic'])) ? "" : "&topic={$topicinfo[0]['id']}";
		list($v_y, $v_n) = ($topicinfo[0]['votes_allowed']) ? array("checked", ""):array("","checked");
		list($c_y, $c_n) = ($topicinfo[0]['comments_allowed']) ? array("checked", ""):array("","checked");
		list($ec_y, $ec_n) = ($topicinfo[0]['entry_create_allowed']) ? array("checked", ""):array("","checked");
		list($ev_y, $ev_n) = ($topicinfo[0]['entry_view_allowed']) ? array("checked", ""):array("","checked");
		list($ed_y, $ed_n) = ($topicinfo[0]['entry_delete_allowed']) ? array("checked", ""):array("","checked");
		
		$entry_e = "Create entry";
		$entry_action = "entry_add";
		$created_by = "<input type='hidden' value='{$_SESSION['email']}' name='created_by'>";
		if ($entryid) {
			$entryinfo = $db_local->feedbackFetchEntry($entryid);
			$entry_e = "Update entry (Id: {$entryinfo[0]['id']})";
			$entry_action = "entry_update";
			$created_by = "";
			
		}
}
else {
		$actionname = "topic_add";
		$submitname = "Create topic";
		$edit = "Create a topic";
		$groupinfo = array("0"=>array("id"=>"", "groupname"=>""));
		$form = "";
}

echo "<h1>FEEDBACK $note</h1>$gradrul";

$generate->botGenSelector('feedback', issetor($botid));

if ($botid) {
	$topic_id = ($topic) ? "<input type='hidden' value='{$topic}' name='topicid'>":"";
	$group_options = $generate->groupOptions(issetor($topicinfo[0]['accessgroup']));
    echo "<div id='input'><table class='rounded compact'>
    		<tr>
    			<td valign='top' width='40%'>
    			<form name='groups' method='post' action='".formUrl($_GET)."' enctype='multipart/form-data'>					
    							<h3>$edit</h3>$gradrul
    								<table class='rounded compact'>
    									<tr>
    										<td colspan='2'>
    										<input type='text' placeholder='Topic title' required name='title' value='".issetor($topicinfo[0]['title'])."'>
                                            <input type='hidden' value='{$botid}' name='botid'>
											{$topic_id}
    									<tr>
    										<td width='150' colspan='2'>
    											<b>Topic user settings (bot commands)</b>
										<tr>
    										<td width='150'>
    											Entry votes:
    										<td>
    											<label>On</label> <input type='radio' $v_y name='votes_allowed' value='1'>
    											<label>Off</label> <input type='radio' $v_n name='votes_allowed' value='0'> <i>Allow voting</i>
    									<tr>
    										<td width='150'>
    											Entry comments:
    										<td>
    											<label>On</label> <input type='radio' $c_y name='comments_allowed' value='1'>
    											<label>Off</label> <input type='radio' $c_n name='comments_allowed' value='0'> <i>Allow commenting</i>
    									<tr>
                                            <td width='150'>
    											Entry create:
    										<td>
    											<label>On</label> <input type='radio' $ec_y name='entry_create_allowed' value='1'>
    											<label>Off</label> <input type='radio' $ec_n name='entry_create_allowed' value='0'> <i>Allow creating entries</i>
    									<tr>
                                            <td width='150'>
    											Entry view:
    										<td>
    											<label>On</label> <input type='radio' $ev_y name='entry_view_allowed' value='1'>
    											<label>Off</label> <input type='radio' $ev_n name='entry_view_allowed' value='0'> <i>Allow view of entry details</i>
										<tr>
                                            <td width='150'>
    											Entry delete:
    										<td>
    											<label>On</label> <input type='radio' $ed_y name='entry_delete_allowed' value='1'>
    											<label>Off</label> <input type='radio' $ed_n name='entry_delete_allowed' value='0'> <i>Allow users delete their own entries</i>
										<tr>
                                            <td width='150'>
    											Topic accessgroup:
    										<td>
    											<select name='accessgroup'>
													<option value=''>Select access group</option>
													$group_options
												</select>
    									<tr>
    										<td colspan=2>
    											<input type='submit' name='$actionname' value='{$submitname}' />
    										"; 
    											if ($topic) {
    												echo "<input type='submit' class='spacing-left cancel' $link_confirm name='topic_delete' value='Delete topic'>";
    												echo "<input type='submit' class='spacing-left cancel' name='cancel_topic' value='Cancel'>";
    											}
    							echo "</form></table>";
    							
    							
    							
    echo "<td valign='top' colspan=2><h3>Topics</h3>$gradrul<table class='rounded compact'><tr><td>";
    echo $generate->feedbackGenTopics($botid);
    echo "</table></table>";
    echo "</div>";
}

if ($topic) {
	echo "<div id='input'>
	<h2>{$topicinfo[0]['title']} (Id: {$topicinfo[0]['id']}) </h2>$gradrul</div>
	
	";
	
	$entries = $generate->feedbackGenTopicEntries($botid, $topic);
	echo "<div id='input'><table class='rounded compact'>
    		<tr>
    			<td valign='top' width='40%'>
    			<form method='post' id='entry' action='".formUrl($_GET)."#entry' enctype='multipart/form-data'>					
    							<h3>$entry_e</h3>$gradrul 
    								<table class='rounded compact'>
    									<tr>
    										<td colspan='2'>
    											<textarea name='description' rows='10'>".issetor($entryinfo[0]['description'])."</textarea>
                                            	<input type='hidden' value='{$topic}' name='topic_id'>
												<input type='hidden' value='{$entryid}' name='entryid'>
												$created_by
    									<tr>
											<td>
												<input type='submit' name='$entry_action' value='$entry_e'/>
												";if ($entryid) {
    												echo "<input type='submit' class='spacing-left cancel' $link_confirm name='entry_delete' value='Delete entry'>";
    												echo "<input type='submit' class='spacing-left cancel' name='cancel_entry' value='Cancel'>";
    											}
									echo "</table></form>
				<td valign='top' colspan=2>
								<form method='post' action='".formUrl($_GET)."' enctype='multipart/form-data'>					
    							<h3>Entries</h3>$gradrul
    								<table class='rounded compact'>
    									<tr>
    										<td colspan='2'>
    											$entries
    									<tr>
											<td colspan=2><input type='submit' class='spacing-left cancel' $link_confirm name='entries_delete' value='Delete selected entries'>
									 </table></table></form>
<tr><td valign='top'>"; 
	if ($entryid) {
		$votes = $generate->feedbackGenEntryVotes($entryid);
		$comments = $generate->feedbackGenEntryComments($entryid);
		$n_c = $db_local->feedbackFetchEntryComments($entryid);
		$n_v = $db_local->feedbackFetchEntryVotes($entryid);
		$num_votes = count($n_v);
		$num_comments = count($n_c);
		echo "<h3>Entry text</h3>$gradrul
		{$entryinfo[0]['description']}
<td valign='top' width='30%'>
		
		<h3>Entry comments ($num_comments)</h3>$gradrul
		$comments
		<form method='post' id='comment' action='".formUrl($_GET)."#comment' enctype='multipart/form-data'>	
		<table class='rounded compact'>
			<tr>
				<td>
					<input type='text' name='comment'>
					<input type='hidden' name='entryid' value='".issetor($_GET['entry'])."'>
				<td>
					<input type='submit' name='comment_add' value='Add comment' >
		</table>
		</form>
<td valign='top'>
		<h3>Entry votes ($num_votes)</h3>$gradrul
		$votes
		
";
}

}

?>


