<?php 
if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 
$s_n = $d_n = "checked";
$s_y = $d_y = "";
$page_group = issetor($_GET['viewgroup']);
$botid = issetor($_GET['botid']);

//Page operations
if (isset($_POST['group_add'])) {
	unset($_POST['group_add']);
	$db_local->groupAdd($_POST);
}

if (isset($_POST['group_update'])) {
	unset($_POST['group_update']);
	//Sub ID = alternate ID of the group (more sane)
	if (!is_numeric($_POST['sub_id'])) {
		$_POST['sub_id'] = str_replace(" ", "", $_POST['sub_id']);
		$run = $db_local->groupUpdate($_POST);
		if (!$run) {
			$bot = ($botid) ? "&botid=$botid":"";
			header("Location: index.php?id=groups&viewgroup={$page_group}$bot&feedback=warning-groupNestedBot"); 
		}
	} else {
		echo feedbackMsg("Could not save form, subscription ID cannot be numeric!", "", "alert");
	}
}

if (isset($_POST['delete_group']) and $_POST['id'] != '15') {
	$db_local->groupRemove($db_local->quote($_POST['id']));
	if (!count($db_local->groupFetchGroups($_POST['id']))) {
	    header("Location: index.php?id=groups&feedback=success-DeleteGroup"); 
	}
	else {
	    header("Location: index.php?id=groups&feedback=alert-Delete"); 
	}
}
elseif (isset($_POST['delete_group']) and $_POST['id'] == '15') {
	echo $db_local->errorHandler(false);
}

if (isset($_GET['execute'])) {
	$botid_e = $db_local->quote(issetor($_GET['botid']));
	//Remove Users from group
	if (isset($_POST['member']) and count($_POST['member']) > 0) {
			foreach ($_POST['member'] as $value) {
				$db_local->groupRemoveContact($page_group, $value);
			}
	}
	//Add Users to group
	if (isset($_POST['non_member']) and count($_POST['non_member']) > 0) {
			foreach ($_POST['non_member'] as $value) {
				$db_local->groupAddContact(array('groupid' => $page_group, 'contactid' => $db_local->quote($value)));
			}	
	}
	//Add Spaces to group
	if (isset($_POST['non_space_member']) and count($_POST['non_space_member']) > 0) {
		foreach ($_POST['non_space_member'] as $value) {
			$db_local->groupAddSpace(array('groupid' => $page_group,
					'spaceid' => $db_local->quote($value), 'botid' => $botid_e));
		}
	}
	//Add Groups to group
	if (isset($_POST['nest'])) {
		$db_local->groupRemoveGroup($_POST['to_group']);
		if (isset($_POST['groups']) and count($_POST['groups'])) {
			foreach ($_POST['groups'] as $value) {
				$db_local->groupAddGroup(array('groupid' => $_POST['to_group'],
											   'nestedid' => $db_local->quote($value)));
			}
		}
	}
	if (isset($_POST['space_member']) and count($_POST['space_member']) > 0) {
		foreach ($_POST['space_member'] as $value) {
			$db_local->groupRemoveSpace($page_group, $db_local->quote($value), $botid_e);
		}
	}
	if (isset($_POST['copy'])) {
		$current_group = $_POST['to_group'];
		$from_group = $_POST['from_group'];
		$members_to_copy = $db_local->groupGetMembers($from_group);
		foreach ($members_to_copy as $key => $value) {
			$db_local->groupAddContact(array('groupid'=>$current_group, 'contactid'=>$value['contactid']));
		}
	}
}

if ($page_group) {
		$actionname = "group_update"; 
		$submitname = "Update group";
		$edit = "Update group";
		$groupinfo = $db_local->groupFetchGroups($db_local->quote($page_group));
		$form = "&viewgroup={$groupinfo[0]['id']}";
		list($s_y, $s_n) = ($groupinfo[0]['subscribable']) ? array("checked", ""):array("","checked");
		list($d_y, $d_n) = ($groupinfo[0]['default_group']) ? array("checked", ""):array("","checked");
		$bot_dropdown = $generate->botGenDropdown('botid', $groupinfo[0]['botid']);
}
else {
		$actionname = "group_add";
		$submitname = "Create group";
		$edit = "Create a group";
		$groupinfo = array("0"=>array("id"=>"", "groupname"=>""));
		$form = "";
		$bot_dropdown = $generate->botGenDropdown('botid');
}

echo "<h1>GROUPS</h1>";

echo "<div id='input'><table class='rounded compact'>
		<tr>
			<td valign='top'>
			<form name='groups' method='post' action='index.php?id=groups{$form}' enctype='multipart/form-data'>					
							<h3>$edit</h3>$gradrul
								<table class='rounded compact'>
									<tr>
										<td>
											Group name:
										<td>
										<input type='text' placeholder='Group Name' required='' name='groupname' value='".issetor($groupinfo[0]['groupname'])."'>
									<tr>
										<td>
											Subscription group:
										<td>
											<label>Yes</label> <input type='radio' name='subscribable' $s_y value='1'> 
											<label>No</label> <input type='radio' name='subscribable' $s_n value='0'> Subscribe via the bot subscription feature
											<input type='hidden' name='id' value='{$groupinfo[0]['id']}'>
									<tr>
										<td>
											Default group:
										<td>
											<label>Yes</label> <input type='radio' name='default_group' $d_y value='1'>
											<label>No</label> <input type='radio' name='default_group' $d_n value='0'> 
									<tr>
										<td>
											Group alias:
										<td>
										<input type='text' name='sub_id' placeholder='Subscription ID (text, no spaces)' value='".issetor($groupinfo[0]['sub_id'])."'>
									<tr>
										<td>
											Group owner (bot):
										<td>
											$bot_dropdown
									<tr>
										<td width='170'>
											Group description:
										<td>
											<textarea placeholder='Group description' name='description' >".issetor($groupinfo[0]['description'])."</textarea>
									<tr>
										<td>
											<input type='submit' name='{$actionname}' value='{$submitname}' />
										<td>"; 
											if ($groupinfo[0]['id'] != 15 and $page_group) {
												echo "<input type=submit name='delete_group' title='Delete group and remove all members' class='cancel' {$link_confirm} value='Delete group'>";
											}
											if ($page_group) {
												echo "<a href='index.php?id=groups' class='linkblock linkblock-cancel linkblock-border'>Cancel</a>";
											}
							echo "</form>";
							
							if ($page_group) {
								$groupslist = $generate->groupLinks('options');
								$groupid = $page_group;
								echo "<tr>
                                        <td colspan='2'><hr><h3>Copy members from another group to this group</h3>$gradrul
                                      <tr>
										<td>
                                            <form name='copy_groups' method='post' action='index.php?id=groups&viewgroup={$groupid}&execute' enctype='multipart/form-data'>
                                                <input type='submit' name='copy' value='Copy'>
                                        <td>
									          <select name='from_group'>
									$groupslist
									</select>
									<input type='hidden' name='to_group' value='{$groupid}'>
									
									</form>
								";
								$groupslist = $generate->groupLinks('nestedgroups', $groupid);
								echo "<tr>
										<td colspan=2><hr>
										<form name='nest_groups' method='post' action='index.php?id=groups&viewgroup={$groupid}&execute' enctype='multipart/form-data'>
									<h3>Logical group linking</h3>$gradrul
									<tr><td><input type='submit' name='nest' value='Apply selection'>
                                    <td>$groupslist
									<input type='hidden' name='to_group' value='{$groupid}'>
									
									</form>
									";
							}
							echo "</table></div>";
							
echo "<td valign='top'><h3>Created groups</h3>$gradrul<table class='rounded compact'><tr><td>";
echo $generate->groupLinks("linksnumbers");
echo "</table></table>";
echo "</div>";

if (isset($_GET['viewgroup'])) {
	$contacts = $db_local->contactFetchContacts();
	$current_group = $db_local->quote($_GET['viewgroup']);
	$num_of_total_members = count($contacts);
	$num_of_members = $db_local->groupMembershipNumber($current_group);
	$num_of_non_members = $num_of_total_members - $num_of_members;
	$non_members = "";
	$botidlink = "";
	
	if ($botid = issetor($_GET['botid'])) {
		$botvalues = $db_local->botFetchBots($botid);
		$botidlink = "&botid=" . $botid;
	}
	
	echo "<div id='mininav'>
	<h1>{$groupinfo[0]['groupname']} : Members</h1>$gradrul
	";

	echo "<form name='members' id='members' method='post' action='index.php?id=groups&viewgroup={$groupinfo[0]['id']}&execute&{$botidlink}#members' enctype='multipart/form-data'>
	<table class='rounded compact'>
	<tr><td><h1>Users</h1><td><div id='input'>
	<input type='submit' style='width: 150px;' name='execute' value='Apply changes'>
	</div>
	<tr> <td width='50%'> <font color='$pos_color'>Members (<b>{$num_of_members}</b>) ></font>:<td width='50%'> <font color='$neg_color'>< Not member (<b>{$num_of_non_members}</b>)</font>:
	<tr> <td valign='top'>
	<table class='rounded compact'><tr><td class='tdTop'>";
	?>
	
	<input type='checkbox' onClick='toggle(this,"member[]")'><td><b>Select all</b><td>	
	
	<?php 
	foreach ($contacts as $key => $value) {
		$image = (!empty($value['avatar'])) ? $value['avatar'] : "/images/static/noimagefound.jpeg";
		$disable = $linked_member = "";
		if ($db_local->groupCheckIfMember($value['id'], $current_group)) {
			if (!$db_local->groupCheckIfActualMember($value['id'], $current_group)) {
				$linked_group_info = $db_local->groupCheckContactGroupLink($current_group, $value['id']);
				$disable = "disabled";
				$linked_member = colorize_value($neg_color, "<b>Member via {$linked_group_info[0]['groupname']}</b>", "");
				$already = colorize_value($infocolor, "<b>Member</b>", "");
				$non_members .= "<tr><td><input name='non_member[]' type='checkbox' value='{$value['id']}'><td><img class='rounded' src='{$image}' width='40' height='40'><td><a class='linkblock' href='index.php?id=contacts&contactid={$value['id']}'><b>{$value['firstName']} {$value['lastName']}</b> ({$value['emails']})</a> $already<br>";
			}
			echo "<tr><td><input name='member[]' $disable type='checkbox' value='{$value['id']}'><td><img class='rounded' src='{$image}' width='40' height='40'><td><a class='linkblock' href='index.php?id=contacts&contactid={$value['id']}'><b>{$value['firstName']} {$value['lastName']}</b> ({$value['emails']})</a> $linked_member";		
		}
		else {
			$non_members .= "<tr><td><input name='non_member[]' type='checkbox' value='{$value['id']}'><td><img class='rounded' src='{$image}' width='40' height='40'><td><a class='linkblock' href='index.php?id=contacts&contactid={$value['id']}'><b>{$value['firstName']} {$value['lastName']}</b> ({$value['emails']})</a><br>";
		}
	}	
	?>
	
	</td></table>
	<td valign='top'><table class='rounded compact'><tr><td><input type='checkbox' onClick='toggle(this, "non_member[]")'><td><b>Select all</b><td>	
	
	<?php echo $non_members; ?></table>
	
	<?php 
	echo "</table></div>";
	
	if ($botid) {
		$spaces = $spark->roomGet(array("sender"=>$botid,"max"=>'500',"type"=>"group"));
		$num_total_spaces = count($spaces['items']);
		$removed = 0;
		$space_members = $db_local->groupGetSpaceMembers($current_group, $botid);
		foreach ($space_members as $key => $value) {
			if (!in_array($value['spaceid'],array_column($spaces['items'], 'id'))) {
				$db_local->groupRemoveSpace($current_group, $value['spaceid'], $botid);
				$removed++;
			}
		}
		
		if ($removed) echo feedbackMsg('Spaces removed ', "$removed spaces has been removed from the membership list because the bot is no longer in the space", 'warning');
		$num_space_members = $db_local->groupSpaceMembershipNumber($current_group, $botid);
		$num_non_space_members = $num_total_spaces - $num_space_members;
		$non_space_members = "";
		echo "<table class='rounded'>
				<tr><td><h1>Spaces</h1><td><div id='input'>
					<input type='submit' style='width: 150px;' name='execute' value='Apply changes'> 
				 
				<tr> <td width='50%'> <font color='$pos_color'>Members (<b>{$num_space_members}</b>) ></font>:<td width='50%'> <font color='$neg_color'>< Not member (<b>{$num_non_space_members}</b>)</font>:
				<tr> <td valign='top'>"; ?>
		<input type='checkbox' onClick='toggle(this,"space_member[]")'> <b>Select all</b><br>	
		<?php 	
		
		foreach ($spaces['items'] as $key => $value) {
			if ($db_local->groupCheckIfSpaceMember($value['id'], $current_group, $botid)) {
				echo "<input name='space_member[]' type='checkbox' value='{$value['id']}'> {$value['title']}<br>";
			}
			else {
				$non_space_members .= "<input name='non_space_member[]' type='checkbox' value='{$value['id']}'> {$value['title']}<br>";
			}
		}
		
		?>
		<td valign='top'><input type='checkbox' onClick='toggle(this, "non_space_member[]")'> <b>Select all</b><br>
		
		<?php echo $non_space_members; ?>
		
		</table>
			
	<?php 
	}
}

echo "</form>";
echo "</table>";
echo "</div>";
?>


