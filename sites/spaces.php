<?php 
//Verification
if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 
//Declarations
$executor = $form = $isbot_member_output = "";
$rooms_selected = $contacts_selected = array();
$spacetype_const = (issetor($_GET['spacetype'])) ? $db_local->quote($_GET['spacetype']):"";
$spaceid = (issetor($_GET['viewspace'])) ? $db_local->quote($_GET['viewspace']):"";
$teamid = (issetor($_GET['teamid'])) ? $db_local->quote($_GET['teamid']):"";
$context = (isset($_GET['team'])) ? true:false;
$botid = (issetor($_GET['botid'])) ? $db_local->quote($_GET['botid']):"";
$botinfo = (!empty($botid)) ? $db_local->botFetchBots($botid):"";
$disable = ($botid) ? "":"disabled";
$botid_link = ($botid) ? "&botid=" . $botid : "";
$teamspace_create_link = ($teamid != "") ? "&teamid=$teamid&spacetype=teamspaces&":"";

//Delete membership
if (isset($_GET['membershipdelete'])){
	$spark->membershipDelete($botid, $db_local->quote($_GET['membershipdelete']));
}
//Delete message
if (isset($_GET['messagedelete'])){
	$spark->messageDelete($botid, $db_local->quote($_GET['messagedelete']));
}
//Delete space
if (isset($_GET['spacedelete'])){
	$spark->roomDelete($botid, $db_local->quote($_GET['spacedelete']));
}
//Switch joinable flag
if (isset($_GET['setjoinable'])){
	$spaceinfo = $spark->roomGetDetails($botid, $spaceid);
	$title = $spaceinfo['title'];
	$data_array = array("spaceid" => $spaceid,
			"spacetitle" => $title,
			"botid" => $botid);
	$db_local->adminAddJoinableSpace($data_array);
}
elseif (isset($_GET['setunjoinable'])){
	$db_local->adminRemoveJoinableSpace($botid, $spaceid);
}
//Switch moderator flag
if (isset($_GET['setmod']) and !empty($botid)) {
	$spark->membershipSetMod($_GET['setmod'], $botid, "true");
}
elseif (isset($_GET['unsetmod']) and !empty($botid)) {
	$spark->membershipSetMod($_GET['unsetmod'], $botid, "false");
}
//Switch group response flag
if (isset($_GET['setgroupresponse'])) {
	$db_local->adminAddGroupResponseAcl($botid, $spaceid);
}
elseif (isset($_GET['unsetgroupresponse'])) {
	$db_local->adminRemoveGroupResponseAcl($botid, $spaceid);
}
//Create space or add users
$creator = isset($_POST['creator']);
$addtoexisting = isset($_POST['addpersontospace']);
if ($addtoexisting or $creator) {
	$a = 0;
	$memberParsel = array();
	if ($creator) {
		$c_title = $_POST['c_spacetitle'];
		$c_type = $_POST['c_spacetype'];
		$c_group = $_POST['c_group'];
		$c_csv = str_replace(" ", "", $_POST['c_mailcsv']);	
		
		if (empty($c_title)) $c_title = 'No name';
		
		if ($c_type == 'team') {
			$newSpaceId = $spark->teamCreate($botid,$c_title);
		} 
		elseif ($c_type == 'space') {
			$newSpaceId = $spark->roomCreate($botid,$c_title)['id'];
		} else {
			$newSpaceId = $spark->roomCreate($botid,$c_title,$c_type)['id'];
			$c_type = "Teamspace";
		}
	} elseif($addtoexisting) {
		$newSpaceId = $spaceid;
		$c_group = issetor($_POST['groups']);
		$c_type = "";
		$c_csv = str_replace(" ", "", $_POST['personemail']);
	}
	if ($c_type != "team") {
		if (!empty($c_group)) {
			$members = $db_local->groupGetMembers($c_group);
			foreach ($members as $key => $member){
				$memberParsel[$a]['roomtype'] = "roomId";
				$memberParsel[$a]['id'] = $newSpaceId;
				$memberParsel[$a]['method'] = "personId";
				$memberParsel[$a]['person'] = $member['contactid'];
				$a++;
			}
		}
		if (!empty($c_csv)) {
			$addlist = explode(",", $c_csv);
			foreach ($addlist as $key => $usertoadd) {
				if (validateEmail($usertoadd)) {
					$memberParsel[$a]['roomtype'] = "roomId";
					$memberParsel[$a]['id'] = $newSpaceId;
					$memberParsel[$a]['method'] = "personEmail";
					$memberParsel[$a]['person'] = $usertoadd;
					$a++;
				}
			}
		}
		//Start request intervals	
		echo $spark->bulkSplitRequestPost("memberships", $memberParsel, $botid);
		//End request intervals
	} elseif ($c_type == "team") echo feedbackMsg("The team was created, adding people directly when creating a team is not supported. To add many people to a team, please select the teams general space and add members from there (That will make the users member of the team automatically). This is a limitation in the team membership API.", "", "warning");
}

if (isset($_GET['joinbot'])) {
	$addlist = $db_local->quote($_GET['joinbot']);
	$spark->membershipCreateMultiple($botid, $db_local->quote($spaceid),$addlist);
}
//Set title
if (isset($_POST['settitle'])){
	$spark->roomUpdate($botid, $db_local->quote($_GET['viewspace']),$_POST['spacetitle']);
}
if ($teamid != "") {
	$teaminfo = $spark->teamGetDetails($botid, $teamid);
}
$space_create_placeholder = ($teamid == "") ? "Space Name":"Team Space Name"; 
?>
	<h1>SPACES & TEAMS <?php echo (issetor($disable)) ? ": Select a bot to manage spaces & teams":"";?></h1><?php echo $gradrul; ?>
	
	<table class='rounded compact'>
			<tr>
				<td>
						<?php 			
						$selected = $generate->botGenSelector('spaces', issetor($botid));
						
						if ($selected) {
							$executor = "<b>{$selected['displayName']}</b>";
							$executor_avatar = "<img class='rounded' title='{$selected['displayName']} (SELECTED)' height='35' width='35' src='{$selected['avatar']}'>";
						}
						
						$disable = (empty($executor)) ? "disabled" : "";
						?>
				</td></tr></table>
				
	<table class='rounded compact'><tr><td class='tdTop'><table class='rounded compact'><tr><td>
	<b>OPTIONS:</b><br>
	<?php  
	if(isset($_GET['botid'])) {
		$botid=$_GET['botid'];
		$group_class = $group_class = $direct_class = $all_class = $teamspaces_class = $teams_class = ""; 
		$spacetype = ($spacetype_const) ? $spacetype_const : "group";
		$selectedvalue = "linkblock-selected";
		switch($spacetype){
			case "group":
				$group_class = $selectedvalue;
				break;
			case "direct":
				$direct_class = $selectedvalue;
				break;
			case "all":
				$all_class = $selectedvalue;
				break;
			case "teamspaces":
				$teamspaces_class = $selectedvalue;
				break;
			case "teams":
				$teamspaces_class = $selectedvalue;
				break;
		}
		echo "<a class='linkblock linkblock-border $all_class' href='index.php?id=spaces&botid=$botid&spacetype=all'>All</a>
			  <a class='linkblock linkblock-border $direct_class' href='index.php?id=spaces&botid=$botid&spacetype=direct'>1:1 Direct</a>
			  <a class='linkblock linkblock-border $group_class' href='index.php?id=spaces&botid=$botid&spacetype=group'>Spaces</a>
			  <a class='linkblock linkblock-border $teamspaces_class' href='index.php?id=spaces&team&botid=$botid&spacetype=teams'>Teams</a>"; 
	}
	else {
		echo "Select a bot to view options!<br>";
	}
	?>
	<br><b><?php 
				if ($context) echo "SELECT TEAM:"; 
				elseif ($spacetype_const == "teamspaces") echo "SELECT TEAM SPACE:"; 
				else echo "SELECT SPACE:";
			?></b><br>
	<?php 
	
	if(!empty($botid)) {
		$type = "";
		$spacetype = ($spacetype_const) ? $spacetype_const : "group";
		$maxresults = "500";
		switch($spacetype){
			case "group":
				$query = array("sender"=>$botid,"max"=>$maxresults,"type"=>"group");		
				break;
			case "direct":
				$query = array("sender"=>$botid,"max"=>$maxresults,"type"=>"direct");
				break;
			case "all":
				$query = array("sender"=>$botid,"max"=>$maxresults, "type"=>"");
				break;
			case "teamspaces":
				$query = array("sender"=>$botid,"max"=>$maxresults,"teamId"=>$teamid,"type"=>"group");
				$type = $spacetype; 
				break;
			case "teams":
				$query = $spark->teamGet($botid, $maxresults);
				break;
			default:
				$query = array("sender"=>$botid,"max"=>$maxresults,"type"=>"group");
				break;
		}
		echo ($spacetype == 'teams') ? $generate->teamGenLinks($query, $botid) : $generate->roomGenLinks($spark->roomGet($query), $botid, $type);		
	}
	else {
		echo "Select a bot to view spaces!<br>";
	}
	$contacts = $db_local->contactFetchContacts();
	$number_of_contacts = count($contacts);
	?>	
</td></tr></table>			
<td class='tdTop'>
<?php 
//CREATOR OF SPACES
if(!empty($botid) and empty($spaceid)) {
	$c_groupoptions = $generate->groupLinks('options');
	$teamspace_selector = ($teamid and $teaminfo) ? "<input type='radio' value='$teamid' name='c_spacetype'>Team space in {$teaminfo['name']}<br>": "";
	
	
	echo "<div id='input'>
			<form method='post' action='#' enctype='multipart/form-data'>
			<table class='rounded compact'> <tr> <td colspan='2'> <b>CREATE SPACE/TEAM AND ADD PEOPLE</b> 
			<tr>
				<td width='200'>SPACE TITLE:<td><input type='text' name='c_spacetitle' placeholder='Title goes here (name of the space/team)'>
			<tr>					
				<td>TYPE:
					<td><input type='radio' value='space' checked name='c_spacetype'>Normal space<br>
						$teamspace_selector
						<input type='radio' value='team' name='c_spacetype'>Team
					
			<tr>
				<td>ADD PEOPLE:						
					<td>
							<select name='c_group'><option value=''>Select group to add</option>$c_groupoptions</select>
							<textarea name='c_mailcsv' placeholder='E-mail CSV input: user@example.com,user@example.com,etc..'></textarea>
				<tr>
				<td>
					<td><input type='submit' name='creator' value='Create'> 				
			</table></form>
			";
}


if (!empty($spaceid)){
	$disable = "";
	$spaceinfo = $spark->roomGetDetails($botid, $spaceid);
	$spacetype = (issetor($spacetype_const) == "teamspaces") ? "group":$spacetype_const;
	$membershipinfo = $spark->membershipGet($spaceid, $botid);
	$team_link = ($teamid != "") ? "teamid=$teamid&":""; 
	$baselink = "index.php?id=spaces&botid=$botid&viewspace=$spaceid&{$team_link}spacetype=$spacetype_const"; 
	$joinable = ($db_local->adminCheckJoinableSpace($botid, $spaceid)) ? "<a class='linkblock linkblock-border' href='$baselink&setunjoinable'>Make unjoinable</a>":"<a class='linkblock linkblock-border' href='$baselink&setjoinable'>Make joinable</a>";
	$groupresponse = ($db_local->adminCheckGroupResponseAcl($botid, $spaceid)) ? "<a class='linkblock linkblock-border' href='$baselink&unsetgroupresponse'>Disable space response</a>":"<a class='linkblock linkblock-border' href='$baselink&setgroupresponse'>Enable space response</a>";
	$groupoptions = $generate->groupLinks("options");
	$isteam = (isset($spaceinfo['teamId']));
	if (!$isteam) $messageinfo = $spark->messageFetchMessages($botid, $spaceid, $spacetype);
	$isteam_light = onoff($isteam);
	$islocked = issetor($spaceinfo['isLocked']);
	$islocked_light = onoff($islocked);
	$isbot_space = $spark->membershipGet($spaceid, $botid, $botinfo[0]['emails']);
	$isbot_space_test = (!empty($isbot_space['items'])) ? true:false;
	if ($isbot_space_test) {
		$mod = ($isbot_space['items'][0]['isModerator'] or !$islocked) ? true:false;
		$isbot_moderator_light = ($mod) ? onoff(true):onoff(false);
	} else {
		$isbot_moderator_light = onoff(false);
		$mod = false;
	}
	if ($isteam) {
		$spacedelete = "<a class='linkblock linkblock-border' href='index.php?id=spaces&botid=$botid&{$team_link}spacetype=$spacetype_const&spacedelete=$spaceid' $link_confirm style='color: $neg_color;'>Archive space</a>";
		$isbot_member = ($spark->roomGet(array("sender"=>$botid,"max"=>$maxresults,"teamId"=>$spaceinfo['teamId'],"type"=>"group"))) ? true:false;
		if ($isbot_member and $isbot_space_test) $messageinfo = $spark->messageFetchMessages($botid, $spaceid, $spacetype);
		$isbot_member_output = "<tr><td>BOT IS PART OF THE TEAM:<td>";
		$isbot_member_output .= onoff($isbot_member);
		$isbot_space_output = "<tr><td>BOT IS IN THIS SPACE:<td>";
		$isbot_space_output .= ($isbot_space_test) ? onoff(true):"<div id='mininav'><ul><li><a href='$baselink&joinbot={$botinfo[0]['emails']}' style='color: $pos_color;'>".onoff(false)." JOIN BOT TO TEAM SPACE</a></li></ul></div>";
		if (!$mod) {
			$spacedelete = "";
			$joinable = "";
			$groupresponse = "1:1 - No options";
			$disable = "disabled";
		}	
	} else {
		$spacedelete = "<a class='linkblock linkblock-border' href='index.php?id=spaces&botid=$botid&{$team_link}spacetype=$spacetype_const&spacedelete=$spaceid' $link_confirm style='color: $neg_color;'>Delete space</a>";
		if ($spacetype_const == "direct" or !$mod) {
			$spacedelete = "";
			$joinable = "";
			$groupresponse = "1:1 - No options";
			$disable = "disabled";
			$mod = false;
		}
		$isbot_space_output = "";
		$isbot_member = "";
	}
	$convoWidget = "<head>
    <meta charset='utf8'>
    <script src='https://code.s4d.io/widget-space/production/bundle.js'></script>
  </head>
  <body>
    <div id='my-webexteams-widget' style='width: 500px; height: 500px;'/>
    <script>
      var widgetEl = document.getElementById('my-webexteams-widget');
      // Init a new widget
      ciscospark.widget(widgetEl).spaceWidget({
        accessToken: '". $spark->authGet($botid) ."',
        destinationId: '$spaceid',
        destinationType: 'spaceId'
      });
    </script>
  </body>
</html>";
	echo "<table width='100%' class='rounded compact'>
			<tr>
				<td colspan='2'>
					<table>
						<tr>	
							<td>
								SPACE OPTIONS:
							<td>
								<div id='mininav'>
									<ul>
										$joinable
										$groupresponse	
										$spacedelete			
									</ul>
								</div>
						<tr>
							<td>	
								SPACE TITLE
							<td>
							<div id='input'>
								<form method='post' action='$baselink' enctype='multipart/form-data'>
									<input type='text' style='width: 70%;' $disable name='spacetitle' value='{$spaceinfo['title']}'> 
									<input type='submit' $disable name='settitle' value='Set title'>
								</form>
							<hr>
						<tr>
							<td>
								ADD PEOPLE (Manual or Group)
							<td>
								<div id='input'>
									<form method='post' action='$baselink' enctype='multipart/form-data'>
										<textarea $disable placeholder='CSV format: email,email,email..' name='personemail'></textarea> 
										<select name='groups' $disable>
											<option value='' selected>Add groups of people</option>
											$groupoptions
										</select><br>
										<input type='submit' value='Add users to space' style='width: 40%;' $disable name='addpersontospace'>
									</form>
							  </div>
						<tr>
							<td>
								SPACE IS LOCKED:
							<td>
								$islocked_light
						<tr>
							<td>
								BOT CAN MODERATE SPACE:
							<td>
								$isbot_moderator_light
						<tr>
							<td>
								SPACE IS PART OF A TEAM:
							<td>
								$isteam_light
								$isbot_member_output
								$isbot_space_output
					</table>
		  	<tr>
				<td width='40%'>
					<b>SPACE MEMBERS</b>
				<td width='60%'>
					<b>MESSAGES</b>
			<tr>
				<td valign='top'>";
	
	echo $generate->roomGenMembership($botid, $membershipinfo, $spacetype_const, $teamid, $mod);
	
	echo "<td valign='top'>";
	if ($spacetype_const == 'direct') {
		echo $convoWidget;
	} else {
	echo (($isteam and $isbot_member and $isbot_space_test) or (!$isteam)) ? $generate->messageConversation($messageinfo, $botid):feedbackMsg("Sorry!", "could not fetch messages", "warning");
	}	
	echo "</table>";
}
?>




