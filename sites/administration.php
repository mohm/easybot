<?php if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 
$subpage = (issetor($_GET['sub'])) ? $db_local->quote($_GET['sub']):header('Location: index.php?id=administration&sub=default');
$statusimage = "";
$down = "";
if (isset($_POST['domains'])) {
	foreach ($_POST['domains'] as $key => $value){
		$db_local->acceptedDomainDelete($value);
	}
}
if (isset($_POST['domain'])) {
	if (validateDomain($_POST['domain'])) {
		$db_local->acceptedDomainAdd($_POST);
	} else {
		echo "Jalla";
		header("Location: index.php?id=administration&sub=default&feedback=warning-InvalidDomain#");
	}
}
	
switch ($subpage) {
	case "default": 	
	//Flip for maintenance mode
	if (isset($_POST['maintenance_mode'])) {
		$db_local->adminSetMaintenance($_POST['maintenance_mode']);
		header('Location: index.php?id=administration&sub=default');
	}	
	if (isset($_POST['maintenance_message'])) {
		$db_local->adminSetMaintenanceMessage($_POST['maintenance_message']);
	}
	if (isset($_POST['task_monitor'])) {
		echo $db_local->adminSetTaskMonitor($_POST['task_monitor']);
		header('Location: index.php?id=administration&sub=default');
	}	
	if (isset($_POST['botid'])) {
		$db_local->query($db_local->constructUpdateValues("log_bot", $_POST, "WHERE id = '0'"));
	}
	if (isset($_POST['email'])) {
		$db_local->adminBlockContact($db_local->quote($_POST['email']));
	}
	if (isset($_POST['logemail'])) {
		$db_local->adminAddLogUser($db_local->quote($_POST['logemail']));
	}
	if (isset($_POST['blocked'])) {
		foreach ($_POST['blocked'] as $key => $value){
			$db_local->query("DELETE FROM blocked_contacts WHERE id = '{$value}'");
		}
	}
	if (isset($_POST['logusers'])) {
		foreach ($_POST['logusers'] as $key => $value){
			$db_local->query("DELETE FROM log_users WHERE id = '{$value}'");
		}
	}
	//Check if maintenance mode is enabled and sets variables
						$activator = ($db_local->adminCheckIfMaintenance()) ? "Disable":"Enable";
						$taskactivator = ($db_local->adminCheckIfTaskMonitor()) ? "Disable":"Enable";
						
						if ($db_local->adminCheckIfTaskMonitor()) {
							//Checks the service status, if up show a green light else a red light
							$statusimage = onoff(!$db_local->adminCheckServiceStatus(),20,20);
							$down = (!$db_local->adminCheckServiceStatus()) ? "All good!":$db_local->adminCheckServiceStatus("report");
						} 						
					
						//Title
						$titlepage = "ADMINISTRATION";
						break;	
					case "logs":
						$botfilter="";
						$limit = $_GET['maxresults'];
						if (is_numeric($limit)) {
								if (isset($_GET['botfilter'])) {
									$botfilter = $_GET['botfilter'];		
								}
								$logs = $generate->logsPrint($botfilter, $limit);
								$titlepage = "LOGS";
						}
						else{
							$logs = "Issue with the maxresults";	
						}
						break;
					case "spaceoptions":
						$titlepage = "Space options";
			}	
			if (!$statusimage) $statusimage = "<b>Disabled</b>";
				?>
				<h1><?php echo $titlepage; ?>
				<?php echo $gradrul; ?></h1>
				<?php 
				if ($subpage === 'default') {
					?><div id='input'><table class='smallform compact' width='70%'>
						<tr>
							<td colspan=3>
								<h3>Status</h3>
								<?php echo $gradrul; ?>
						<tr>
							<td>
								Added users
							<td>
								<?php echo "<b><font color='$infocolor'>".count($db_local->contactFetchContacts())."</font></b>";?>
						<tr>
							<td>
								Added bots
							<td>
								<?php echo "<b><font color='$infocolor'>".count($db_local->botFetchBots())."</font></b>";?>
						<tr>
							<td colspan='3'>
								<h3>Task service settings</h3>
								<?php echo $gradrul; ?>
						<tr>
							<td>
								Task service monitoring
							<td width='200'><?php echo $statusimage; ?>&nbsp<?php echo $down; ?>
							<td width='200'>
								<form method='post' action='#taskservice' id='taskservice' enctype='multipart/form-data'>
									<input id='input' type='submit' style='width: 200px;' value='<?php echo $taskactivator; ?>'>
									<input type='hidden' value='<?php echo !$db_local->adminCheckIfTaskMonitor(); ?>' name='task_monitor'>
								</form>
								
						<tr>
							<td>
								Maintenance mode 
								<?php $main_message = $db_local->adminGetMaintenanceMessage(); ?>
							<td>
							 <?php echo ($db_local->adminCheckIfMaintenance()) ? "<b>Enabled</b>":"<b>Disabled</b>"; ?>
							<td>
								<form method='post' action='#maintenance' id='maintenance' enctype='multipart/form-data'>
									<input id='input' type='submit' style='width: 200px;' value='<?php echo $activator; ?>'>
									<input type='hidden' value='<?php echo !$db_local->adminCheckIfMaintenance(); ?>' name='maintenance_mode'>
								</form>
						<tr>
							<td>
							Maintenance mode bot response: 
							<td>
							<form method='post' id='maintenancemsg' action='#maintenancemsg' enctype='multipart/form-data'>
							<input type='text' placeholder='Maintenance message' name='maintenance_message' value='<?php echo $main_message?>'>
							<td>
							<input id='input' type='submit' style='width: 200px;' value='Set'></td></form>
						<tr>
						<td>
								Add mail domains for bot restriction 
							<td>
								<form method='post' id='domains' action='#domains' enctype='multipart/form-data'>
									<input type='text' placeholder='example.com' required name='domain'>
							<td><input class='spacing-left' style='width: 200px;' type='submit' value='Add'>
								</form>
							
							<form method='post' action='#domainsadded' id='domainsadded' enctype='multipart/form-data'>
							<tr>
								<td> Domains
								<td>
										<?php 
											$domain_data = $db_local->acceptedDomainFetch();
											$number_of_domains = count($domain_data);
											foreach ($domain_data as $key => $value) {
												echo "<input type='checkbox' name='domains[]' value='{$value['id']}'> {$value['domain']}<br>";
											}
											if ($number_of_domains>0) echo "<td><input type='submit' style='width: 200px;' class='spacing-left cancel' value='Delete selected'>";
											else echo "<b>No domains added</b>";
							?>
							</form>
						<tr><td colspan='3'><h3>Block bot communication with e-mails</h3><?php echo $gradrul;?>
						<tr>
							<td> 
								Block e-mail
							<td>
								<form name='usersBlocked' method='post' id='block' action='#block' enctype='multipart/form-data'>
									<input type='text' placeholder='Email' required name='email'>
							<td> <input style='width: 200px;' type='submit' class='spacing-left' value='Add'>
								</form>
						<tr>
							<td>
								Blocked e-mails
							<td>
							<form method='post' action='#blocked' id='blocked' enctype='multipart/form-data'>
							<?php 
								$blocked_contacts = $db_local->adminGetBlockedContacts();
								$number_of_blocked_contacts = count($blocked_contacts);
								if (!$blocked_contacts) echo "<b>No blocked e-mails</b>";
								foreach ($blocked_contacts as $key => $value){
									echo "<input type='checkbox' name='blocked[]' value='{$value['id']}'> {$value['email']}<br>";
								}
								if ($number_of_blocked_contacts>0) echo "<td><input type='submit' style='width: 200px;' class='spacing-left cancel' value='Delete selected'>";		
										?>
							</form>
						<tr><td colspan='3'><h3>Bot event notifications</h3><?php echo $gradrul;?>
						<tr>
							<td>
								Add event-notification receivers (Webex Teams e-mails)
							<td>
								<form name='usersLogging' method='post' id='events' action='#events' enctype='multipart/form-data'>
								<input type='text' placeholder='Email' required name='logemail'>
							<td>
								<input style='width: 200px;' type='submit' class='spacing-left' value='Add'>
								</form>
						<tr>
							<td>Event-notification receivers
							<form method='post' action='#eventreceivers' id='eventreceivers' enctype='multipart/form-data'>
								<td>
										<?php 
										$log_users = $db_local->adminGetLogUsers();
										if (!$log_users) echo "<b>No event receivers</b>";
										$number_of_log_users = count($log_users);
										foreach ($log_users as $key => $value){
											echo "<input type='checkbox' name='logusers[]' value='{$value['id']}'> {$value['email']}<br>";
										}
										if ($number_of_log_users>0) echo "<td><input type='submit' class='spacing-left cancel' style='width: 200px;' value='Delete selected'>";		
										?>
							</form>
								<tr>
									<td>
										Event notification sender
									<td><form method='post' action='#eventsender' id='eventsender' enctype='multipart/form-data'>
										<?php 
										$selectedbot = $db_local->adminGetLogBot();
										echo (empty($selectedbot)) ? $generate->botGenDropdown("botid"):$generate->botGenDropdown("botid",$selectedbot);
										?>
										<td><input type='submit' style='width: 200px;' value='Set'>
										</form>
							</table>
					</table>
					</div>
				<?php 
					}
				elseif ($subpage === 'logs') 
					echo $logs;		
				?>

