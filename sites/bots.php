<?php if ($windowid != "login" and !verify()) header("Location: index.php?id=login");
$subpage = issetor($_GET['sub']);
$botid = issetor($_GET['botid']);
$maininfo = $db_local->botGetMainInfo();
$base = "index.php?id=base";
$link = "$base&sub=profile&botid={$botid}&apiq=";

//quickhooks
if (isset($_POST['createhook_group'])) {
	$whid = $spark->webhookCreateQuick($botid, 'group');
	header("Location: #$whid");
	
} 
else if (isset($_POST['createhook_direct'])) {
	$whid = $spark->webhookCreateQuick($botid, 'direct');
	header("Location: #$whid");
}

if (isset($_POST['addbotmail'])) {
	$botmail = explode(',',$db_local->quote($_POST['addbotmail']));
	foreach ($botmail as $foo => $mail) {
		if (validateEmail($mail)) {
			list($user, $domain) = explode('@', $mail);
			if ($domain == "sparkbot.io" or $domain == "webex.bot") {
				$data = array('sender' => $maininfo['id'], 'recepientType' => 'email', 'recepientValue' => $mail);
				echo ($db_local->botAdd($spark->peopleGet($data))) ? msgList("success-Query"):msgList("alert-Query");
			} else echo msgList("warning-botEmail");
		} else echo msgList("alert-Query");
	}
}
if (isset($_POST['deletebot'])) {
	$deleteid = $db_local->quote($_POST['deletebot']);
	if ($botid) {
		$db_local->botDelete($botid);
		redirect('id=bots&feedback=success-Query');
	}
}
if (isset($_POST['addtoken'])) {
	$db_local->botUpdateSettings($botid,$db_local->quote($_POST['addtoken']));
}
if (isset($_POST['domaintrigger'])) {
	$db_local->botDeleteAllowedDomain($botid);
	if (isset($_POST['botdomains'])) {
		foreach ($_POST['botdomains'] as $key => $domainid){
			$db_local->botAddAllowedDomain($domainid, $botid);
		}
	}
}
if (isset($_POST['defres'])) $db_local -> botSetDefaultResponse($db_local->quote($_POST['defres']), $botid);
if (isset($_GET['setprimary'])) $db_local->botSetPrimary($botid);


$bots = $db_local->botFetchBots();
$botinfo = (issetor($botid)) ? $db_local->botFetchBots($botid)[0] : "";

//PROFILE
if ($subpage == "profile") {
	if (isset($_GET['updatebot'])) {
		$data = array('sender' => $maininfo['id'], 'recepientType' => 'id', 'recepientValue' => $botinfo['id']);
		$db_local->botUpdate($spark->peopleGet($data));
		$botinfo = $db_local->botFetchBots($botid)[0];
	}
	$profile = $generate->botGenProfile($botinfo);
	$action = issetor($_GET['apiq']);
	echo "<div>{$profile}</div>";
	if (empty($botinfo['access'])) {
		echo "<div id='input'><table class='smallform'><tr><td><form method='post' action='index.php?id=bots&sub=profile&botid=$botid&apiq=settings&addaccess' enctype='multipart/form-data'>
		<font color='red'><b>BOT ACCESS TOKEN IS MISSING - PLEASE ADD AN ACCESS TOKEN FOR THIS BOT:</b></font><input type='text' name='addtoken' placeholder='ACCESSTOKEN'><input type='submit' name='tokenadd' value='Add token'> <input type='submit' $link_confirm name='deletebot' class='cancel' value='Delete bot'>
		</form></table></div>";
		die();
	}
	else {
		echo "<hr><nav id='nav'>
		<ul class='full-radius'>
		<li><a href='{$link}me'>ME</a></li>
		<li class='dropdown'><a href='#'>ACTIVITY</a>
		<div class='dropdown-content'>
		<a href='index.php?id=spaces&botid=$botid'>SPACES & TEAMS</a>
		<a href='index.php?id=messages&botid=$botid'>MESSAGES</a>
		<a href='index.php?id=devices&botid=$botid'>DEVICES</a>
		</div></li>
		<li><a href='{$link}response'>RESPONSES</a></li>
		<li><a href='{$link}webhooks'>WEBHOOKS</a></li>
		<li><a href='{$link}settings'>SETTINGS</a></li>
		</ul>
		</nav><hr>";
	}
	switch ($action) {
		//Lists details about the bot
		case "me":
			echo "<h1>My details</h1><hr class='gradient'>";
			echo $generate->pretty(array('items'=>array($spark->peopleGetMe($botinfo['id']))));
			break;
			//Webhooks
		case "webhooks":
			if (isset($_POST['webhook_setaccess'])) {
				$webhookid = issetor($_POST['webhookid']);
				$groupid = issetor($_POST['whgroup']);
				if ($webhookid and is_numeric($groupid)) {
					$db_local->webhookSetAccessGroup($webhookid, $groupid);
				}
			}
			echo "<h3>Quick webhooks (Automatic)</h3>
			<div id='input'><form id='quickhook' method='post' enctype='multipart/form-data'>
			<table class='smallform'>
			<tr>
			<td>
			<input type='submit' name='createhook_group' value='Create group conversation webhook'> 
			<td>
			<input type='submit' name='createhook_direct' value='Create direct conversation webhook'> 
					</table></div></form><hr>";?>
					<h3>Create new webhook (Manual)</h3>
					<hr class='gradient'>
					<div id='input'>
						<form name='webhooks' method='post' action='index.php?id=bots&sub=profile&botid=<?php echo $botinfo['id'];?>&apiq=webhooks' enctype='multipart/form-data'>
							<table class='smallform midsize'>
							<tr> <td> Which bot: <td><b><?php echo $botinfo['displayName']; ?></b>
							<input type='hidden' value='<?php echo $botinfo['id']; ?>' name='sender'>
							<tr> <td> Webhook name: <td> <input type='text' required='' placeholder='name' name='name' value=''> 
							<tr> <td> Target URL: <td> <input type='text' required='' placeholder='targeturl' name='targetUrl' value=''>
							<tr> <td> Resource: <td> <input type='text' required='' placeholder='messages' name='resource'>
							<tr> <td> Event: <td>
							<input type='text' required='' placeholder='created' name='event'>
							<tr> <td> Filter: <td>
							<input type='text' placeholder='roomId=xx&roomType=xx' name='filter'>
							<tr> <td> Secret: <td>
							<input type='text' placeholder='random / optional' name='secret'>
							<tr> <td> Create webhook: <td>
							<input type='submit' name='webhook_create' value='Create webhook for this bot'>
							</table>
						</form>
					</div>
				<?php 
					if(isset($_POST['webhook_create'])) {									
						$result = $spark->webhookCreate($_POST);
						$db_local->webhookDbLink($result['id'], $botid);
						header("Location: #{$result['id']}");
					}
					if (isset($_POST['delete_webhook'])) {
						$webhookid = issetor($_POST['webhookid']);
						if ($webhookid) {
							$spark->webhookDelete($webhookid, $botid);
							$db_local->webhookDbUnlink($webhookid);
						}
					}
					if (isset($_POST['resync'])) {
						$result = $spark->webhookGet($botid);
						$webhookid = issetor($_POST['webhookid']);
						if ($webhookid and count($result['items'])) {
							foreach ($result['items'] as $key => $value) {
								if ($webhookid == $value['id']) {
									$db_local->webhookDbLink($value['id'], $botid);
								}
								
							}
						}
					}
					echo "<h3>My webhooks</h3>
					$gradrul";
					echo $generate->pretty($spark->webhookGet($botid), 'hooks', $botid);
				break;
				case "response":
					if (isset($_GET['setfeature'])) {
						$db_local->adminSetFeature($_GET['setfeature'],$botid);
					}
					if (isset($_GET['edit'])) {
						$action_sub = "&update={$_GET['edit']}";
						$values = $db_local->responseFetchResponses("",$_GET['edit'])[0];
						$selected = ($values['is_task'] == 1) ? "selected" : "";
						$f_selected = ($values['is_feature'] == 1) ? "selected" : ""; 
					}
					$group_dropdown = "<select name='accessgroup'><option value='0'>none</option>";
					$group_data = $db_local->groupFetchGroups();		
					foreach ($group_data as $key) {			
						$group_selected = "";
						if (isset($_GET['edit']) and isset($values['accessgroup'])) {
							if ($key['id'] == $values['accessgroup'])
								$group_selected = "selected";
							}
							$group_dropdown .= ($key['botid'] == "0" or $key['botid'] == $botid) ? "<option value='{$key['id']}' $group_selected>{$key['groupname']}</option>":"";
						}
						$group_dropdown .= "</select>";
					?>
					<h3>Create responses</h3>
					<hr class='gradient'>
					<div id='input'>
						<form method='post' action='index.php?id=bots&sub=profile&botid=<?php echo issetor($botinfo['id']) . issetor($action_sub);?>&apiq=response' enctype='multipart/form-data'>
							<table class='rounded'>
								<tr> 
									<td>
										<b>Pre-written features (on/off):</b> 
									<td> 
										Bot: 
										<td> 
											<b><?php echo issetor($botinfo['displayName']);  ?><b/>
												<tr><td rowspan='7' valign='top'>
													
													<div id='mininav'>
														<ul><table>
														<?php 
														$features = $db_local->adminGetFeatures();
														foreach ($features as $key=>$feature) {
															$current = $db_local->responseFetchResponse($botid, $feature['keyword']);
															$en = (count($current)>0 and $current['is_feature'] == '1') ? true:false;
															echo "<tr><td><li><p title='{$feature['description']}'>{$feature['title']}<td><li><a href='index.php?id=bots&sub=profile&botid=$botid&apiq=response&setfeature={$feature['id']}' title='{$feature['description']}'>".onoff($en,'Disable','Enable')."</li></div><br>";
														}
														?>
														</ul></div></table>
													</div>
							<tr> 
								<td> 
									Trigger external task (API): 
										<td>
											<select name='is_task'>
												<option value='0'>No</option>
												<option value='1' <?php echo issetor($selected) ?>>Yes</option>
											</select>		 
							<tr> 
								<td> 
									Keyword: 
										<td>
											<input type='text' required='' placeholder='Keyword' name='keyword' value='<?php echo issetor($values['keyword']);?>'>
											<input type='hidden' value='<?php echo issetor($botinfo['id']);?>' name='botid'>
					<?php 
							if (isset($_GET['edit'])) {
								$editid = issetor($_GET['edit']);
								echo "<input type='hidden' value='$editid' name='id'>";
							}
					?>
							<tr> 
								<td> 
									Response: 
										<td>
											<textarea required='' placeholder='Response to keyword' name='response' rows='20' cols='40'><?php echo issetor($values['response']);?></textarea>
							<tr> 
								<td> 
									File URL: 
										<td>
											<input type='text' placeholder='URL' name='file_url' value='<?php echo issetor($values['file_url']);?>'>
							<tr> 
								<td> 
									Access group: 
										<td> <?php echo $group_dropdown;?>
							<tr> 
								<td> Request info: 
									<td>
										<input type='submit' value='Save response'>
							</form>
						</table><br>
					</div>
					<?php 
					if(isset($_POST['keyword']) and isset($_POST['response']) and !isset($_GET['update'])) {									
						echo $db_local->errorHandler($db_local->responseCreate($_POST));																			
					}
					if (isset($_GET['delete'])) {
						$delete_id = $_GET['delete'];
						$db_local->responseDelete($delete_id);
					}
					if (isset($_GET['update'])) {
						$update_id = $_GET['update'];
						if(isset($_POST['keyword']) and isset($_POST['response']) and isset($_GET['update'])) {
							echo $db_local->errorHandler($db_local->responseUpdate($_POST));
						}
					}
					echo "<h3>My responses</h3>
					<hr class='gradient'>";
					echo $generate->responseGenResponses($botid);
				break;
				
				//SETTINGS
				case "settings":
					$me = $spark->peopleGetMe($botid);
					$myid = issetor($me['id']);
					$verify_self = ($myid == $botid) ? true:false;
					$validity = onoff($verify_self, "Valid", "Not Valid");
					$error = ($verify_self) ? "<font color=$pos_color>Access token is valid for this bot" : "<font color=$neg_color>Access token is invalid for this bot</font>";
					?>
					<h3>Settings</h3>
					<hr class='gradient'>
					<div id='input'>
						<form method='post' action='index.php?id=bots&sub=profile&botid=<?php echo issetor($botid);?>&apiq=settings' enctype='multipart/form-data'>
						<table class='smallform'><tr><td width='300'>Current accesstoken validity: <td> <?php echo $validity . ' ' .  $error; ?>
						<tr>
							<td> Current access token:
								<td><b><?php echo $botinfo['access']; ?></b>
						<tr>
							<td> 
									Update access token:
								<td>
									<input type='text' name='addtoken' placeholder='New access token'><td><input type='submit' value='Update token'></form>
						<tr>
							<td>
									Delete bot and all relations:
								<td>
									<input type='submit' name='deletebot' value='Delete bot' class='cancel' <?php echo $link_confirm; ?>>
									<?php if (!$verify_self) die();?>
						<tr>
							<td>
								Restricted response
							<td>
								<?php 
								echo onoff($db_local->adminCheckBotRestriction($botid),'Restricted, bot will reply to specific mail domains!','Unrestricted, bot will reply to everyone!');
								echo ($db_local->adminCheckBotRestriction($botid)) ? "<font color=$pos_color> Bot has an active restriction policy!</font>":warning("Please consider restricting the bot by adding a domain!") . "<font color=$neg_color> Bot has no restriction policy and can reply to any domains!</font>";
								?>
						<tr>
							<td>
								Bot responds to these mail domains (Enables restriction)
							<td>
								<form method='post' action='#' enctype='multipart/form-data'>	
									<input type='hidden' name='domaintrigger'>	
									<?php 
									$domains = $db_local->acceptedDomainFetch();
									foreach ($domains as $key => $value) {
										echo ($db_local->botCheckDomain($value['id'], $botid)) ? "<input type='checkbox' checked name='botdomains[]' value='{$value['id']}'> {$value['domain']}<br>" : "<input type='checkbox' name='botdomains[]' value='{$value['id']}'> {$value['domain']}<br>";
									}
									?>
									<td><input type='submit' value='Apply selection'>
								</form>
							<tr>
							<td>	
									Default response:
								<td>	
									<form method='post' action='index.php?id=bots&sub=profile&botid=<?php echo $botid;?>&apiq=settings' enctype='multipart/form-data'>
										<input type='text' value='<?php echo $botinfo['defres']; ?>' name='defres' placeholder='Unknown command, please type ... to see a list of commands'><td><input type='submit' value='Apply default response'>
									</form>
							<tr>
							<td>	
									Update bot details: 
								<td>	
									<form method='post' action='index.php?id=bots&sub=profile&botid=<?php echo $botid;?>&apiq=settings&updatebot' enctype='multipart/form-data'>
										<input type='submit' value='Fetch updated bot details from Webex Teams'>
									</form>
						<tr>
							<td>
									Primary bot:
								<td>
									<?php echo ($db_local->adminCheckIsBotMain($botid)) ? onoff(true,20,20):"<form method='post' action='index.php?id=bots&sub=profile&botid=$botid&apiq=settings&setprimary' enctype='multipart/form-data'><input type='submit' value='Set as primary bot!'></form>";?>
							</table>
							<?php 			
								if (isset($_GET['removespace'])) {
									$db_local->adminRemoveJoinableSpace($botid,$_GET['removespace']);
								}
								if (count($spaces = $db_local->adminGetJoinableSpace($botid))>0) {
									echo "<h3>My joinable spaces</h3><hr class='gradient'>
											<table class='rounded'><tr><td bgcolor='#737CA1'><b>Joinable spaces (Spaces joinable via this bot)</b><td bgcolor='#737CA1'><b>Manage</b><tr><td>Options<td><a href='index.php?id=bots&apiq=settings&sub=profile&botid=$botid&joinableupdate'>Update all</a> - Purge all";
									foreach ($spaces as $key => $value) {
										if (isset($_GET['joinableupdate'])) {
											$test = $spark->roomGetDetails($botid,$value['spaceid']);
											if (!isset($test['id'])) {
												$db_local->adminRemoveJoinableSpace($botid,$value['spaceid']);
											}
											else {
												$db_local->adminUpdateJoinableSpace($test['id'], $test['title']);
												$value['spacetitle'] = $test['title'];
											}
										}
										echo "<tr><td>{$value['spacetitle']}<td><a href='index.php?id=bots&sub=profile&apiq=settings&botid=$botid&removespace={$value['spaceid']}'>Remove</a>";
									}
									echo "</table>";
								}
								//Checks if this bot has some excluded spaces where the bot will reply to all people in that partiucular space
								//Can update the name on all spaces and check if the space still exists, if not it will delete the space from the database
								if (count($excluded_spaces = $db_local->adminGetGroupResponseAcl($botid))) {
									echo "<br><table class='rounded'><tr><td bgcolor='#737CA1'><b>Excluded spaces (Group response enabled for this bot)</b><td bgcolor='#737CA1'><b>Manage</b><tr><td>Options<td><a href='index.php?id=bots&sub=profile&apiq=settings&botid=$botid&excludedupdate'>Update all</a> - Purge all";
									if (isset($_GET['removeexcluded'])) {
										$db_local->adminRemoveGroupResponseAcl($botid,$_GET['removeexcluded']);
										$excluded_spaces = $db_local->adminGetGroupResponseAcl($botid);
									}
									foreach ($excluded_spaces as $key => $value) {
										if (isset($_GET['excludedupdate'])) {
											$test = $spark->roomGetDetails($botid,$value['id']);
											if (!isset($test['id'])) {
												$db_local->adminRemoveGroupResponseAcl($botid,$value['id']);
												continue;
											}
											else {
												$db_local->adminUpdateGroupResponseAcl($test['id'], $test['title']);
												$value['spacetitle'] = $test['title'];
											}
										}
										if ($value['spacetitle'] == "") $value['spacetitle'] = "Unknown, please update!";
										echo "<tr><td>{$value['spacetitle']}<td><a href='index.php?id=bots&sub=profile&apiq=settings&botid=$botid&removeexcluded={$value['id']}'>Remove</a>";
									}
									echo "</table>";
								}			
				default:
				break;
			}
		}
			else {
				//BOT DASHBOARD	
				echo "<h1>MY BOTS<hr class='gradient'></h1>
			 		  <table class='rounded compact'>
						<tr>
							<td width='25' colspan='2'><b>Bot</b>
								<td> <b>Responses</b>
								<td> <b>Webhooks</b>
								<td> <b>Excluded spaces</b>
								<td> <b>Has access token?</b>
								<td> <b>Restricted?</b>
								<td> <b>Primary?</b>";
				foreach ($bots as $key => $value) {
					$name = $value['displayName'];
					$image = "<img class='rounded' src='{$value['avatar']}' height=40 width=40 border=0>";
					$responses = count($db_local->responseFetchResponses($value['id']));
					$webhooks = count($db_local->select("SELECT * FROM bot_webhook WHERE botid = '{$value['id']}'"));
					$excluded_spaces = count($db_local->adminGetGroupResponseAcl($value['id']));
					$restriction = $db_local->adminCheckBotRestriction($value['id']);
					$warning = ($webhooks > 0 and !$restriction) ? warning("This bot is unrestricted and have webhooks! This means that the bot will respond to everyone talking to it, also outside your organization. Please consider setting up a domain restriction.") : "";
					$n_responses = ($responses > 0) ? colorize_value($pos_color, $responses): colorize_value($inactivecolor, $responses);
					$n_webhooks = ($webhooks > 0) ? colorize_value($pos_color, $webhooks): colorize_value($inactivecolor, $webhooks);
					$n_excluded = ($excluded_spaces > 0) ? colorize_value($pos_color, $excluded_spaces): colorize_value($inactivecolor, $excluded_spaces);
					$primary = ($maininfo['id'] == $value['id']) ? onoff(true,"All requests to Webex Teams API is primarily made with the primary bot"):"";
					$n_restriction = onoff($restriction,'Domain restricted!', 'Not domain restricted!');
					$hastoken = onoff((!empty($value['access'])),'Yes', 'No') ;	
					echo "	<tr>
					<td> <a href='index.php?id=bots&sub=profile&botid={$value['id']}&apiq=settings'>$image</a>
								<td> <a class='linkblock' href='index.php?id=bots&sub=profile&botid={$value['id']}&apiq=settings'>$name</a>
								<td><b/>$n_responses
								<td><b/>$n_webhooks
								<td><b/>$n_excluded
								<td><b/>$hastoken
								<td>$n_restriction $warning
								<td>$primary";
				}
				echo "</table>
	<h3>Add bots<hr class='gradient'></h3>
	<div id='input'>
	<table class='smallform'>
	<tr>
		<td>
			<form method='post' action='index.php?id=bots' enctype='multipart/form-data'> 
				<input type='text' name='addbotmail' placeholder='bot@webex.bot'>
				<td>
					<input type='submit' value='Add bot'>
			</form>
		</td></tr></table>
</div>";			
			}		
		?>