<?php 
if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 
$mainbot = $db_local->botGetMainInfo();
$botid = $mainbot['id'];
//Page operations 
if(isset($_POST['add_contact'])) {
	$user_request = array('recepientValue' => $_POST['contactid'], 'recepientType' => 'id', 'sender' => $botid);
	$user_info = $spark->peopleGet($user_request);
	if (isset($user_info['items'][0]['id'])) {
		$db_local->contactAdd($user_info, $botid);
		header("Location: index.php?id=contacts&contactid={$user_info['items'][0]['id']}&feedback=success-UserAdd");
	} else header("Location: index.php?id=contacts&feedback=warning-UserNotFound");
}
if (isset($_POST['bulk_add'])) {
	//Adds 50 users at a time (pace control)
	$users = $_POST['users_csv'];
	$group = $_POST['groups'];
	$csv = preg_replace("/\r|\n|\s+/", "", $users);
	$csv = explode(",", $csv);
	echo $spark->bulkSplitRequestPost("addtolocal",$csv, $botid, 0, 50, 150, 10, 150, $group);
}
if (isset($_POST['retry_addtolocal'])) {
	$a = 0;
	$group = $_POST['groups'];
	//Start request intervals
	echo $spark->bulkSplitRequestPost("addtolocal", $_POST['personids'], $botid, 0, 50, 150, 10, 150, $group);
	//End request intervals
}
if (isset($_POST['delete_updatelocal'])) {
	if (count($_POST['personids']) > 0) {
		foreach ($_POST['personids'] as $key => $value) {
			$db_local->contactRemove($value);
		}
	}
}
if (isset($_POST['bulk_delete']) or isset($_POST['bulk_update'])) {
	$context = (isset($_POST['bulk_delete'])) ? "delete":"update";
	if (isset($_POST['userlist'])) {
		$data = $_POST['userlist'];
		if (isset($_POST['bulk_delete'])) {
			foreach ($data as $key => $value) {
				$db_local->contactRemove($value);
				if ($value == $_SESSION['userid']) {
					session_destroy();
					header('Location: index.php?id=login');
				}
			}
		} else {
			echo $spark->bulkSplitRequestPost("updatelocal",$data, $botid, 0, 1000, 150, 15, 100);
		}
	}
	else {
		echo feedbackMsg("No users selected","please select users to $context them","warning");
	}
}
if(isset($_POST['update_contact'])) {
	$updateid = $db_local->quote($_POST['contactid']);
	$user_request = array('recepientValue' => $updateid, 'recepientType' => 'id', 'sender' => $botid);
	$user_info = $spark -> peopleGet($user_request);
	if (count($user_info['items'])) {
		$db_local -> contactUpdate($user_info);
		header("Location: index.php?id=contacts&contactid={$updateid}&feedback=success-UserUpdate");
	} else {
		header("Location: index.php?id=contacts&contactid={$updateid}&feedback=warning-UserNotFound");
	}
}
if (isset($_GET['add']) and isset($_GET['group']) and isset($_GET['contactid'])) {
		$db_local -> groupAddContact(array('groupid' => $db_local->quote($_GET['group']), 
										   'contactid' => $db_local->quote($_GET['contactid'])));
}
if (isset($_GET['remove']) and isset($_GET['group']) and isset($_GET['contactid'])) {
		$db_local -> groupRemoveContact($db_local->quote($_GET['group']), $db_local->quote($_GET['contactid'])); //Removes a group membership
}
if (isset($_GET['special_access_remove'])) {
	$db_local -> adminRemoveUserGroupResponseAcl($db_local->quote($_GET['botid']), 
											   $db_local->quote($_GET['spaceid']), 
											   $db_local->quote($_GET['special_access_remove']));
}
if(isset($_POST['delete_contact'])) {
	$db_local -> contactRemove($db_local->quote($_POST['contactid']));
	if ($_POST['contactid'] == $_SESSION['userid']) {
		session_destroy();
		header('Location: index.php?id=login');
	}
	header('Location: index.php?id=contacts');
}
if (isset($_GET['contactid'])) {
	$userid = $db_local->quote($_GET['contactid']);
	
	if (isset($_POST['genpass'])) {
		$password = generatePassword();
		$hashpw = password_hash($password, PASSWORD_DEFAULT);
		$text = "Here is your new login passord to the WebEx Teams Bot Management site, please change the password when you login.\n<blockquote class='success'>Password: <b>" . $password . "</b></blockquote>\n";
		$db_local->userUpdateSettings('update',$hashpw, $userid, '1');
		$spark->messageSend($spark->messageBlob($text, $userid, $botid));
	}
	if (isset($_POST['gentoken'])) {
		$token = hash('md5', generatePassword());
		$db_local -> userUpdateSettings('token',$token,$userid);
	}
	if (isset($_POST['setpass'])) {
		$password = $db_local -> quote($_POST['password']);
		$conf_password = $db_local -> quote($_POST['pwconfirm']);
		if (!empty($password)) {
			$message = feedbackMsg("Success!", "New password set", "success");
			if ($password == $conf_password) { 
				$hashpw = password_hash($password, PASSWORD_DEFAULT);
				$db_local -> userUpdateSettings('update', $hashpw, $userid, '1');
				$message = feedbackMsg("Success!", "New password set", "success");
			} 
			else {
				$message = msgList("warning-PwMismatch");
			}
		}else {
			$message = feedbackMsg("Warning!", "You cannot set a blank password!", "warning");
		}
	}
	if (isset($_POST['removelogin'])) {
		$db_local -> userUpdateSettings('remove','',$userid);
	}
	echo "<h1><a href='index.php?id=contacts'>USERS</a> > ".strtoupper($db_local->contactGetName($userid))."$gradrul</h1>";
	echo $generate->contactGenProfile($userid, 'listmemberships');
	echo "<table class='rounded compact'><tr><td width='200'>Group response access:<td>";
	echo $db_local->contactGetGroupResponseAcl($userid);
	echo "</table>";
	$valid = onoff($db_local->adminCheckIsLoginUser($userid),"Yes","No", "User is a site admin", "User is not a site admin");
	$userinfo = $db_local->contactFetchContacts($userid);
	$message = issetor($message);
	if (issetor($userinfo[0]['token'])) {
		$token = $userinfo[0]['token'];
		$tokenval = "success";
	} else {
		$token = "No token found";
		$tokenval = "danger";
	}
	
	echo "<h3>User settings</h3>$gradrul 
		<div id='input'><form method='post' id='usersettings' action='#usersettings' enctype='multipart/form-data'>
			<table class='smallform midsize'>
			<tr><td colspan='2'>$message
			<tr>
				<td>Site admin:<td>$valid 
			<tr>
				<td>Set/change admin password (manual):<td><input type='password' placeholder='New password' name='password'><input type='password' name='pwconfirm' placeholder='Confirm password'><input type='submit' value='Set password' name='setpass'>
			<tr>
				<td><input type='submit' name='gentoken' value='Generate API Token (API access)'>:<td><blockquote class='$tokenval'><strong>$token</strong></blockquote>
			<tr>
				<td>Make random password and notify user:<td><input type='submit' name='genpass' value='Generate'>
			<tr>
				<td>Remove site admin role (does not delete user):<td><input type='submit' class='cancel' name='removelogin' value='Remove'>
			</table></form>";
}
else {
	$contacts = $db_local->contactFetchContacts();
	$number_of_contacts = count($contacts);
	$n_contacts = colorize_value($infocolor, $number_of_contacts);
	?>
	<?php 
	$groupoptions = $generate->groupOptions();
	?><h1>USERS</h1> 
	<div id='input'>
	<table class='rounded compact'>
		<tr>
			<td valign='top'>			
				<form method='post' onsubmit='loading("process", "Searching...")' action='#' enctype='multipart/form-data'> 	
				    <input type='text' class='search' focused name='search_contact' placeholder='Search for Name or E-mail in Webex Teams'><input type='submit' class='hide' name='search' value='Search'>	
				</form>	
		<tr>
			<td valign='top'> <h3>Import users</h3><?php echo $gradrul; ?>
				
							<form method='post' action='#' onsubmit='loading("process", "Please wait while importing users...")' enctype='multipart/form-data'>
								<table class='rounded compact'>
									<tr>
										<td colspan='2'>		
											<textarea name='users_csv' required placeholder='E-mails (CSV)' rows='4' cols='60'></textarea>
									<tr>		
										<td>
											<input type='submit' id='import_users' value='Import users' name='bulk_add'>
									<td width='80%'>
											<select name='groups'>
												<option value='' selected>Make member of..</option>
												<?php echo $groupoptions; ?>
											</select>
					    	</form>
					    	<tr><td colspan='2'>
					    	<button type='button' width='100%' id='process' class='processhide'></button> 
					    	<tr><td colspan='2'>
					    	<?php 
					if(isset($_POST['search'])) {
						$recepient_type = (validateEmail($_POST['search_contact'])) ? "email":"displayName";
						$contact_data = array('recepientValue' => $_POST['search_contact'], 'recepientType' => $recepient_type, 'sender' => $botid);
						echo $generate->userSearch($spark->peopleGet($contact_data));
					}	
			?>
					    </td>
					</tr>
				</table>

	<?php
	echo "<td valign='top'>";
	echo "<h3>Imported users ($n_contacts)</h3>$gradrul";
	echo $generate->contactGenUserList();
	echo "</table></div>";
}			
echo "</div>";
?>