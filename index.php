<!DOCTYPE html>
<?php session_start(); 
$version = "24031";
$site_path = "sites/";
$neg_color = "#ff4444";
$pos_color = "#007203";
$infocolor = "#0C2C52";
$com_color1 = "#5E9DC8";
$com_color2 = "#3d4f7d";
$gradrul = "<hr class='gradient'>";
$inactivecolor = "lightgray";
$warningcolor = "orange";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$link_confirm = "onclick='return confirm(\"Are you sure? This operation cannot be undone!\")'";
include "includes/functions.inc.php";
if (!isset($_SESSION['status'])) {
	$_SESSION['status'] = "";
}

$db_local = new Db();
$generate = new OutputEngine();
$spark = new SparkEngine();

if (verify()) {
	if ($db_local->adminCheckIsLoginUser($_SESSION['userid'])){
		//Do nothing
	} else {
		if ($db_local->adminCheckSiteAdminExists()) header('Location: index.php?id=login');
		else header('Location: index.php?id=setup');
	}
	if ($db_local->adminCheckIfMaintenance()) $warning = "<font color='$neg_color'> - WARNING: MAINTENANCE MODE ACTIVATED!</font>";
	if ($db_local->adminCheckIfTaskMonitor()) {
		if ($db_local->adminCheckServiceStatus()) $warning1 = "<font color='$neg_color'> - WARNING: TASK SERVICE IS DOWN!</font>";
	}
}

$windowid = (isset($_GET['id'])) ? $_GET['id'] : "bots";

switch ($windowid) {
	case "login":
		$page = "{$site_path}login.php";
		break;
	case "setup":
		$page = "{$site_path}setup.php";
		break;
	case "administration":
		$page= "{$site_path}administration.php";
		break;
	case "logs":
		$page= "{$site_path}logs.php";
		break;
	case "contacts":
		$page = "{$site_path}contacts.php";
		break;
	case "spaces":
		$page = "{$site_path}spaces.php";
		break;
	case "groups":
		$page = "{$site_path}groups.php";
		break;
	case "feedback":
		$page = "{$site_path}feedback.php";
		break;
	case "messages":
		$page = "{$site_path}messages.php";
		break;
	case "bots":
		$page = "{$site_path}bots.php";
		break;
	case "docs":
		$page = "{$site_path}docs.php";
		break;
	case "devices":
		$page = "{$site_path}devices.php";
		break;
	default:
		$page= "{$site_path}bots.php";
		break;
}
?>

<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel='stylesheet' href='https://code.s4d.io/widget-space/production/main.css'>
	<link rel="stylesheet" href="style/easybot.css" type="text/css">
	<script language="JavaScript">
function toggle(source, name) {
	  checkboxes = document.getElementsByName(name);
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}
function loading(id, loadingtext)
{
  ele = document.getElementById(id);
  ele.style.display="block"
  ele.innerHTML='<i class="fa fa-spinner fa-spin"></i> ' + loadingtext;
}

</script>
</head>

<body>
<div id="page-wrapper">
<header id="header">	
	<div class="top-wrapper"><table><tr><td><img class='rounded' height='75px' width='75px' src='/images/static/webexteams.png' style='background: none; border: none; -webkit-filter: drop-shadow(2px 2px 2px #222); filter: drop-shadow(2px 2px 2px #222);'><td><h1 class='heading1'>Webex Teams - Bot Manager<?php echo issetor($warning) . issetor($warning1);?></h1></table></div>
</header>
<nav id="nav">
	<ul>
  		<?php 		
  		if (verify()) { 
  			echo "<li class='dropdown'><a href='#'>ADMINISTRATION</a>
				<div class='dropdown-content'>	
				<a href='index.php?id=administration&sub=default'>SETTINGS</a>
				<a href='index.php?id=logs&maxresults=50'>LOGS</a>
				<a href='index.php?id=docs'>USER GUIDE</a>
				</div></li>";
  			echo "<li><a href='index.php?id=bots'>MY BOTS</a></li>";
  			echo "<li class='dropdown'><a href='#'>ACTIVITY</a>
				<div class='dropdown-content'>	
				<a href='index.php?id=contacts'>USERS</a>
				<a href='index.php?id=groups'>GROUPS</a>
                <a href='index.php?id=feedback'>FEEDBACK</a>
				<a href='index.php?id=messages'>MESSAGES</a>
				<a href='index.php?id=spaces'>SPACES & TEAMS</a>
				<a href='index.php?id=devices'>DEVICES</a>
				</div></li>";
  			echo "<li style='float:right'><a href='index.php?id=login&signoff'>LOG OFF</a></li>";	
  			echo "<li style='float:right'><a href='index.php?id=contacts&contactid={$_SESSION['userid']}' title='View profile'><img class='rounded' src='{$_SESSION['avatar']}' width='15px' height='15px'></a></li>";
  		}
		else {
			echo "<li style='float:right'><a href='index.php?id=login'>LOGIN</a></li>";
		}
		
  		?>		
  		
	</ul>
</nav>

<section style='margin-top: 5px;'>
	<?php 	
	$feedback = issetor($_GET['feedback']);
	if (!empty($feedback)) {
		echo msgList($feedback);
	}
	if ($db_local->adminCheckSiteAdminExists()) {
		if ($windowid != "login" and !verify()) header("Location: index.php?id=login");
	} 
	else {
		session_destroy();
		if ($windowid != "setup") header("Location: index.php?id=setup");
	}

	include $page; 
	?>	
	
</section>
</body>
</html>



	
   
   